public class Solution {
    public List<String> powerSet(String str) {
        int N = str.length();
        List<String> res = new ArrayList<>();
        for (int i = 0; i < (1 << N); i++) {
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < N; j++) {
                if ((i & (1 << j)) > 0) {
                    sb.append(str.charAt(j));
                }
            }
            if (sb.length() != 0) {
                res.add(sb.toString());
            }
        }
        return res;
    }

    public static void main(String[] args) {
        System.out.println(new Solution().powerSet("abcd"));
    }
}

public class Solution {
    public List<String> powerSet(String str) {
        List<String> res = new ArrayList<>();
        helper(res, "", str.toCharArray(), 0);
        return res;
    }

    public void helper(List<String> res, String buffer, char[] arr, int pos) {
        if (buffer.length() > 0) {
            res.add(buffer);
        }
        for (int i = pos; i < arr.length; i++) {
            helper(res, buffer + arr[i], arr, i + 1);
        }
    }

    public static void main(String[] args) {
        System.out.println(new Solution().powerSet("abcd"));
    }
}