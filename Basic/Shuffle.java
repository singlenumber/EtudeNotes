public class Solution {
    static Random rand = new Random();

    public static void shuffle1(int[] A) {
        for (int i = 0; i < A.length; i++) {
            int j = i + rand.nextInt(A.length - i);
            int tmp = A[i];
            A[i] = A[j];
            A[j] = tmp;
        }
    }

    public static void shuffle2(int[] A) {
        for (int i = A.length - 1; i >= 0; i--) {
            int j = rand.nextInt(i + 1);
            int tmp = A[i];
            A[i] = A[j];
            A[j] = tmp;
        }
    }
    public static void main(String[] args) {
        int[] A = {1, 2, 3, 4, 5, 6};
        shuffle2(A);
        for (int e: A) {
            System.out.print(e + " ");
        }
    }
}