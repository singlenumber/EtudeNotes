public class Solution {
    public List<Integer> onlyOne(int[] A) {
        HashSet<Integer> res = new HashSet<>();
        HashSet<Integer> set = new HashSet<>();
        for (int i = 0; i < A.length; i++) {
            if (!set.contains(A[i])) {
                set.add(A[i]);
                res.add(A[i]);
            } else {
                res.remove(A[i]);
            }
        }
        return new ArrayList<>(res);
    }

    public static void main(String[] args) {
        System.out.println(new Solution().onlyOne(new int[]{1,1,2,3,3,4}));
    }
}