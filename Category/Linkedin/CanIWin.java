boolean canIWin(int maxNum, int target)，从1,2...maxNum的数组里两个玩家轮流选数，第一个达到sum>=target的玩家获胜

Given an array of positive integers and two players. In each turn, one player picks up one number and if the sum of all the picked up numbers is greater than a target number, the player wins. Write a program canIWin() to print the result.
Answer:
enum Result {Win, Lose, Draw}
public class PickUpNumbers {
        public static Result canIWin(int[] numberPool, int target) {
                if (target <= 0) return Result.Lose;
                boolean isEmpty = true;
                for (int data : numberPool)
                        if (data > 0) isEmpty = false;
                if (isEmpty) return Result.Draw;
                else {
                        for (int data : numberPool)
                                if (data >= target) return Result.Win;
                        Result drawFlag = Result.Draw, rivalWinFlag = Result.Win;
                        for (int i = 0; i < numberPool.length; ++i) {
                                if (numberPool < 0) continue;
                                int data = numberPool;
                                numberPool = -1;
                                Result rivalResult = canIWin(numberPool, target - data); // rival's turn
                                if (rivalResult != Result.Win) rivalWinFlag = rivalResult;
                                if (rivalResult != Result.Draw) drawFlag = rivalResult;
                                numberPool = data;
                        }
                        if (drawFlag == Result.Draw) return Result.Draw;
                        if (rivalWinFlag == Result.Win) return Result.Lose; // whatever number i choose, rival wins
                        return Result.Win;
                }
        }
}
Test Code:
public static void main(String[] args) {
        int[] numberPool1 = {1, 2, 3};
        System.out.println(PickUpNumbers.canIWin(numberPool1, 5));
        System.out.println(PickUpNumbers.canIWin(numberPool1, 4));
        System.out.println(PickUpNumbers.canIWin(numberPool1, 8));
        int[] numberPool2 = {1, 2, 3, 4, 5};
        System.out.println(PickUpNumbers.canIWin(numberPool2, 11));
        int[] numberPool3 = {1, 2, 3, 4, 5, 6};
        System.out.println(PickUpNumbers.canIWin(numberPool3, 17));
}
Result:
Win
Lose
Draw
Lose
Win