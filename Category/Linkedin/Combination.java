import java.util.*;

public class Solution {
    public List<String> combination(String str) {
        List<String> result = new ArrayList<>();
        helper(result, str, 0, "");
        return result;
    }

    public void helper(List<String> result, String str, int pos, String buffer) {
        if (pos >= str.length()) {
            result.add(buffer);
            return;
        }
        int left = str.indexOf("[", pos);
        int right = str.indexOf("]", pos);
        if (left == -1 && right == -1) {
            helper(result, str, str.length(), buffer + str.substring(pos));
        } else {
            List<Integer> numbers = parseString(str.substring(left + 1, right));
            for (int i = 0; i < numbers.size(); i++) {
                helper(result, str, right + 1, buffer + str.substring(pos, left) + numbers.get(i));
            }
        }
    }

    public List<Integer> parseString(String str) {
        List<Integer> res = new ArrayList<>();
        int i = 0;
        while (i < str.length()) {
            int comma = str.indexOf(',',i);
            if (comma == -1) {
                comma = str.length();
            }
            res.add(Integer.parseInt(str.substring(i, comma)));
            i = comma + 1;
        }
        return res;
    }

    public static void main(String[] args) {
        String str = "app[1,2].corp[3,4].goo[5,6].com";
        List<String> res = new Solution().combination(str);
        System.out.println(res);
    }
}