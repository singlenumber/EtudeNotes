/*

10 -> 5 -> 12 -> 7 -> 11 
 |               |
 4 -> 20 -> 13   17 -> 6
      |      |   |     |
      2     16   9     8
             |   |
             3   19 -> 15 
The above list should be converted to 10->5->12->7->11->4->20->13->17->6->2->16->9->8->3->19->15
*/
public class Solution {
    public void flattenList(ListNode head) {
        if (head == null) {
            return;
        }
         
        // Step 1: get the tail of the list
        ListNode tail = head;
        while (tail.next != null) {
            tail = tail.next;
        }
         
        // Step 2: iterate from the first layer. If a node has a child, put it to
        // the end of hte list, and update the tail pointer
        ListNode p = head;
         
        while (p != tail) {
            if (p.child != null) {
                ListNode childHead = p.child;
                tail.next = chiidHead;
                 
                // Updat the tail
                while (tail.next != null) {
                    tail = tail.next;
                }
                 
                p.child = null;
            }
             
            p = p.next;
        }
    }
}