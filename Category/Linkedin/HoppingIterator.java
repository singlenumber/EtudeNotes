import java.util.*;

/**
 * Implement an iterator that hops specified number of times and then returns the next
 * element after the hop. Note: the iterator always returns the first element as
 * it is, and starts hopping only after the first element.
 *
 * Examples:
 *
 * If the original iterator returns: [1, 2, 3, 4, 5] in order, then the hopping
 * iterator will return [1, 3, 5] in order when the hop value is 1.
 *
 * If the original iterator returns: [1, 2, 3, 4, 5] in order, then the hopping
 * iterator will return [1, 4] in order when the hop value is 2.
 *
 * If the original iterator returns: [1, 2, 3, 4, 5] in order, then the hopping
 * iterator will return [1, 5] in order when the hop value is 3.
 *
 * Methods expected to be implemented:
 *
 * public class HoppingIterator<T> implements Iterator<T> {
 * 		public HoppingIterator(Iterator<T> iterator, int numHops) {...}
 * 		public boolean hasNext() {...}
 * 		public T next() {...}
 * }
 */
public class HoppingIterator<T> implements Iterator<T> {

    final private Iterator<T> iterator;
    private boolean first;
    T nextItem;
    int numOfHops;

    public HoppingIterator(Iterator<T> iterator, int numOfHops) {
        this.iterator = iterator;
        this.numOfHops = numOfHops;
        first = true;
        nextItem = null;

    }

    @Override
    public boolean hasNext() {
        if (nextItem != null) {
            return true;
        }
        if (!first) {
            for (int i = 0; i < numOfHops && iterator.hasNext(); i++) {
                iterator.next();
            }
        }
        if (iterator.hasNext()) {
            nextItem = iterator.next();
            first = false;
        }
        return nextItem != null;
    }

    @Override
    public T next() {
        if (nextItem == null) {
            throw new NoSuchElementException();
        }
        T toReturn = nextItem;
        nextItem = null;
        return toReturn;
    }

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>(Arrays.asList(1,2,3,4,5));
        HoppingIterator<Integer> iter = new HoppingIterator<>(list.iterator(), 1);
        while (iter.hasNext()) {
            System.out.println(iter.next());
        }

    }
}
