import java.util.*;

public class Solution {
    Iterable<Integer> Union(Iterator<Integer> a, Iterator<Integer> b) {
        List<Integer> res = new ArrayList<>();
        Integer p = a.hasNext() ? a.next(): null;
        Integer q = b.hasNext() ? b.next(): null;
        while (p != null && q != null) {
            if (p.equals(q)) {
                res.add(p);
                p = a.hasNext() ? a.next(): null;
                q = b.hasNext() ? b.next(): null;
            } else if (p < q) {
                res.add(p);
                p = a.hasNext() ? a.next(): null;
            } else if (p > q) {
                res.add(q);
                q = b.hasNext() ? b.next(): null;
            }

        }
        while (p != null) {
            res.add(p);
            p = a.hasNext() ? a.next(): null;
        }
        while (q != null) {
            res.add(q);
            q = b.hasNext() ? b.next(): null;
        }
        return res;
    }

    Iterable<Integer> Intersection(Iterator<Integer> a, Iterator<Integer> b) {
        List<Integer> res = new ArrayList<>();
        Integer p = a.hasNext() ? a.next(): null;
        Integer q = b.hasNext() ? b.next(): null;
        while (p != null && q != null) {
            if (p.equals(q)) {
                res.add(p);
                p = a.hasNext() ? a.next(): null;
                q = b.hasNext() ? b.next(): null;
            } else if (p < q) {
                p = a.hasNext() ? a.next(): null;
            } else if (p > q) {
                q = b.hasNext() ? b.next(): null;
            }

        }
        return res;
    }

    public static void main(String[] args) {
        Solution s = new Solution();
        List<Integer> list1 = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5));
        List<Integer> list2 = new ArrayList<>(Arrays.asList(3, 4, 5, 6, 7));
        System.out.println(s.Union(list1.iterator(), list2.iterator()));
        System.out.println(s.Intersection(list1.iterator(), list2.iterator()));
    }
}