import java.io.*;
import java.util.*;

public class Solution {
    public static Iterable<Integer> mergeKSortedIterators(List<Iterator<Integer>> iterators) {
        List<Integer> result = new ArrayList<>();
        if (iterators == null || iterators.size() == 0) {
            return result;
        }

        PriorityQueue<MyIterator> pq = new PriorityQueue<>(iterators.size());

        for (Iterator<Integer> iterator : iterators) {
            if (iterator.hasNext()) {
                pq.add(new MyIterator(iterator.next(), iterator));
            }
        }

        while (!pq.isEmpty()) {
            MyIterator curr = pq.poll();
            result.add(curr.val);
            if (curr.hasNext()) {
                pq.add(curr);
            }
        }

        return result;
    }

    private static class MyIterator implements Comparable<MyIterator> {
        private Integer val;
        private Iterator<Integer> iterator;

        public MyIterator(Integer val, Iterator<Integer> iterator) {
            this.val = val;
            this.iterator = iterator;
        }

        public boolean hasNext() {
            if (iterator.hasNext()) {
                val = iterator.next();
                return true;
            }
            return false;
        }

        public int compareTo(MyIterator that) {
            return this.val - that.val;
        }
    }

    public static void main(String[] args) {
        List<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(3);
        a.add(5);

        List<Integer> b = new ArrayList<>();
        b.add(2);
        b.add(4);

        List<Iterator<Integer>> iterators = new ArrayList<>();
        iterators.add(a.iterator());
        iterators.add(b.iterator());

        Iterable<Integer> result = mergeKSortedIterators(iterators);

        for (Integer num : result) {
            System.out.println(num);
        }
    }
}

// Merge K arrays
public class Solution {
    public List<Integer> mergeKArray(List<List<Integer>> lists) {
        List<Integer> res = new ArrayList<>();
        PriorityQueue<Tuple> minHeap = new PriorityQueue<>((a, b) -> a.val - b.val);
        for(List<Integer> list: lists) {
            if (list != null) {
                Iterator<Integer> iterator = list.iterator();
                if (iterator.hasNext()) {
                    int val = iterator.next();
                    minHeap.offer(new Tuple(val, iterator));
                }
            }
        }
        while (minHeap.size() != 0) {
            Tuple cur = minHeap.poll();
            res.add(cur.val);
            if (cur.getIterator().hasNext()) {
                int val = cur.getIterator().next();
                minHeap.offer(new Tuple(val, cur.iterator));
            }
        }
        return res;
    }


    public static void main(String[] args) {
        List<Integer> a = Arrays.asList(3, 5, 7, 9);
        List<Integer> b = Arrays.asList(2, 4, 8);
        List<Integer> c = Arrays.asList(1, 5, 6, 9);
        List<List<Integer>> list = Arrays.asList(a, b, c);
        List<Integer> res = new Solution().mergeKArray(list);
        System.out.println(res);
    }

    class Tuple {
        int val;
        Iterator<Integer> iterator;

        Tuple(int val, Iterator<Integer> iterator) {
            this.val = val;
            this.iterator = iterator;
        }

        public int getVal() {
            return val;
        }

        public Iterator<Integer> getIterator() {
            return iterator;
        }
    }
}