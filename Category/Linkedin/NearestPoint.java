public class Solution {
    List<Point> list = new ArrayList<>();

    public void addPoint(Point p){
        list.add(p);
    }

    public List<Point> findNearest(Point center, int p){
        HashMap<Integer, Point> map = new HashMap<>();
        PriorityQueue<Point> pq = new PriorityQueue<>(p, new Comparator<Point>() {
            @Override
            public int compare(Point o1, Point o2) {
                int diff_o2 = (int)Math.sqrt(Math.pow(o2.x - center.x, 2) + Math.pow(o2.y - center.y, 2));
                int diff_o1 = (int)Math.sqrt(Math.pow(o1.x - center.x, 2) + Math.pow(o1.y - center.y, 2));
                return diff_o2 - diff_o1;
            }
        });
        for (Point e: list) {
            if (pq.size() < p) {
                pq.offer(e);
            } else {
                int cur_dist = (int)Math.sqrt(Math.pow(e.x - center.x, 2) + Math.pow(e.y - center.y, 2));
                int peek_dist = (int)Math.sqrt(Math.pow(pq.peek().x - center.x, 2) + Math.pow(pq.peek().y - center.y, 2));
                if (cur_dist < peek_dist) {
                    pq.poll();
                    pq.offer(e);
                }
            }
        }
        List<Point> res = new ArrayList<>();
        while (pq.size() != 0) {
            res.add(pq.poll());
        }
        return res;
    }

    public static void main(String[] args) {
        Solution s = new Solution();
        s.addPoint(new Point(0, 1));
        s.addPoint(new Point(0, 2));
        s.addPoint(new Point(0, 2));
        s.addPoint(new Point(0, 3));
        s.addPoint(new Point(0, 4));
        s.addPoint(new Point(0, 5));
        System.out.println(s.findNearest(new Point(0,0), 3));
    }
}
class Point {
    int x;
    int y;
    Point(int x, int y){
        this.x = x;
        this.y = y;
    }
    @Override
    public int hashCode(){
        return x * 10 + y;
    }
    @Override
    public boolean equals(Object o){
        Point other = (Point)o;
        return x == other.x && y == other.y;
    }
    public String toString() {
        return x + " : " + y;
    }
}


//

public class Solution {
    public List<Point> findKNearestPoints(List<Point> points, Point original, int k) {
        List<Point> result = new ArrayList<>();
         
        if (points == null || ponts.size() == 0 || original == null || k <= 0) {
            return result;
        }
         
        PriorityQueue<Point> pq = new PriorityQueue<>(k);
         
        for (Point point : points) {
            if (pq.size() < k) {
                pq.offer(point);
            } else {
                if (pq.peek().compareTo(point) > 0) {
                    pq.poll();
                    pq.offer(point);
                }
            }
        }
         
        result.addAll(pq);
        return result;
    }
     
    class Point implements Comparable<Point> {
        int x, y;
        double distance;
         
        public Point (int x, int y, Point original) {
            this.x= x;
            this.y = y;
             
            // sqrt(x^2 + y^2)
            distance = Math.hypot(x - original.x, y - original.y);
        }
         
        @Override
        public int compareTo(Point that) {
            return this.distance.compareTo(that.distance);
        }
    }
}