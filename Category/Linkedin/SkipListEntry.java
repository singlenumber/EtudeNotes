/**
 * Created by etude on 11/18/16.
 */
public class SkipListEntry {
    public Integer key;

    public int pos; //主要为了打印 链表用

    public SkipListEntry up, down, left, right; // 上下左右 四个指针

    public static int negInf = Integer.MIN_VALUE; // 负无穷
    public static int posInf = Integer.MAX_VALUE; // 正无穷

    public SkipListEntry(Integer k) {
        key = k;
        up = down = left = right = null;
    }

    public Integer getKey() {
        return key;
    }

    public Integer setKey(Integer newKey) {
        Integer oldValue = key;
        key = newKey;
        return oldValue;
    }

    public boolean equals(Object o) {
        SkipListEntry ent;
        try {
            ent = (SkipListEntry) o; // 检测类型
        } catch (ClassCastException ex) {
            return false;
        }
        return (ent.getKey() == key) && (ent.getKey() == key);
    }

    public String toString() {
        return "(" + key + ")";
    }
}