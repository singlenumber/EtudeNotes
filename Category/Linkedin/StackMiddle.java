public class Solution {
    public static void main(String[] args) {
        MyStack stack = new MyStack();
        stack.push(1);
        System.out.println(stack.getMiddle());
        stack.push(2);
        System.out.println(stack.getMiddle());
        stack.push(3);
        System.out.println(stack.getMiddle());
        stack.push(4);
        System.out.println(stack.getMiddle());
        stack.push(5);
        System.out.println(stack.getMiddle());
        stack.push(6);
        System.out.println(stack.getMiddle());
        stack.pop();
        System.out.println(stack.getMiddle());
    }

}

class MyStack {
    int count;
    ListNode mid;
    ListNode head;

    MyStack() {
        count = 0;
        mid = null;
        head = null;
    }

    public void push(int val) {
        ListNode newNode = new ListNode(val);
        newNode.prev = null;
        newNode.next = head;
        count++;

        if (count == 1) {
            mid = newNode;
        } else {
            head.prev = newNode;
            if (count % 2 == 1) {
                mid = mid.prev;
            }
        }
        head = newNode;
    }

    public int pop() {
        if (count == 0) {
            return -1;
        }
        int toRetrun = head.val;
        head = head.next;
        if (head != null) {
            head.prev = null;
        }
        count--;
        if (count % 2 != 1) {
            mid = mid.next;
        }
        return toRetrun;
    }

    public int getMiddle() {
        return mid.val;
    }
}

class ListNode {
    ListNode prev;
    ListNode next;
    int val;

    ListNode(int val) {
        this.val = val;
    }
}