import java.io.*;
import java.util.NoSuchElementException;

public class TextIterator {

    private final BufferedReader bufferedReader;

    private String cachedLine;

    private boolean finished = false;
    public TextIterator(final File file) throws FileNotFoundException {
        FileReader reader = new FileReader(file);
        if (reader == null) {
            throw new IllegalArgumentException("Reader must not be null");
        }
        bufferedReader = new BufferedReader(reader);
    }

    public boolean hasNext() {
        if (cachedLine != null) {
            return true;
        } else if (finished) {
            return false;
        } else {
            try {
                while (true) {
                    final String line = bufferedReader.readLine();
                    if (line == null) {
                        finished = true;
                        return false;
                    } else {
                        cachedLine = line;
                        return true;
                    }
                }
            } catch(final IOException ioe) {
                throw new IllegalStateException(ioe);
            }
        }
    }

    public String next() {
        if (!hasNext()) {
            throw new NoSuchElementException("No more lines");
        }
        final String currentLine = cachedLine;
        cachedLine = null;
        return currentLine;
    }
}
