import java.util.*;

public class Solution {
    public List<String> fullJustify(String[] words, int maxWidth) {
        List<String> res = new ArrayList<>();
        if (words == null || words.length == 0) return res;
        int count = 0;
        int last = 0;
        for (int i = 0; i < words.length; i++) {
            if (count + words[i].length() + i - last > maxWidth) {
                StringBuilder sb = new StringBuilder();
                for (int j = last; j < i; j++) {
                    sb.append(words[j]);
                    if (j < i - 1) {
                        sb.append(" ");
                    }
                }
                for (int j = sb.length(); j < maxWidth; j++) {
                    sb.append(" ");
                }
                res.add(sb.toString());
                count = 0;
                last = i;
            }
            count += words[i].length();

        }
        StringBuilder sb = new StringBuilder();
        for (int i = last; i < words.length; i++) {
            sb.append(words[i]);
            if (sb.length() < maxWidth) {
                sb.append(" ");
            }
        }
        for (int i = sb.length(); i < maxWidth; i++) {
            sb.append(" ");
        }
        res.add(sb.toString());
        return res;
    }

    public static void main(String[] args) {
        Solution s = new Solution();
        String[] arr = { "This", "is", "an", "example", "of", "text", "justification."};
        List<String> res = s.fullJustify(arr, 16);
        for (String str: res) {
            System.out.println("|" + str + "|");
        }
    }
}
