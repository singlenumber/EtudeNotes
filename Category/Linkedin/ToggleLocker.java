import java.util.*;

public class Solution {
    public int[] locker(int n) {
        int[] res = new int[n + 1];
        Arrays.fill(res, 0);
        for (int i = 1; i <= n; i++) {
            int j = 1;
            while (j < res.length) {
                if (j % i == 0) {
                    res[j] ^= 1;
                }
                j += 1;
            }
            for (j = 1; j <= n; j++) {
                System.out.print(res[j] + " ");
            }
            System.out.println();
        }
        return res;
    }

    public static void main(String[] args) {
        int[] res = new Solution().locker(5);
    }
}