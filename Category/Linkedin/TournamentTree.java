/*
         2
       /  \
    2       7
  /  \    /  \
 5    2  8    7
return 5 (second min)
 */

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode(int val) {
        this.val = val;
        left = null;
        right = null;
    }
}

public class Solution {
    public static int secMin(TreeNode root) {
        if (root.left == null && root.right == null) {
            return Integer.MAX_VALUE;
        }
        int L;
        int R;
        if (root.left.val == root.val) {
            L = secMin(root.left);
            R = root.right.val;
        } else {
            L = root.left.val;
            R = secMin(root.right);
        }
        return Math.min(L, R);
    }

    public static void main(String[] args) {
        TreeNode child1 = new TreeNode(2);
        child1.left = new TreeNode(5);
        child1.right = new TreeNode(2);

        TreeNode child2 = new TreeNode(7);
        child2.left = new TreeNode(8);
        child2.right = new TreeNode(7);

        TreeNode root = new TreeNode(2);
        root.left = child1;
        root.right = child2;
        int res = secMin(root);
        System.out.println(res);
    }
}