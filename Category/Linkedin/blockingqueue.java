import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by etude on 11/20/16.
 */
public class BlockingQueue {
    private final Lock lock = new ReentrantLock();
    private Condition full = lock.newCondition();
    private Condition empty = lock.newCondition();
    private Queue<Integer> elementQ;
    private int size;
    private int capacity;

    BlockingQueue(int size) {
        capacity = size;
        this.size = 0;
        elementQ = new LinkedList<>();
    }

    public void put(int elem) {
        lock.lock();
        try {
            while (size == capacity) {
                full.await();
            }
            elementQ.offer(elem);
            size++;
            empty.signalAll();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public int take() {
        lock.lock();
        try {
            while (size == 0) {
                empty.await();
            }
            int toReturn = elementQ.poll();
            size--;
            full.signalAll();
            return toReturn;
        } catch (Exception e) {
            return -1;
        } finally {
            lock.unlock();
        }
    }

    public static void main(String[] args) {
        BlockingQueue bq = new BlockingQueue(2);
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 4; i++) {
                    try {
                        System.out.println("Putting " + i);
                        bq.put(i);
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 5; i++) {
                    try {
                        System.out.println("Taking " + bq.take());
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

}



public interface BlockingQueue {
    /** Retrieve and remove the head of the queue, waiting if no elements
    are present. */
    T take();
 
    /** Add the given element to the end of the queue, waiting if necessary
    for space to become available. */
    void put (T obj);
}


public interface BlockedQueue <T> {
  BlockedQueue(int capacity);
  void put(T data);
  T poll();
}

public class myBlockedQueue <T> implements BlockedQueue {
    private Queue<T> myQueue;
    private int capacity = 20;

    public myBlockedQueue(int capacity) {
        myQueue = new LinkedList<>();
        this.capacity = capacity;
    }

    public synchronized void put (T data) 
            throws InterruptedException{
        while (myQueue.size() == capacity) {
            wait();  // this.wait();
        }

        myQueue.offer(data);
        notifyAll();  // this.notifyAll(); notify one of threads blocked at poll();
    }

    public synchronized T poll() 
            throws InterruptedException {
        while (myQueue.size() == 0) {
            wait();  // this.wait();
        }

        T data = myQueue.poll();
        notifyAll();  // this.notifyAll();  notify one of threads blocked at put();

        return data;
    }
}

import java.io.*;
import java.util.*;
 
public class BlockingQueue<T> {
    private Queue<T> queue;
    private int capacity = 10;
     
    // Constructor 
    public BlockingQueue(int capacity) {
        queue = new LinkedList<>();
        this.capacity = capacity;
    }
     
    // Add the given element to the end of the queue, 
    // Waiting if necessary for space to become available
    public synchronized void put(T obj) 
            throws InterruptedException {
        while (queue.size() == capacity) {
            wait();
        }
         
        queue.add(obj);
        notifyAll();
    }
     
    // Retrive and remove the head of the queue, 
    // waiting if no elements are present
    public synchronized T take() 
            throws InterruptedException {
        while (queue.size() == 0) {
            wait();
        }
         
        T obj = queue.poll();
        notifyAll();
         
        return obj;
    }
}

public class BlockingQueue implements Queue {
 
    private java.util.Queue queue = new java.util.LinkedList();
 
    /**
     * Make a blocking Dequeue call so that we'll only return when the queue has
     * something on it, otherwise we'll wait until something is put on it.
     * 
     * @returns  This will return null if the thread wait() call is interrupted.
     */
    public synchronized Object dequeue() {
        Object msg = null;
        while (queue.isEmpty()) {
            try {
                wait();
            } catch (InterruptedException e) {
                // Error return the client a null item
                return msg;
            }
        }
        msg = queue.remove();
        return msg;
    }
 
    /**
     * Enqueue will add an object to this queue, and will notify any waiting
     * threads that there is an object available.
     */
    public synchronized void enqueue(Object o) {
        queue.add(o);
        // Wake up anyone waiting for something to be put on the queue.
        notifyAll();
    }
 
}
