public class Solution {
    public static int count(int[] A, int K) {
        Arrays.sort(A);
        int beg = 0, end = A.length - 1;
        int res = 0;
        while (beg < end) {
            int sum = A[beg] + A[end];
            if (sum == K) {
                res++;
                while (beg < end && A[beg] == A[beg + 1]) {
                    beg++;
                }
                while (beg < end && A[end] == A[end - 1]) {
                    end--;
                }
                beg++; end--;
            } else if (sum < K) {
                beg++;
            } else {
                end--;
            }
        }
        return res;
    }


    public static void main(String[] args) {
        int[] A = {6,6,3,9,3,5,1};
        int[] B = {1,3,46,1,3,9};
        System.out.println(count(A, 12));
        System.out.println(count(B, 47));
    }
}
