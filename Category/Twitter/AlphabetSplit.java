public class Solution {
    public String alphaBetSplit(String str) {
        String res = "";
        String cur = String.valueOf(str.charAt(0));
        for (int i = 1; i < str.length(); i++) {
            if (str.charAt(i) > str.charAt(i - 1)) {
                cur += str.charAt(i);
                if (cur.length() > res.length()) {
                    res = cur;
                }
            } else {
                cur = String.valueOf(str.charAt(i));
            }
        }
        return res;
    }

    public static void main(String[] args) {
        String input1 = "enterprise";
        String input2 = "enterprisefghijk";
        Solution s = new Solution();
        System.out.println(s.alphaBetSplit(input1));
        System.out.println(s.alphaBetSplit(input2));
    }
}
