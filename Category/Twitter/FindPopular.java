public class Solution{

    public int findPopular1(int[] arr) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int e: arr) {
            Integer val = map.get(e);
            map.put(e, val == null ? 0: val + 1);
        }
        int res = 0;
        int max = 0;
        for (int e: map.keySet()) {
            if (map.get(e) > max) {
                max = map.get(e);
                res = e;
            }
        }
        return res;
    }

    public int findPopular2(int[] arr) {
        if (arr == null || arr.length == 0)
            return 0;

        Arrays.sort(arr);

        int previous = arr[0];
        int popular = arr[0];
        int count = 1;
        int maxCount = 1;

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] == previous)
                count++;
            else {
                if (count > maxCount) {
                    popular = arr[i-1];
                    maxCount = count;
                }
                previous = arr[i];
                count = 1;
            }
        }

        return count > maxCount ? arr[arr.length-1] : popular;
    }

    public static void main(String[] args) {
        int[] arr = {1, 1, 2, 2, 2, 3, 4, 4, 4, 4, 5};
        Solution s = new Solution();
        System.out.println(s.findPopular1(arr));
        System.out.println(s.findPopular2(arr));
    }
}