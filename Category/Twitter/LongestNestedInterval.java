public class Solution {

    public List<Interval> foo1(Interval[] arr) {
        List<Interval> res = new ArrayList<>();
        HashMap<Integer, List<Interval>> map = new HashMap<>();
        int max = 0;
        int[] dp = new int[arr.length];
        List<List<Interval>> dp_helper = new ArrayList<>();
        for (int i = 0; i < arr.length; i++) {
            dp[i] = 1;
            dp_helper.add(new ArrayList<>());
            dp_helper.get(i).add(arr[i]);
        }

        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i].beg < arr[j].beg && arr[i].end > arr[j].end) {
                    if (dp[i] + 1 > dp[j]) {
                        dp[j] = dp[i] + 1;
                        List<Interval> newList = new ArrayList<>(dp_helper.get(i));
                        newList.add(arr[j]);
                        dp_helper.set(j, newList);
                    }
                }
            }
            if (dp[i] > max) {
                max = dp[i];
                res = dp_helper.get(i);
            }
        }
        return res;
    }

    public List<Interval> foo2(Interval[] arr) {
        Arrays.sort(arr, (a, b) -> a.beg - b.beg);
        return foo1(arr);
    }

    public List<Interval> foo3() {
        List<Interval> res = new ArrayList<>();
        return res;
    }

    public static void main(String[] args) {
        Solution s = new Solution();
        Interval[] arr1 = new Interval[]{
                new Interval(3,4), new Interval(1,9), new Interval(2,8), new Interval(4,5), new Interval(11,12)
        };
        Interval[] arr2 = new Interval[] {
                new Interval(1,10), new Interval(3,4), new Interval(2,9), new Interval(3,8), new Interval(4,5), new Interval(11,12)
        };
        Interval[] arr3 = new Interval[] {
                new Interval(1,10), new Interval(3,4), new Interval(4,5), new Interval(11,12), new Interval(3,8), new Interval(2,9)
        };
        System.out.println(s.foo1(arr1));
        System.out.println(s.foo2(arr2));
    }
}

class Interval {
    int beg;
    int end;
    public Interval(int beg, int end) {
        this.beg = beg;
        this.end = end;
    }

    @Override
    public String toString() {
        return "[" + beg + "," + end + "]";
    }
}