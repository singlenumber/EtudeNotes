public class Solution {
    public int[] lookback(int[] height) {
        int[] res = new int[height.length];
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < height.length; i++) {
            if (stack.isEmpty()) {
                stack.push(height[i]);
                res[i] = height[i];
            } else {
                while (!stack.isEmpty() && height[i] < stack.peek()) {
                    stack.pop();
                }
                if (stack.empty()) {
                    res[i] = height[i];
                } else {
                    res[i] = stack.peek();
                }
                stack.push(height[i]);
            }
        }
        return res;
    }
    /*
    [3]
    [3, 5]
    [2]
    [2, 6]
    [2, 6, 9]
    [2, 6, 7]
    [2, 6, 7, 10]
    [3, 3, 2, 2, 6, 6, 7]
    */
    public static void main(String[] args) {
        int[] input = {3, 5, 2, 6, 9, 7, 10};
        int[] expect = {3, 3, 2, 2, 6, 6, 7};
        int[] res = new Solution().lookback(input);
        System.out.println(Arrays.toString(res));
    }
}