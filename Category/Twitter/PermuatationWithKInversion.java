public class Solution {
    /*
    For each permutation with numbers, we have positions where we can add the number . This means that we’ll have N new permutations.

    But how many inversions will each of these new permutations have? If we count the positions in an inverse order
    (e.g.: N = 6, POS = 5 4 3 2 1 0), the number of inversions will be the current number of inversions + the position:

    E.g:
    2 elements, 0 inversions: 1 2

    We add the number 3:

    1 2 3 --> inversions = 0 (0 + 0)
    1 3 2 --> inversions = 1 (0 + 1)
    3 1 2 --> inversions = 2 (0 + 2)
    2 elements, 1 inversions: 2 1

    We add the number 3:

    2 1 3 --> inversions = 1 (1 + 0)
    2 3 1 --> inversions = 2 (1 + 1)
    3 2 1 --> inversions = 3 (1 + 2)
    We sum up all the inversions:

    0 inversions = 1
    1 inversions = 1 + 1 = 2
    2 inversions = 1 + 1 = 2
    3 inversions = 1
     */
    public int PermuatationWithKInversion(int n, int k) {
        int[][] dp = new int[k + 1][n + 1];

        for (int i = 0; i <= k; i++)
            dp[i][0] = 0;
        for (int i = 0; i <= n; i++)
            dp[0][i] = 1;
        for (int i = 1; i <= k; i++) {
            for (int j = 1; j <= n; j++) {
                int m = i;
                dp[i][j] = 0;
                int l = 0;
                while (m >= 0 && l < j) {
                    dp[i][j] += dp[m][j - 1];
                    l++;
                    m--;
                }
            }
        }
        return dp[k][n];
    }

    public static void main(String[] args) {
        Solution s = new Solution();
        System.out.println(s.PermuatationWithKInversion(4, 6));
    }
}