public class Solution {
    public String shorten(String str) {
        if (str.length() <= 1) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str.charAt(0));
        int cnt = 1;
        for (int i = 1; i < str.length(); i++) {
            char c = str.charAt(i);
            if (c == str.charAt(i - 1)) {
                cnt++;
            } else {
                cnt = 1;
            }
            if (cnt <= 2) {
                sb.append(str.charAt(i));
            }
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        String input = "aaabaaccc";
        //aaabaaccc--> aabaacc
        System.out.println(new Solution().shorten(input));
    }
}