/*
input: 一个string，例如 the string "I want to count the frequency of the words in the input"
output:
'I.............1'
'want.......1'
'the.........3'
*/

public class Solution {
    public List<String> wordCount(String str) {
        String[] strs = str.split(" ");
        int maxLen = 0;
        List<String> res = new ArrayList<>();
        LinkedHashMap<String, Integer> map = new LinkedHashMap<>();
        for (String s: strs) {
            Integer count = map.get(s);
            if (count == null) {
                map.put(s, 1);
                count = 1;
            } else {
                map.put(s, map.get(s) + 1);
                count = map.get(s) + 1;
            }
            maxLen = Math.max(maxLen, s.length() + String.valueOf(count).length());
        }


        for (Map.Entry<String, Integer> e: map.entrySet()) {
            StringBuilder sb = new StringBuilder();
            String word = e.getKey();
            sb.append(word);
            int count = e.getValue();
            int curLen = word.length() + String.valueOf(count).length();
            for (int i = 0; i < maxLen - curLen; i++) {
                sb.append(".");
            }
            sb.append(count);
            res.add(sb.toString());
        }
        return res;
    }

    public static void main(String[] args) {
        String input = "I want to count the frequency of the words in the input";
        List<String> lists = new Solution().wordCount(input);
        for (String s: lists) {
            System.out.println(s);
        }
    }
}
