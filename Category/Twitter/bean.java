public class Solution {
    public double bean(int R, int W) {
        float[][] dp = new float[R + 1][W + 1];
        dp[R][W] = 1;
        for (int i = R - 1 ; i >= 0; i--) {
            float p = (float) (i + 1) / (i + 1 + W);
            dp[i][W] = (dp[i + 1][W] * p * p);
        }

        for (int i = W - 1; i >= 0; i--) {
            float p = (float) R /  (i + 1 + R);
            dp[R][i] = (1 - p * p) * dp[R][i + 1];
        }

        for (int i = R - 1; i >= 0; i--) {
            for (int j = W - 1; j >= 0; j--) {

                float p1 = (float) (i + 1) / (i + 1 + j);
                float p2 = (float) (i) / (i + j + 1);
                dp[i][j] = p1 * p1 * dp[i + 1][j] + (1 - p2 * p2) * dp[i][j + 1];


            }
        }
        for (int i = 0; i <= R; i++) {
            for (int j = 0; j <= W; j++ ) {
                System.out.print(i + "-" + j + ":" + dp[i][j] + "    ");
            }
            System.out.println();
        }
        return dp[0][1];

    }
    public static void main(String[] args) {
        Solution s = new Solution();
        System.out.println(s.bean(10, 10));
    }
}