public class Solution {
    public int findPrefix(String[] strs, String prefix) {
        int len = strs.length;
        int k = prefix.length();
        int beg = 0;
        int end = strs.length - 1;
        while (beg < end) {
            int mid = beg + (end - beg) / 2;
            String cur = strs[mid].substring(0, k);
            if (cur.compareTo(prefix) >= 0) {
                end = mid;
            } else {
                beg = mid + 1;
            }
        }
        if (strs[beg].substring(0, k).equals(prefix)) {
            return beg;
        }
        return -1;
    }

    public static void main(String[] args) {
        String[] strs = {"Ann", "App",  "Apple", "Boy"};
        Solution s = new Solution();
        System.out.println(s.findPrefix(strs, "Ap")); //1
        System.out.println(s.findPrefix(strs, "c")); //-1
        System.out.println(s.findPrefix(strs, "B")); //3
        System.out.println(s.findPrefix(strs, "Ann")); //3
    }
}