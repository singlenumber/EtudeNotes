public class Solution {
    public List<String> getTopN(String str, int n){
        String[] strs = str.split(" ");
        HashMap<String, Integer> map = new HashMap<>();
        for (String s: strs) {
            if (!map.containsKey(s)) {
                map.put(s, 1);
            } else {
                map.put(s, map.get(s) + 1);
            }
        }
        TreeMap<Integer, String> treemap = new TreeMap<>( (a, b) -> b - a);
        for (Map.Entry<String, Integer> e: map.entrySet()) {
                treemap.put(e.getValue(), e.getKey());
        }
        List<String> res = new ArrayList<>();
        for (Map.Entry<Integer, String> e: treemap.entrySet()) {
            n--;
            res.add(e.getValue());
            if (n == 0) {
                break;
            }
        }
        return res;
    }

    public static void main(String[] args) {
        String str = "a a b b c c beta beta beta alpha";
        Solution s = new Solution();
        System.out.println(s.getTopN(str, 2));
    }
}