/*
Sample Input 0

4 3
1 2 3 
Sample Output 0
4

Explanation 0 
[1,1,1,1]
[1,1,2]
[2,2]
[1,3]
Sample Input 1

10 4
2 5 3 6
Sample Output 1
5

Explanation 1
[2,2,2,2,2]
[2,2,3,3]
[2,2,6]
[2,3,5]
[5,5]
*/

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    public static long makeChange(int[] coins, int money) {
        long[] dp = new long[money + 1];
        dp[0] = 1;
        for (int coin: coins) {
            for (int i = 1; i <= money; i++) {
                if (i >= coin) {
                    dp[i] += dp[i - coin];
                }
            }
        }
        return dp[money];
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int m = in.nextInt();
        int coins[] = new int[m];
        for(int coins_i=0; coins_i < m; coins_i++){
            coins[coins_i] = in.nextInt();
        }
        System.out.println(makeChange(coins, n));
    }
}
