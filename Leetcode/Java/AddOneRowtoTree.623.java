public class Solution {
    public TreeNode addOneRow(TreeNode root, int v, int d) {
        if (d == 1) {
            TreeNode newRoot = new TreeNode(v);
            newRoot.left = root;
            return newRoot;
        }
        helper(root, v, d, 1);
        return root;
    }

    public void helper(TreeNode root, int v, int d, int cur) {
        if (root == null) {
            return;
        }
        if (cur == d - 1) {
            TreeNode leftTmp = root.left;
            root.left = new TreeNode(v);
            root.left.left = leftTmp;

            TreeNode rightTmp = root.right;
            root.right = new TreeNode(v);
            root.right.right = rightTmp;
            return;
        }
        helper(root.left, v, d, cur + 1);
        helper(root.right, v, d, cur + 1);
    }
}