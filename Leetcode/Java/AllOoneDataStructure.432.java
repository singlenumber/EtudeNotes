class AllOne {
    TreeMap<Integer, HashSet<String>> reversedIndex;
    Map<String, Integer> index;
    
    /** Initialize your data structure here. */
    public AllOne() {
        reversedIndex = new TreeMap<>();
        index = new HashMap<>();
    }
    
    /** Inserts a new key <Key> with value 1. Or increments an existing key by 1. */
    public void inc(String key) {
        if (!index.containsKey(key)) {
            index.put(key, 1);
            if (!reversedIndex.containsKey(1)) {
                reversedIndex.put(1, new HashSet<>());
            }
            reversedIndex.get(1).add(key);
        } else {
            int currentCount = index.get(key);
            reversedIndex.get(currentCount).remove(key);
            if (reversedIndex.get(currentCount).size() == 0) {
                reversedIndex.remove(currentCount);
            }
            if (!reversedIndex.containsKey(currentCount + 1)) {
                reversedIndex.put(currentCount + 1, new HashSet<>());
            }
            index.put(key, currentCount + 1);
            reversedIndex.get(currentCount + 1).add(key);
        }
    }
    
    /** Decrements an existing key by 1. If Key's value is 1, remove it from the data structure. */
    public void dec(String key) {
        if (index.containsKey(key)) {
            int currentCount = index.get(key);
            reversedIndex.get(currentCount).remove(key);
            if (reversedIndex.get(currentCount).size() == 0) {
                reversedIndex.remove(currentCount);
            }
            if (currentCount == 1) {
                index.remove(key);
            } else {
                if (!reversedIndex.containsKey(currentCount - 1)) {
                    reversedIndex.put(currentCount - 1, new HashSet<>());
                }
                reversedIndex.get(currentCount - 1).add(key);
                index.put(key, currentCount - 1);
            }
        }
    }
    
    /** Returns one of the keys with maximal value. */
    public String getMaxKey() {
        if (index.size() == 0) {
            return "";
        }
        return reversedIndex.get(reversedIndex.lastKey()).iterator().next();
    }
    
    /** Returns one of the keys with Minimal value. */
    public String getMinKey() {
        if (index.size() == 0) {
            return "";
        }
        return reversedIndex.get(reversedIndex.firstKey()).iterator().next();
    }
}

/**
 * Your AllOne object will be instantiated and called as such:
 * AllOne obj = new AllOne();
 * obj.inc(key);
 * obj.dec(key);
 * String param_3 = obj.getMaxKey();
 * String param_4 = obj.getMinKey();
 */