public class Solution {
    public int numberOfPatterns(int m, int n) {
        int res = 0;
        boolean[] vis = new boolean[10];
        int[][] blocks = new int[10][10];
        blocks[1][3] = blocks[3][1] = 2;
        blocks[1][7] = blocks[7][1] = 4;
        blocks[1][9] = blocks[9][1] = 5;
        blocks[2][8] = blocks[8][2] = 5;
        blocks[3][7] = blocks[7][3] = 5;
        blocks[3][9] = blocks[9][3] = 6;
        blocks[4][6] = blocks[6][4] = 5;
        blocks[7][9] = blocks[9][7] = 8;

        for(int i = m; i <= n; i++){
            res += dfs(blocks, vis, 1, i - 1) * 4;
            res += dfs(blocks, vis, 2, i - 1) * 4;
            res += dfs(blocks, vis, 5, i - 1);
        }
        return res;
    }
    public int dfs(int[][] blocks, boolean[] vis, int cur, int remain){
        if (remain < 0) {
            return 0;
        }
        if (remain == 0){
            return 1;
        }
        int res = 0;
        vis[cur] = true;
        for (int next = 1; next <= 9; next++) {
            int point = blocks[cur][next];
            if (!vis[next] && (point == 0 || vis[point] )) {
                res += dfs(blocks, vis, next, remain - 1);
            }
        }
        vis[cur] = false;
        return res;
    }
}