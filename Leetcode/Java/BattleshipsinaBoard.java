public class Solution {
    public int countBattleships(char[][] board) {
        if (board.length == 0) return 0;
        int M = board.length;
        int N = board[0].length;
        int res = 0;
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                if (board[i][j] == '.' || (i > 0 && board[i - 1][j] == 'X') || (j > 0 && board[i][j - 1] == 'X')) {
                    continue;
                }
                res++;
            }
        }
        return res;
    }
}

public class Solution {
    public int countBattleships(char[][] board) {
        if (board.length == 0) return 0;
        int M = board.length;
        int N = board[0].length;
        int res = 0;
        int[] dx = {0, 0, -1, 1};
        int[] dy = {-1, 1, 0, 0};
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                if (board[i][j] == 'X') {
                    res++;
                    board[i][j] = '.';
                    int curX = i;
                    int curY = j;
                    for (int k = 0; k < 4; k++) {
                        while (curX + dx[k] >= 0 && curX + dx[k] < M && curY + dy[k] >= 0 && curY + dy[k] < N && board[curX + dx[k]][curY + dy[k]] == 'X') {
                            board[curX + dx[k]][curY + dy[k]] = '.';
                            curX += dx[k];
                            curY += dy[k];
                        }
                    }
                }
            }
        }
        return res;
    }
}