class Solution {
    public int maxProfit(int k, int[] prices) {
        if (k <= 0 || prices == null || prices.length < 2) {
            return 0;
        }
        int n = prices.length;
        if (k >= n / 2) {
            int maxProfit = 0;
            for (int i = 1; i < n; i++) {
                if (prices[i] > prices[i - 1]) {
                    maxProfit += prices[i] - prices[i - 1];
                }
            }
            return maxProfit;
        }
        int[][] dp = new int[k + 1][n];
        for (int i = 1; i <= k; i++) {
            int curMax = - prices[0];
            for (int j = 1; j < n; j++) {
                dp[i][j] = Math.max(curMax + prices[j], dp[i][j - 1]);
                curMax = Math.max(curMax, dp[i - 1][j] - prices[j]);
            }
        }
        return dp[k][n - 1];
    }
}