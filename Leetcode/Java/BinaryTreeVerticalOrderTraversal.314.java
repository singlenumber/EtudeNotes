/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public List<List<Integer>> verticalOrder(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        if (root == null)
            return result;
        int[] range = new int[] {0, 0};
        getRange(root, range, 0);

        for (int i = range[0]; i <= range[1]; i++) {
            result.add(new ArrayList<>());
        }

        Queue<TreeNode> queue = new LinkedList<>();
        Queue<Integer> colQueue = new LinkedList<>();

        queue.offer(root);
        colQueue.offer(-range[0]);
        
        while (!queue.isEmpty()) {
            TreeNode node = queue.poll();
            int col = colQueue.poll();
            result.get(col).add(node.val);
            
            if (node.left != null) {
                queue.offer(node.left);
                colQueue.offer(col - 1);
            }
            if (node.right != null) {
                queue.offer(node.right);
                colQueue.offer(col + 1);
            }
        }
        return result;
    }

    public void getRange(TreeNode root, int[] range, int curCol) {
        if (root == null) {
            return;
        }
        range[0] = Math.min(range[0], curCol);
        range[1] = Math.max(range[1], curCol);

        getRange(root.left, range, curCol - 1);
        getRange(root.right, range, curCol + 1);
    }
}


public class Solution {
    public List<List<Integer>> verticalOrder(TreeNode root) {
        List<List<Integer>> res = new ArrayList<>();
        if (root == null) return res;
        Queue<TreeColNode> q = new LinkedList<>();
        TreeMap<Integer, List<Integer>> map = new TreeMap<>();
        q.offer(new TreeColNode(root, 0));
        int curLevel = 1;
        int nextLevel = 0;
        while (!q.isEmpty()) {
            for (int i = 0; i < curLevel; i++) {
                TreeColNode cur = q.poll();
                if (map.containsKey(cur.col)) {
                    map.get(cur.col).add(cur.node.val);
                } else {
                    map.put(cur.col, new ArrayList<>(Arrays.asList(cur.node.val)));
                }
                if (cur.node.left != null) {
                    q.offer(new TreeColNode(cur.node.left, cur.col - 1));
                    nextLevel++;
                }
                if (cur.node.right != null) {
                    q.offer(new TreeColNode(cur.node.right, cur.col + 1));
                    nextLevel++;
                }
            }
            curLevel = nextLevel;
            nextLevel = 0;
        }
        for (List<Integer> e: map.values()) {
            res.add(e);
        }
        return res;
    }
}
class TreeColNode {
    int col;
    TreeNode node;
    TreeColNode(TreeNode node, int col) {
        this.node = node;
        this.col = col;
    }
}