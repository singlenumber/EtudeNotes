public class Solution {
    public int maxKilledEnemies(char[][] grid) {
        if (grid.length == 0) {
            return 0;
        }
        int M = grid.length;
        int N = grid[0].length;
        int[] vertical = new int[N];
        int horizontal = 0;
        int res = 0;
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                if (grid[i][j] == 'W') {
                    continue;
                }
                if (i == 0 || grid[i - 1][j] == 'W') {
                    vertical[j] = getVertical(grid, i, j);
                }
                if (j == 0 || grid[i][j - 1] == 'W') {
                    horizontal = getHorizon(grid, i, j);
                }
                if (grid[i][j] == '0') {
                    res = Math.max(res, vertical[j] + horizontal);
                }
            }
        }
        return res;
    }
    public int getVertical(char[][] grid, int i, int j) {
        int num = 0;
        while (i < grid.length && grid[i][j] != 'W') {
            if (grid[i][j] == 'E') {
                num++;
            }
            i++;
        }
        return num;
    }
    public int getHorizon(char[][] grid, int i, int j) {
        int num = 0;
        while (j < grid[0].length && grid[i][j] != 'W') {
            if (grid[i][j] == 'E') {
                num++;
            }
            j++;
        }
        return num;
    }
}