public class Solution {
    public int maxCoins(int[] nums) {
        int len = nums.length;
        int[] newnums = new int[len + 2];
        newnums[0] = 1;
        newnums[len + 1] = 1;
        for (int i = 0; i < len; i++) {
            newnums[i + 1] = nums[i];
        }
        int[][] dp = new int[len + 2][len + 2];
        return helper(1, len, newnums, dp);
    }
    
    public int helper(int start, int end, int[] nums, int[][] dp) {
        if (dp[start][end] > 0) {
            return dp[start][end];
        }
        for (int i = start; i <= end; i++) {
            dp[start][end] = Math.max(dp[start][end], helper(start, i - 1, nums, dp) + 
                nums[start - 1] * nums[i] * nums[end + 1] + helper(i + 1, end, nums, dp));
        }
        return dp[start][end];
    }
}

public class Solution {
    public int maxCoins(int[] iNums) {
        int n = iNums.length;
        int[] nums = new int[n + 2];
        for (int i = 0; i < n; i++) nums[i + 1] = iNums[i];
        nums[0] = nums[n + 1] = 1;
        int[][] dp = new int[n + 2][n + 2];
        for (int k = 1; k <= n; k++) {
            for (int i = 1; i <= n - k + 1; i++) {
                int j = i + k - 1;
                for (int x = i; x <= j; x++) {
                    dp[i][j] = Math.max(dp[i][j], dp[i][x - 1] + nums[i - 1] * nums[x] * nums[j + 1] + dp[x + 1][j]);
                }
            }
        }
        return dp[1][n];
    }
}