/**
 * Definition for undirected graph.
 * class UndirectedGraphNode {
 *     int label;
 *     List<UndirectedGraphNode> neighbors;
 *     UndirectedGraphNode(int x) { label = x; neighbors = new ArrayList<UndirectedGraphNode>(); }
 * };
 */
public class Solution {
    public UndirectedGraphNode cloneGraph(UndirectedGraphNode node) {
        if (node == null) return null;
        Queue<UndirectedGraphNode> queue = new LinkedList<>();
        Map<UndirectedGraphNode, UndirectedGraphNode> map = new HashMap<>();
        UndirectedGraphNode copyRoot = new UndirectedGraphNode(node.label);
        map.put(node, copyRoot);
        queue.offer(node);
        while (!queue.isEmpty()) {
            UndirectedGraphNode cur = queue.poll();
            for (UndirectedGraphNode adj: cur.neighbors) {
                if (!map.containsKey(adj)) {
                    UndirectedGraphNode newnode = new UndirectedGraphNode(adj.label);
                    map.put(adj, newnode);
                    queue.offer(adj);
                }
                map.get(cur).neighbors.add(map.get(adj));
            }
        }
        return copyRoot;
    }
}

public class Solution {
    public UndirectedGraphNode cloneGraph(UndirectedGraphNode node) {
        if (node == null) return null;
        Stack<UndirectedGraphNode> stack = new Stack<>();
        Map<UndirectedGraphNode, UndirectedGraphNode> map = new HashMap<>();
        UndirectedGraphNode copyRoot = new UndirectedGraphNode(node.label);
        map.put(node, copyRoot);
        stack.push(node);
        while (!stack.isEmpty()) {
            UndirectedGraphNode cur = stack.pop();
            for (UndirectedGraphNode adj: cur.neighbors) {
                if (!map.containsKey(adj)) {
                    UndirectedGraphNode newnode = new UndirectedGraphNode(adj.label);
                    map.put(adj, newnode);
                    stack.push(adj);
                }
                map.get(cur).neighbors.add(map.get(adj));
            }
        }
        return copyRoot;
    }
}
