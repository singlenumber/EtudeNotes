/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    public List<Integer> closestKValues(TreeNode root, double target, int k) {
        LinkedList<Integer> res = new LinkedList<>();
        traverse(res, root, target, k);
        return res;
    }
    public void traverse(LinkedList<Integer> res, TreeNode cur, double target, int k){
        if (cur == null) {
            return;
        }
        traverse(res, cur.left, target, k);
        if (res.size() == k) {
            if (Math.abs(cur.val - target) < Math.abs(res.peekFirst() - target)) {
                res.removeFirst();
            } else {
                return;
            }
        }
        res.add(cur.val);
        traverse(res, cur.right, target, k);
    }
}