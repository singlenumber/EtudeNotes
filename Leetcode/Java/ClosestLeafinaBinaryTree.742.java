/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public int findClosestLeaf(TreeNode root, int k) {
        Map<TreeNode, List<TreeNode>> g = new HashMap<>();
        dfs(g, root, null);
        Queue<TreeNode> queue = new LinkedList<>();
        Set<TreeNode> set = new HashSet<>();
        
        for (TreeNode node: g.keySet()) {
            if (node != null && node.val == k) {
                queue.offer(node);
                set.add(node);
            }
        }
        
        while (!queue.isEmpty()) {
            TreeNode node = queue.poll();
            if (node != null) {
                if (g.get(node).size() <= 1) {
                    return node.val;
                }
                for (TreeNode neighbor: g.get(node)) {
                    if (!set.contains(neighbor)) {
                        set.add(neighbor);
                        queue.offer(neighbor);
                    }
                }
            }
        }
        throw null;
    }

    private void dfs(Map<TreeNode, List<TreeNode>> g, TreeNode node, TreeNode parent) {
        if (node != null) {
            if (!g.containsKey(node)) {
                g.put(node, new ArrayList<>());
            }
            if (!g.containsKey(parent)) {
                g.put(parent, new ArrayList<>());
            }
            g.get(node).add(parent);
            g.get(parent).add(node);
            dfs(g, node.left, node);
            dfs(g, node.right, node);
        }
    }
}