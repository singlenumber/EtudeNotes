public class Solution {
    public List<List<Integer>> combinationSum3(int k, int n) {
        List<List<Integer>> res = new ArrayList<>();
        List<Integer> list = new ArrayList<>();
        helper(res, list, k, n, 1, 0);
        return res;
    }
    
    public void helper(List<List<Integer>> res, List<Integer> list, int k, int n, int pos, int sum) {
        if (sum > n) {
            return;
        }
        if (list.size() == k && sum == n) {
            res.add(new ArrayList<>(list));
            return;
        }
        for (int i = pos; i <= 9; i++) {
            list.add(i);
            helper(res, list, k, n, i + 1, sum + i);
            list.remove(list.size() - 1);
        }
    }
}