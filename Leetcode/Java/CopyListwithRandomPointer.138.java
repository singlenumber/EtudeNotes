/**
 * Definition for singly-linked list with a random pointer.
 * class RandomListNode {
 *     int label;
 *     RandomListNode next, random;
 *     RandomListNode(int x) { this.label = x; }
 * };
 */
public class Solution {
    public RandomListNode copyRandomList(RandomListNode head) {
        if (head == null) {
            return head;
        }
        RandomListNode cur = head;
        while (cur != null) {
            RandomListNode newnode = new RandomListNode(cur.label);
            newnode.next = cur.next;
            cur.next = newnode;
            cur = newnode.next;
        }
        cur = head;
        while (cur != null) {
            if (cur.random != null) {
                cur.next.random = cur.random.next;
            }
            cur = cur.next.next;
        }
        RandomListNode newHead = head.next;
        RandomListNode p = head;
        RandomListNode q = head.next;
        while (p != null) {
            p.next = q.next;
            if (p.next != null) {
                q.next = p.next.next;
            }
            p = p.next;
            q = q.next;
        }
        return newHead;
    }
}

public class Solution {
    public RandomListNode copyRandomList(RandomListNode head) {
        if (head == null) {
            return head;
        }
        Map<RandomListNode, RandomListNode> map = new HashMap<>();
        RandomListNode newHead = new RandomListNode(head.label);
        RandomListNode cur1 = head;
        RandomListNode cur2 = newHead;
        map.put(head, newHead);
        while (cur1.next != null) {
            cur1 = cur1.next;
            cur2.next = new RandomListNode(cur1.label);
            cur2 = cur2.next;
            map.put(cur1, cur2);
        }
        cur1 = head;
        cur2 = newHead;
        while (cur1 != null) {
            if (cur1.random != null) {
                cur2.random = map.get(cur1.random);
            }
            cur1 = cur1.next;
            cur2 = cur2.next;
        }
        return newHead;
    }
}
