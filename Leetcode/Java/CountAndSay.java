/*
 * The count-and-say sequence is the sequence of integers beginning as follows:
 * 1, 11, 21, 1211, 111221, ...

 * 1 is read off as "one 1" or 11.
 * 11 is read off as "two 1s" or 21.
 * 21 is read off as "one 2, then one 1" or 1211.

 * Given an integer n, generate the nth sequence.

 * Note: The sequence of integers will be represented as a string.
 */

public class Solution {
    public String countAndSay(int n) {
        String res = "1";
        for (int j = 1; j < n; j++) {
            String cur = "";
            for (int i = 0; i < res.length(); i++) {
                int cnt = 1;
                while (i + cnt < res.length() && res.charAt(i) == res.charAt(i + cnt)) {
                    cnt++;
                }
                cur += cnt + "" + res.charAt(i);
                i = i + cnt - 1;
            }
            res = cur;
        }
        return res;
    }
}

public class Solution {
    public String countAndSay(int n) {
        String res = "1";
        for (int i = 1; i < n; i++) {
            String cur = "";
            int cnt = 1;
            for (int j = 0; j < res.length(); j += cnt) {
                cnt = 1;
                while (j + cnt < res.length() && res.charAt(j) == res.charAt(j + cnt)) {
                    cnt++;
                }
                cur = cur + cnt + res.charAt(j);
            }
            res = cur;
        }
        return res;
    }
}

public class Solution {
    public String countAndSay(int n) {
        if (n <= 0)
            return null;
        String res = "1";
        int num = 1;
        for (int j = 1; j < n; j++) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < res.length(); i++) {
                if (i < res.length() - 1 && res.charAt(i) == res.charAt(i + 1)) {
                    num++;
                } else {
                    sb.append(num + "" + res.charAt(i));
                    num = 1;
                }
            }
            res = sb.toString();
        }
        return res;
    }
}

