public class Solution {
    public boolean canFinish(int numCourses, int[][] prerequisites) {
        Map<Integer, Set<Integer>> g = new HashMap<>();
        for (int[] pre: prerequisites) {
            int a = pre[0];
            int b = pre[1];
            if (!g.containsKey(a)) {
                g.put(a, new HashSet<>());
            }
            if (!g.containsKey(b)) {
                g.put(b, new HashSet<>());
            }
            g.get(a).add(b);
        }
        Set<Integer> isVisited = new HashSet<>();
        for (Integer node: g.keySet()) {
            if (isVisited.contains(node)) {
                continue;
            }
            if (dfs(g, isVisited, new HashSet<Integer>(), node)) {
                return false;
            }
        }
        return true;
    }

    public boolean dfs(Map<Integer, Set<Integer>> g, Set<Integer> isVisited, Set<Integer> path, int node) {
        path.add(node);
        isVisited.add(node);
        for (int adj: g.get(node)) {
            if (path.contains(adj)) {
                return true;
            }
            if (isVisited.contains(adj)) {
                continue;
            }
            if (dfs(g, isVisited, path, adj)) {
                return true;
            }
        }
        path.remove(node);
        return false;
    }
}
//BFS
public boolean canFinish(int numCourses, int[][] prerequisites) {
    if (numCourses == 0 || prerequisites.length == 0) {
        return true;
    }
    int preCount[] = new int[numCourses];
    HashMap<Integer, List<Integer>> dependentMap = new HashMap<Integer, List<Integer>>();
    for (int[] item : prerequisites) {
        preCount[item[0]]++;
        if (dependentMap.containsKey(item[1])) {
            dependentMap.get(item[1]).add(item[0]);
        } else {
            List<Integer> list = new ArrayList<Integer>();
            list.add(item[0]);
            dependentMap.put(item[1], list);
        }
    }
    int freeCount = 0;
    Queue<Integer> queue = new LinkedList<Integer>();
    for (int i = 0; i < numCourses; i++) {
        if (preCount[i] == 0) {
            queue.offer(i);
        }
    }
    while (!queue.isEmpty()) {
        int free = queue.poll();
        freeCount++;
        if (dependentMap.get(free) != null) {
            for (int dependent : dependentMap.get(free)) {
                preCount[dependent]--;
                if (preCount[dependent] == 0) {
                    queue.offer(dependent);
                }
            }
        }
    }
    return freeCount == numCourses;
}
//Kahn
public class Solution {
    public boolean canFinish(int numCourses, int[][] prerequisites) {
        // BFS topological sorting with Kahn's algorithm
        // if cycle is detected, return false
        int[] indegree = new int[numCourses];
        for (int[] p : prerequisites) {
            indegree[p[0]]++;
        }
        // queue for nodes with indegree == 0
        LinkedList<Integer> queue = new LinkedList<Integer>();
        for (int i = 0; i < indegree.length; i++) {
            if (indegree[i] == 0) {
                queue.offer(i);
            }
        }
        int canFinish = 0;
        while (queue.size() != 0) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                Integer curr = queue.poll();
                canFinish++;
                for (int[] p : prerequisites) {
                    if (p[1] == curr) {
                        indegree[p[0]]--;
                        if (indegree[p[0]] == 0) {
                            queue.offer(p[0]);
                        }
                    }
                }
            }
        }
        return canFinish == numCourses;
    }
}
//tarjan
public class Solution {
    public boolean canFinish(int numCourses, int[][] prerequisites) {
        // 0 for white, unvisited
        // 1 for grey, in stack
        // 2 for black, fully explored
        int[] status = new int[numCourses];
        for (int i = 0; i < numCourses; i++) {
            if (status[i] == 0) {
                if (!DFS(i, prerequisites, status)) {
                    return false;
                }
            }
        }
        return true;
    }
    
    private boolean DFS(int curr, int[][] prerequisites, int[] status) {
        if (status[curr] == 1) {
            return false;
        }
        status[curr] = 1;
        for (int[] p : prerequisites) {
            if (p[0] == curr && status[p[1]] != 2) {
                if (!DFS(p[1], prerequisites, status)) {
                    return false;
                }
            }
        }
        status[curr] = 2;
        return true;
    }
}