/**
 * Definition for an interval.
 * public class Interval {
 *     int start;
 *     int end;
 *     Interval() { start = 0; end = 0; }
 *     Interval(int s, int e) { start = s; end = e; }
 * }
 */
public class SummaryRanges {
    TreeMap<Integer, Interval> map;

    public SummaryRanges() {
        map = new TreeMap<>();
    }

    public void addNum(int val) {
        if (map.containsKey(val)) {
            return;
        }
        Integer L = map.lowerKey(val);
        Integer R = map.higherKey(val);
        if (L != null && R != null && map.get(L).end + 1 == val && map.get(R).start == val + 1) {
            map.get(L).end = map.get(R).end;
            map.remove(R);
        } else if (L != null && map.get(L).end + 1 >= val) {
            map.get(L).end = Math.max(map.get(L).end, val);
        } else if (R != null && map.get(R).start - 1 == val) {
            map.put(val, new Interval(val, map.get(R).end));
            map.remove(R);
        } else {
            map.put(val, new Interval(val, val));
        }
    }

    public List<Interval> getIntervals() {
        return new ArrayList<>(map.values());
    }
}

/**
 * Your SummaryRanges object will be instantiated and called as such:
 * SummaryRanges obj = new SummaryRanges();
 * obj.addNum(val);
 * List<Interval> param_2 = obj.getIntervals();
 */