/*
 * A message containing letters from A-Z is being encoded to numbers using the 
 * following mapping:

 * 'A' -> 1
 * 'B' -> 2
 * ...
 * 'Z' -> 26
 * Given an encoded message containing digits, determine the total number of 
 * ways to decode it.

 * For example,
 * Given encoded message "12", it could be decoded as "AB" (1 2) or "L" (12).

 * The number of ways decoding "12" is 2.
 */
/* "0" if only singele "0 "no solution
   if i=0 i-1=1 or i-1=2 still has solution dp[i+1] = dp[i-1]
   if i!=0 dp[i+1]=dp[i] and if 10-26 dp[i+1]+=dp[i-1]
*/

// O(n) time; O(n) space.
public class Solution {
    public int numDecodings(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        int[] dp = new int[s.length() + 1];
        dp[0] = 1;
        if (s.charAt(0) != '0') {
            dp[1] = 1;
        }
        for (int i = 2; i <= s.length(); i++) {
            if (s.charAt(i - 1) != '0') {
                dp[i] += dp[i - 1];
            }
            if (s.charAt(i - 2) == '1' || (s.charAt(i - 2) == '2' && s.charAt(i - 1) <= '6')) {
                dp[i] += dp[i - 2];
            }
        }
        return dp[s.length()];
    }
}

// O(n) time; O(1) space.
public class Solution {
    public int numDecodings(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        int prev = 1;
        int cur = 0;
        if (s.charAt(0) != '0') {
            cur = 1;
        }
        for (int i = 2; i <= s.length(); i++) {
            int next = 0;
            if (s.charAt(i - 1) != '0') {
                next += cur;
            }
            if (s.charAt(i - 2) == '1' || (s.charAt(i - 2) == '2' && s.charAt(i - 1) <= '6')) {
                next += prev;
            }
            prev = cur;
            cur = next;
        }
        return cur;
    }
}
