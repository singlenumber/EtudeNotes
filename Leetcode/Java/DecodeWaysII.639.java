class Solution {
    public int numDecodings(String s) {
        int M = 1000000007;
        int N = s.length();
        long[] dp = new long[N + 1];
        dp[0] = 1;
        dp[1] = s.charAt(0) == '*' ? 9 : s.charAt(0) != '0' ? 1 : 0;

        for (int i = 2; i <= N; i++) {
            if (s.charAt(i - 1) == '0') {
                if (s.charAt(i - 2) == '1' || s.charAt(i - 2) == '2') {
                    dp[i] += dp[i - 2];
                } else if (s.charAt(i - 2) == '*') {
                    dp[i] += dp[i - 2] * 2;
                } else {
                    return 0;
                }
            } else if (s.charAt(i - 1) >= '1' && s.charAt(i - 1) <= '9') {
                dp[i] += dp[i - 1];
                if (s.charAt(i - 2) == '1' || (s.charAt(i - 2) == '2' && s.charAt(i - 1) <= '6') ) {
                    dp[i] += dp[i - 2];
                } else if (s.charAt(i - 2) == '*') {
                    dp[i] += s.charAt(i - 1) <= '6' ? (2 * dp[i - 2]) : dp[i - 2];
                }
            } else if (s.charAt(i - 1) == '*') {
                dp[i] += dp[i - 1] * 9;
                if (s.charAt(i - 2) == '1') {
                    dp[i] += dp[i - 2] * 9;
                } else if (s.charAt(i - 2) == '2') {
                    dp[i] += dp[i - 2] * 6;
                } else if (s.charAt(i - 2) == '*') {
                    dp[i] += dp[i - 2] * 15;
                }
            }
            dp[i] %= M;
        }
        return (int)dp[N];
    }
}