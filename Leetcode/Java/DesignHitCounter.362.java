class HitCounter {
    List<Integer> list;
    /** Initialize your data structure here. */
    public HitCounter() {
        list = new ArrayList<>();
    }

    /** Record a hit.
     @param timestamp - The current timestamp (in seconds granularity). */
    public void hit(int timestamp) {
        list.add(timestamp);
    }

    /** Return the number of hits in the past 5 minutes.
     @param timestamp - The current timestamp (in seconds granularity). */
    public int getHits(int timestamp) {
        List<Integer> newList = new ArrayList<>();
        for (Integer e: list) {
            if (timestamp - e < 300) {
                newList.add(e);
            }
        }
        list = newList;
        return list.size();
    }
}

class HitCounter {
    Queue<Integer> queue;
    /** Initialize your data structure here. */
    public HitCounter() {
        queue = new LinkedList<>();
    }

    /** Record a hit.
     @param timestamp - The current timestamp (in seconds granularity). */
    public void hit(int timestamp) {
        queue.offer(timestamp);
    }

    /** Return the number of hits in the past 5 minutes.
     @param timestamp - The current timestamp (in seconds granularity). */
    public int getHits(int timestamp) {
        while (!queue.isEmpty() && (timestamp - queue.peek()) >= 300 ) {
            queue.poll();
        }
        return queue.size();
    }
}