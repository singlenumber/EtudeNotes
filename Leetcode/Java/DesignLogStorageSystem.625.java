public class LogSystem {
    private List<String[]> logs;
    private List<String> granularity;
    int[] indices;
    public LogSystem() {
        logs = new ArrayList<>();
        granularity = Arrays.asList("Year", "Month", "Day", "Hour", "Minute", "Second");
        indices = new int[]{4,7,10,13,16,19};
    }

    public void put(int id, String timestamp) {
        logs.add(new String[]{
                Integer.toString(id),
                timestamp
        });
    }

    public List<Integer> retrieve(String s, String e, String gra) {
        List<Integer> result = new ArrayList<>();
        int substringIndex = indices[granularity.indexOf(gra)];
        for (String[] log: logs) {
            if (log[1].substring(0, substringIndex).compareTo(s.substring(0, substringIndex)) >= 0 &&
                log[1].substring(0, substringIndex).compareTo(e.substring(0, substringIndex)) <= 0) {
                result.add(Integer.parseInt(log[0]));
            }
        }
        return result;
    }
}

/**
 * Your LogSystem object will be instantiated and called as such:
 * LogSystem obj = new LogSystem();
 * obj.put(id,timestamp);
 * List<Integer> param_2 = obj.retrieve(s,e,gra);
 */