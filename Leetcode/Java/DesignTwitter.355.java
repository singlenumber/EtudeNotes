public class Twitter {
    private static int timestamp = 0;
    Map<Integer, User> userMap;
    private class Tweet {
        int tweetId;
        int time;
        Tweet next;
        Tweet(int tweetId) {
            this.tweetId = tweetId;
            time = timestamp++;
            next = null;
        }
    }

    private class User {
        int id;
        Set<Integer> followed;
        Tweet head;
        User(int id) {
            this.id = id;
            followed = new HashSet<>();
            followed.add(id);
            head = null;
        }

        void follow(int id) {
            followed.add(id);
        }

        void unfollow(int id) {
            followed.remove(id);
        }

        void post(int tweetId) {
            Tweet newTweet = new Tweet(tweetId);
            newTweet.next = head;
            head = newTweet;
        }
    }

    /** Initialize your data structure here. */
    public Twitter() {
        userMap = new HashMap<>();
    }

    /** Compose a new tweet. */
    public void postTweet(int userId, int tweetId) {
        if (!userMap.containsKey(userId)) {
            User newUser = new User(userId);
            userMap.put(userId, newUser);
        }
        userMap.get(userId).post(tweetId);
    }

    /** Retrieve the 10 most recent tweet ids in the user's news feed. Each item in the news feed must be posted by users who the user followed or by the user herself. Tweets must be ordered from most recent to least recent. */
    public List<Integer> getNewsFeed(int userId) {
        List<Integer> res = new ArrayList<>();
        if (!userMap.containsKey(userId)) {
            return res;
        }
        Set<Integer> users = userMap.get(userId).followed;
        PriorityQueue<Tweet> maxHeap = new PriorityQueue<>(users.size(), (a, b) -> (b.time - a.time));
        for (int user: users) {
            Tweet tweet = userMap.get(user).head;
            if (tweet != null) {
                maxHeap.offer(tweet);
            }
        }
        int N = 0;
        while (maxHeap.size() != 0 && N < 10) {
            Tweet t = maxHeap.poll();
            res.add(t.tweetId);
            if (t.next != null) {
                maxHeap.offer(t.next);
            }
            N++;
        }
        return res;
    }

    /** Follower follows a followee. If the operation is invalid, it should be a no-op. */
    public void follow(int followerId, int followeeId) {
        if (!userMap.containsKey(followerId)) {
            User u = new User(followerId);
            userMap.put(followerId, u);
        }
        if (!userMap.containsKey(followeeId)) {
            User u = new User(followeeId);
            userMap.put(followeeId, u);
        }
        userMap.get(followerId).follow(followeeId);
    }

    /** Follower unfollows a followee. If the operation is invalid, it should be a no-op. */
    public void unfollow(int followerId, int followeeId) {
        if (!userMap.containsKey(followerId) || followerId == followeeId)
            return;
        userMap.get(followerId).unfollow(followeeId);
    }
}

/**
 * Your Twitter object will be instantiated and called as such:
 * Twitter obj = new Twitter();
 * obj.postTweet(userId,tweetId);
 * List<Integer> param_2 = obj.getNewsFeed(userId);
 * obj.follow(followerId,followeeId);
 * obj.unfollow(followerId,followeeId);
 */