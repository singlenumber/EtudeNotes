/*
 * Given two words word1 and word2, find the minimum number of steps required to convert word1 to word2. 
 * (each operation is counted as 1 step.)

 * You have the following 3 operations permitted on a word:

 * a) Insert a character
 * b) Delete a character
 * c) Replace a character
 */
/*dp[i][j] means how many steps from length i str to length j string
  xxxxa,yyyyb 1.a==b xxxx->yyyy
              2.a!=b (1) xxxx->yyyy + b = i-1 j-1 + 1
                     (2) xxxxa->yyyy + b = i j-1 + 1
                     (3) xxxxa del a + xxxx->yyyyb = i-1 j + 1
*/
public class Solution {
    public int minDistance(String word1, String word2) {
        if (word1 == null && word2 == null) {
            return 0;
        }
        int M = word1.length();
        int N = word2.length();
        if (M == 0) return N;
        if (N == 0) return M;
        int[][] dp = new int[M + 1][N + 1];
        for (int i = 0; i <= M; i++) {
            for (int j = 0; j <= N; j++) {
                if (i == 0) {
                    dp[i][j] = j;
                } else if (j == 0) {
                    dp[i][j] = i;
                } else {
                    if (word1.charAt(i - 1) == word2.charAt(j - 1)) {
                        dp[i][j] = dp[i - 1][j - 1];
                    } else {
                        dp[i][j] = Math.min(dp[i - 1][j - 1], Math.min(dp[i - 1][j], dp[i][j - 1])) + 1;
                    }
                }
            }
        }
        return dp[M][N];
    }
}

public class Solution {
    public int minDistance(String word1, String word2) {
        int N = word2.length();
        int[] dp = new int[N + 1];
        for (int i = 0; i <= word2.length(); i++) {
            dp[i] = i;
        }
        for (int i = 1; i <= word1.length(); i++) {
            int prev = dp[0];
            dp[0] = i;
            for (int j = 1; j <= word2.length(); j++) {
                int delete = 1 + dp[j];
                int insert = 1 + dp[j - 1];
                int replace = prev + (word1.charAt(i - 1) == word2.charAt(j - 1) ? 0: 1);
                prev = dp[j];
                dp[j] = Math.min(delete, Math.min(insert, replace)); 
            }
        }
        return dp[N];
    }
}
