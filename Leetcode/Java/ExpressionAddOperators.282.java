public class Solution {
    public List<String> addOperators(String num, int target) {
        List<String> result = new ArrayList<>();
        if (num == null || num.length() == 0) return result;
        helper(result, target, num, "", 0, 0, 0);
        return result;
    }

    public void helper(List<String> result, int target, String num, String cur, int pos, long curVal, long preVal){
        if (pos == num.length()) {
            if (target == curVal) {
                result.add(cur);
            }
            return;
        }
        for (int i = pos; i < num.length(); i++) {
            if (i != pos && num.charAt(pos) == '0') break;
            long value = Long.parseLong(num.substring(pos, i + 1));
            if (pos == 0) {
                helper(result, target, num, cur + value, i + 1, value, value);
            } else {
                helper(result, target, num, cur + "+" + value, i + 1, curVal + value, value);
                helper(result, target, num, cur + "-" + value, i + 1, curVal - value, -value);
                helper(result, target, num, cur + "*" + value, i + 1, curVal - preVal + preVal * value, preVal * value);
            }
        }
    }
}