public class Solution {
    public List<Integer> findAnagrams(String s, String p) {
        List<Integer> res = new ArrayList<>();
        if (s == null || p == null || s.length() == 0 || p.length() == 0) {
            return res;
        }
        int[] map = new int[256];
        for (Character c: p.toCharArray()) {
            map[c]++;
        }
        int right = 0;
        int left = 0;
        int count = p.length();
        while (right < s.length()) {
            if (map[s.charAt(right++)]-- >= 1) {
                count--;
            }
            if (count == 0) {
                res.add(left);
            }
            if (right - left == p.length() && map[s.charAt(left++)]++ >= 0) {
                count++;
            }
        }
        return res;
    }
}