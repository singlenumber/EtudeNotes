/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    public List<Integer> largestValues(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        dfs(result, root, 0);
        return result;
    }
    public void dfs(List<Integer> result, TreeNode cur, int deep) {
        if (cur == null) {
            return;
        }
        if (deep == result.size()) {
            result.add(cur.val);
        } else {
            result.set(deep, Math.max(result.get(deep), cur.val));
        }
        dfs(result, cur.left, deep + 1);
        dfs(result, cur.right, deep + 1);
    }
}

public class Solution {
    public List<Integer> largestValues(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (queue.size() != 0) {
            int curSize = queue.size();
            int maxNum = Integer.MIN_VALUE;
            for (int i = 0; i < curSize; i++) {
                TreeNode curNode = queue.poll();
                maxNum = Math.max(maxNum, curNode.val);
                if (curNode.left != null) {
                    queue.offer(curNode.left);
                }
                if (curNode.right != null) {
                    queue.offer(curNode.right);
                }
            }
            result.add(maxNum);
        }
        return result;
    }
}