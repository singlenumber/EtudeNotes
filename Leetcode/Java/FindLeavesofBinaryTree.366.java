/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public List<List<Integer>> findLeaves(TreeNode root) {
        List<List<Integer>> res = new ArrayList<>();
        height(root, res);
        return res;
    }

    private int height(TreeNode node, List<List<Integer>> list) {
        if (node == null) {
            return 0;
        }
        int level = 1 + Math.max(height(node.left, list), height(node.right, list));
        if (list.size() < level) {
            list.add(new ArrayList<>());
        }
        list.get(level - 1).add(node.val);
        return level;
    }
}