public class Solution {
    public int findMin(int[] nums) {
        int beg = 0;
        int end = nums.length - 1;
        while (beg < end) {
            int mid = beg + (end - beg) / 2;
            if (nums[beg] < nums[end]) {
                return nums[beg];
            }
            if (nums[beg] < nums[mid]) {
                beg = mid + 1;
            } else if (nums[beg] > nums[mid]) {
                end = mid;
            } else {
                beg++;
            }
        }
        return nums[beg];
    }
}
