/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    int max = 0;
    Map<Integer, Integer> map = new HashMap<>();
    public int[] findMode(TreeNode root) {
        if (root == null) {
            return new int[0];
        }
        inOrder(root);
        List<Integer> list = new ArrayList<>();
        for (Integer e: map.keySet()) {
            if (map.get(e) == max) {
                list.add(e);
            }
        }
        int[] result = new int[list.size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = list.get(i);
        }
        return result;
    }

    public void inOrder(TreeNode p) {
        if (p != null) {
            map.put(p.val, map.getOrDefault(p.val, 0) + 1);
            max = Math.max(max, map.get(p.val));
            inOrder(p.left);
            inOrder(p.right);
        }
    }
}