class Solution {
    public int pivotIndex(int[] nums) {
        int left = 0, sum = 0;
        for (Integer e: nums) {
            sum += e;
        }
        for (int i = 0; i < nums.length; i++) {
            if (i != 0) {
                left += nums[i - 1];
            }
            if (sum - left - nums[i] == left) {
                return i;
            }
        }
        return -1;
    }
}


class Solution {
    public int pivotIndex(int[] nums) {
        if (nums == null || nums.length < 2) {
            return -1;
        }
        int len = nums.length;
        int[] left = new int[len];
        int[] right = new int[len];
        left[0] = 0;
        right[len - 1] = 0;
        for (int i = 1; i < len; i++) {
            left[i] = left[i - 1] + nums[i - 1];
            right[len - 1 - i] = right[len - i] + nums[len - i];
        }
        for (int i = 0; i < len; i++) {
            if (left[i] == right[i]) {
                return i;
            }
        }
        return -1;
    }
}