/* The knows API is defined in the parent class Relation.
      boolean knows(int a, int b); */
public class Solution extends Relation {
    public int findCelebrity(int n) {
        boolean[] can = new boolean[n];
        Arrays.fill(can, true);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (can[i] && i != j) {
                    if (knows(i, j) || !knows(j, i)) {
                        can[i] = false;
                        break;
                    } else {
                        can[j] = false;
                    }
                }
            }
            if (can[i]) {
                return i;
            }
        }
        return -1;
    }
}

public class Solution extends Relation {
    public int findCelebrity(int n) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i != j && (knows(i, j) || !knows(j, i))) {
                    break;
                }
                if (j == n - 1) {
                    return i;
                }
            }
        }
        return -1;
    }
}

public class Solution extends Relation {
    public int findCelebrity(int n) {
        int candidate = 0;
        for (int i = 0; i < n; i++) {
            if (knows(candidate, i)) {
                candidate = i;
            }
        }
        for (int i = 0; i < n; i++) {
            if (candidate == i) continue;
            if (knows(candidate, i) || !knows(i, candidate)) return -1;
        }
        return candidate;
    }
}
