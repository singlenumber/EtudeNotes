public class Solution {
    public char findTheDifference(String s, String t) {
        int[] map = new int[26];
        for (Character c: s.toCharArray()) {
            map[c - 'a']++;
        }
        for (Character c: t.toCharArray()) {
            int val = --map[c - 'a'];
            System.out.println(val);
            if (val < 0) {
                return c;
            }
        }
        return ' ';
    }
}