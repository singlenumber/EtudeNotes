public class Solution {
    public int findDuplicate(int[] nums) {
        int beg = 0;
        int end = nums.length - 1;
        while (beg <= end) {
            int mid = beg + (end - beg) / 2;
            int cnt = 0;
            for (int e: nums) {
                if (e <= mid) {
                    cnt++;
                }
            }
            if (cnt > mid) {
                end = mid - 1;
            } else {
                beg = mid + 1;
            }
        }
        return beg;
    }
}

public class Solution {
    public int findDuplicate(int[] nums) {
        int slow = 0;
        int fast = 0;
        do {
            slow = nums[slow];
            fast = nums[nums[fast]];
        } while (slow != fast);
        int find = 0;
        slow = 0;
        while (find != slow) {
            slow = nums[slow];
            find = nums[find];
        }
        return find;
    }
}