/*
 * Given an unsorted integer array, find the first missing positive integer.

 * For example,
 * Given [1,2,0] return 3,
 * and [3,4,-1,1] return 2.

 * Your algorithm should run in O(n) time and uses constant space.
 */

public class Solution {
    public int firstMissingPositive(int[] nums) {
        Arrays.sort(nums);
        int prev = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] <= 0 || nums[i] == prev) {
                continue;
            }
            if (nums[i] != prev + 1) {
                return prev + 1;
            }
            prev = nums[i];
        }
        return prev + 1;
    }
}

public class Solution {
    public int firstMissingPositive(int[] nums) {
        for (int i = 0; i < nums.length; i ++) {
            while (nums[i] != i + 1) {
                if (nums[i] - 1 < 0 || nums[i] > nums.length || nums[nums[i] - 1] == nums[i]) {
                    break;
                }
                int tmp = nums[i];
                nums[i] = nums[tmp - 1];
                nums[tmp - 1] = tmp;
            }
        }
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != i + 1) return i + 1;
        }
        return nums.length + 1;
    }
}

public class Solution {
    public int firstMissingPositive(int[] nums) {
        int i = 0;
        while(i < nums.length){
            if((nums[i] != i + 1) && (nums[i] - 1 >= 0) && (nums[i] - 1 <= nums.length - 1) && (nums[nums[i] - 1] != nums[i])){
                swap(nums, i, nums[i] - 1);
            }else{
                i++;
            }
        }
        for(i = 0 ; i < nums.length; i++){
            if(nums[i] != i + 1)
                return i + 1;
        }
        return nums.length + 1;
    }
    public void swap(int[] num, int a, int b){
        int tmp = num[a];
        num[a] = num[b];
        num[b] = tmp;
    }
}
