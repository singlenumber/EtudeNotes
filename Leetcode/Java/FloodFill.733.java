class Solution {
    public int[][] floodFill(int[][] image, int sr, int sc, int newColor) {
        if (image[sr][sc] == newColor) {
            return image;
        }
        dfs(image, sr, sc, image[sr][sc], newColor);
        return image;
    }

    private void dfs(int[][] image, int x, int y, int org, int target) {
        if (x < 0 || x >= image.length || y < 0 || y >= image[0].length || image[x][y] != org)  {
            return;
        }
        image[x][y] = target;
        dfs(image, x + 1, y, org, target);
        dfs(image, x - 1, y, org, target);
        dfs(image, x, y + 1, org, target);
        dfs(image, x, y - 1, org, target);
    }
}