public class Solution {
    public boolean canCross(int[] stones) {
        if (stones.length == 0) return true;
        HashMap<Integer, HashSet<Integer>> map = new HashMap<>();
        for (int i = 0; i < stones.length; i++) {
            map.put(stones[i], new HashSet<>());
        }
        map.get(0).add(1);
        for (int i = 0; i < stones.length - 1; i++) {
            int stone = stones[i];
            for (int jump: map.get(stone)) {
                if (stone + jump == stones[stones.length - 1]) {
                    return true;
                }
                Set<Integer> set = map.get(stone + jump);
                if (set != null) {
                    set.add(jump + 1);
                    if (jump - 1 > 0) {
                        set.add(jump - 1);
                    }
                    set.add(jump);
                }
            }
        }
        return false;
    }
}

//TLE
public class Solution {
    public boolean canCross(int[] stones) {
        int k = 0;
        return helper(stones, 0, k);
    }

    private boolean helper(int[] stones, int index, int k) {
        //目前已经达到了
        if (index == stones.length - 1) return true;
        //选择k的步伐，范围k-1到k
        for (int i = k - 1; i <= k + 1; i++) {
            int nextJump = stones[index] + i;
            //看接下来有没有合适的石头可以跳过去，从接下来的位置中查找有没有nextJump的位置，有的话返回石头的编号
            int nextPosition = Arrays.binarySearch(stones, index + 1, stones.length, nextJump);
            if (nextPosition > 0) {
                if (helper(stones, nextPosition, i)) return true;
            }
        }

        return false;
    }
}