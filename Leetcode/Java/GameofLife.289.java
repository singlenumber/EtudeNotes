/*
1st bit is current state
2nd bit is next state
1: Live 0: Dead
*/

public class Solution {
    public void gameOfLife(int[][] board) {
        for(int i = 0; i < board.length; i++){
            for(int j = 0; j < board[0].length; j++){
                int neighbors = getCount(board, i, j);
                if(board[i][j]==1){
                    if(neighbors == 2 || neighbors == 3)
                        board[i][j] = 3;
                }else{
                    if(neighbors == 3)
                        board[i][j] = 2;
                }
            }
        }
        for(int i = 0; i < board.length; i++){
            for(int j = 0; j < board[0].length; j++){
                board[i][j] = board[i][j] >> 1;
            }
        }
    }
    
    public int getCount(int[][] board, int row, int col){
        int cnt = 0;
        for(int i = row - 1; i <= row + 1; i++){
            for(int j = col - 1; j <= col + 1; j++){
                if(i >= 0 && i < board.length && j >= 0 && j < board[0].length){
                    cnt += (board[i][j] & 1);
                }
            }
        }
        return cnt - (board[row][col] & 1);
    }
}