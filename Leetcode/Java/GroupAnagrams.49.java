public class Solution {
    public List<List<String>> groupAnagrams(String[] strs) {
        List<List<String>> res = new ArrayList<>();
        HashMap<String, List<String>> map = new HashMap<>();
        for (String str: strs) {
            char[] arr = str.toCharArray();
            Arrays.sort(arr);
            String newstr = new String(arr);
            if (!map.containsKey(newstr)) {
                List<String> list = new ArrayList<>();
                res.add(list);
                map.put(newstr, list);
            }
            map.get(newstr).add(str);
        }
        return res;
    }
}