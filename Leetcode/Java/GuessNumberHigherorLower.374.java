public class Solution extends GuessGame {
    public int guessNumber(int n) {
        int beg = 1;
        int end = n;
        while (beg <= end) {
            int mid = beg + (end - beg) / 2;
            int res = guess(mid);
            if (res == 0) {
                return mid;
            } else if (res > 0) {
                beg = mid + 1;
            } else if (res < 0) {
                end = mid - 1;
            }
        }
        return -1;
    }
}

/* The guess API is defined in the parent class GuessGame.
   @param num, your guess
   @return -1 if my number is lower, 1 if my number is higher, otherwise return 0
      int guess(int num); */

public class Solution extends GuessGame {
    public int guessNumber(int n) {
        int beg = 1;
        int end = n;
        while (beg <= end) {
            int mid1 = beg + (end - beg) / 3;
            int mid2 = end - (end - beg) / 3;
            int res1 = guess(mid1);
            int res2 = guess(mid2);
            if (res1 == 0) {
                return mid1;
            } else if (res2 == 0) {
                return mid2;
            } else if (res1 < 0) {
                end = mid1 - 1;
            } else if (res2 > 0) {
                beg = mid2 + 1;
            } else {
                beg = mid1 + 1;
                end = mid2 - 1;
            }
        }
        return -1;
    }
}