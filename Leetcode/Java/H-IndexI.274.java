public class Solution {
    public int hIndex(int[] citations) {
        Arrays.sort(citations);
        int hIndex = 0;
        for (int i = 0; i < citations.length; i++) {
            int cur = Math.min(citations[i], citations.length - i);
            hIndex = Math.max(hIndex, cur);
        }
        return hIndex;
    }
}

public class Solution {
    public int hIndex(int[] citations) {
        if (citations == null || citations.length == 0) {
            return 0;
        }
        int len = citations.length;
        int[] count = new int[len + 1];
    
        for (int c: citations)
            if (c > len) 
                count[len]++;
            else 
                count[c]++;
        int total = 0;
        for (int i = len; i >= 0; i--) {
            total += count[i];
            if (total >= i)
                return i;
        }ß
        return 0;
    }
}
