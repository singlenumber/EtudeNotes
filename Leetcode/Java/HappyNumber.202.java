//Better version
public class Solution {
    public boolean isHappy(int n) {
        HashSet<Integer> set = new HashSet<>();
        while (n != 1) {
            if (set.contains(n)) {
                return false;
            }
            set.add(n);
            n = getNext(n);
        }
        return true;
    }
    public int getNext(int n) {
        int sum = 0;
        while (n != 0) {
            sum += (n % 10) * (n % 10);
            n /= 10;
        }
        return sum;
    }
}

public class Solution {
    public boolean isHappy(int n) {
        if (n == 0) {
            return false;
        }
        if (n == 1) {
            return true;
        }
        int res = n;
        HashSet<Integer> isVisited = new HashSet<>();
        while(!isVisited.contains(res)) {
            isVisited.add(res);
            int tmp = 0;
            while(res != 0) {
                tmp += Math.pow(res % 10, 2);
                res /= 10;
            }
            res = tmp;
            System.out.println(tmp);
            if (tmp == 1) {
                return true;
            }
        }
        return false;
    }
}