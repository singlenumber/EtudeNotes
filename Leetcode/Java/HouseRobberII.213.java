public class Solution {
    public int rob(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        if (nums.length == 1) {
            return nums[0];
        }
        return Math.max(rob(nums, 0, nums.length - 2), rob(nums, 1, nums.length - 1));
    }
    // 1 - 2 - 3 - 1
    
    public int rob(int[] nums, int beg, int end) {
        int prevNo = 0;
        int prevYes = 0;
        for (int i = beg; i <= end; i++) {
            int tmp = prevNo;
            prevNo = Math.max(prevYes, prevNo);
            prevYes = tmp + nums[i];
        }
        return Math.max(prevYes, prevNo);
    }
}