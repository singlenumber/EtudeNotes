class MyQueue {
    // Push element x to the back of queue.
    Stack<Integer> input = new Stack<>();
    Stack<Integer> output = new Stack<>();
    public void push(int x) {
        input.push(x);
    }

    // Removes the element from in front of queue.
    public void pop() {
        peek();
        output.pop();
    }

    // Get the front element.
    public int peek() {
        if (output.isEmpty()) {
            while(!input.isEmpty()){
                output.push(input.pop());
            }
        }
        return output.peek();
    }

    // Return whether the queue is empty.
    public boolean empty() {
        return input.isEmpty() && output.isEmpty();
    }
}

class MyQueue {
    Stack<Integer> s1 = new Stack<Integer>();
    Stack<Integer> s2 = new Stack<Integer>();
    // Push element x to the back of queue.
    public void push(int x) {
        s1.push(x);
    }

    // Removes the element from in front of queue.
    public void pop() {
        while (s1.size() != 1) {
            s2.push(s1.pop());
        }
        s1.pop();
        while (!s2.isEmpty()) {
            s1.push(s2.pop());
        }
    }

    // Get the front element.
    public int peek() {
        while (s1.size() != 1) {
            s2.push(s1.pop());
        }
        int val = s1.peek();
        while (!s2.isEmpty()) {
            s1.push(s2.pop());
        }
        return val;
    }

    // Return whether the queue is empty.
    public boolean empty() {
        return s1.isEmpty();
    }
}

