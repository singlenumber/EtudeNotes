public class Solution {
    public String intToRoman(int num) {
        StringBuilder res = new StringBuilder();
        int[] radix = {1000, 900, 500, 400, 100, 90,50, 40, 10, 9, 5, 4, 1};
        String[] symbol = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
        for (int i = 0; i < radix.length; i++) {
            while(num >= radix[i]) {
                num -= radix[i];
                res.append(symbol[i]);
            }
        }
        return res.toString();
    }
}


public class Solution {
    public String intToRoman(int num) {
        String[] ones = {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"};
        String[] tens = {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"};
        String[] huns = {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"};
        String[] thos = {"", "M", "MM", "MMM", "", "", "", "", "", ""};

        String result = "";
        result += thos[num / 1000];
        result += huns[num % 1000 / 100];
        result += tens[num % 100 / 10];
        result += ones[num % 10];
        return result;
    }
}
