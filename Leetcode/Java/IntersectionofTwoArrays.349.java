//Hash
public class Solution {
    public int[] intersection(int[] nums1, int[] nums2) {
        HashSet<Integer> set = new HashSet<>();
        HashSet<Integer> resSet = new HashSet<>();
        for (Integer num: nums1) {
            set.add(num);
        }
        for (Integer num: nums2) {
            if (set.contains(num)) {
                resSet.add(num);
            }
        }
        int[] res = new int[resSet.size()];
        int i = 0;
        for (Integer num: resSet) {
            res[i++] = num;
        }
        return res;
    }
}
//two pointers
public class Solution {
    public int[] intersection(int[] nums1, int[] nums2) {
        HashSet<Integer> resSet = new HashSet<>();
        Arrays.sort(nums1);
        Arrays.sort(nums2);
        int i = 0;
        int j = 0;
        while (i < nums1.length && j < nums2.length) {
            if (nums1[i] < nums2[j]) {
                i++;
            } else if (nums1[i] > nums2[j]) {
                j++;
            } else {
                resSet.add(nums1[i]);
                i++;
                j++;
            }
        }
        int[] res = new int[resSet.size()];
        int k = 0;
        for (Integer num: resSet) {
            res[k++] = num;
        }
        return res;
    }
}
//binary
public class Solution {
    public int[] intersection(int[] nums1, int[] nums2) {
        HashSet<Integer> resSet = new HashSet<>();
        Arrays.sort(nums2);
        for (Integer num: nums1) {
            if (binarySearch(nums2, num)) {
                resSet.add(num);
            }
        }
        int[] res = new int[resSet.size()];
        int i = 0;
        for (Integer num: resSet) {
            res[i++] = num;
        }
        return res;
    }
    
    public boolean binarySearch(int[] A, int target) {
        int beg = 0;
        int end = A.length - 1;
        while (beg <= end) {
            int mid = beg + (end - beg) / 2;
            if (A[mid] == target) {
                return true;
            } else if (A[mid] < target) {
                beg = mid + 1;
            } else if (A[mid] > target) {
                end = mid - 1;
            }
        }
        return false;
    }
}

//lambda
public class Solution {
    public int[] intersection(int[] nums1, int[] nums2) {
        return Arrays.stream(nums1)
                .distinct()
                .filter(p -> {
                    return Arrays.stream(nums2)
                            .anyMatch(q -> {
                                return q == p;
                            });
                })
                .toArray();
    }
}