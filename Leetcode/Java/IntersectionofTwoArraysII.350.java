public class Solution {
    public int[] intersect(int[] nums1, int[] nums2) {
        HashMap<Integer, Integer> map = new HashMap<>();
        List<Integer> resList = new ArrayList<>();
        for (Integer num: nums1) {
            if (!map.containsKey(num)) {
                map.put(num, 1);   
            } else {
                map.put(num, map.get(num) + 1);
            }
        }
        for (Integer num: nums2) {
            if (map.containsKey(num) && map.get(num) > 0) {
                resList.add(num);
                map.put(num, map.get(num) - 1);
            }
        }
        int i = 0;
        int[] res = new int[resList.size()];
        for (Integer num: resList) {
            res[i++] = num;
        }
        return res;
    }
}