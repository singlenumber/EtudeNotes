public class Solution {
    public boolean isIsomorphic(String s, String t) {
        if (s == null || t == null || s.length() != t.length()) {
            return false;
        }
        HashMap<Character, Character> m1 = new HashMap<>();
        HashMap<Character, Character> m2 = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            char a = s.charAt(i);
            char b = t.charAt(i);
            if (!m1.containsKey(a) && !m2.containsKey(b)) {
                m1.put(a, b);
                m2.put(b, a);
            } else {
                if (m1.get(a) == null || m2.get(b) == null || b != m1.get(a) || a != m2.get(b)) {
                    return false;
                }
            }
        }
        return true;
    }
}

public class Solution {
    public boolean isIsomorphic(String s, String t) {
        HashMap<Character, Character> pro = new HashMap<>();
        HashMap<Character, Character> con = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            char a = s.charAt(i);
            char b = t.charAt(i);
            if (!pro.containsKey(a) && !con.containsKey(b)) {
                pro.put(a, b);
                con.put(b, a);
            } else {
                if (!pro.containsKey(a) || !con.containsKey(b)) {
                    return false;
                }
                if (pro.get(a) != b || con.get(b) != a) {
                    return false;
                }
            }
        }
        return true;
    }
}

public class Solution {
    public boolean isIsomorphic(String s, String t) {
        HashMap<Character, Character> m1 = new HashMap<>();
        HashMap<Character, Character> m2 = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            char a = s.charAt(i);
            char b = t.charAt(i);
            if (!m1.containsKey(a) && !m2.containsKey(b)) {
                m1.put(a, b);
                m2.put(b, a);
            } else {
                if (m1.containsKey(a) && !m2.containsKey(b)) return false; 
                if (!m1.containsKey(a) && m2.containsKey(b)) return false;
                if (m1.get(a) != b || m2.get(b) != a) return false;
            }
        }
        return true;
    }
}

public class Solution {
    public boolean isIsomorphic(String s, String t) {
        HashMap<Character, Integer> m1 = new HashMap<>();
        HashMap<Character, Integer> m2 = new HashMap<>();
        for (Integer i = 0; i < s.length(); i++) {
            if (m1.put(s.charAt(i), i) != m2.put(t.charAt(i), i)) {
                return false;
            }
        }
        return true;
    }
}
