public class Solution {
  List<Integer> closestKValues(TreeNode root, double target, int k) {
    List<Integer> result = new ArrayList<>();
    if (root == null || k < 0) {
      return result;
    }
    Stack<Integer> ascend = new Stack<>();
    Stack<Integer> descend = new Stack<>();
    inorder(root, target, ascend);
    reverseinorder(root, target, descend);
    int i = 0;
    while (i++ < k) {
      if (!ascend.isEmpty() && !descend.isEmpty()) {
        if (Math.abs(ascend.peek() - target) > Math.abs(descend.peek() - target)) {
          result.add(descend.pop());
        } else {
          result.add(ascend.pop());
        }
      } else if (!ascend.isEmpty()) {
        result.add(ascend.pop());
      } else if (!descend.isEmpty()) {
        result.add(descend.pop());
      } else {
        break;
      }
    }
    return result;
  }

  private void inorder(TreeNode node, double target, Stack<Integer> stack) {
    if (node == null) {
      return;
    }
    inorder(node.left, target, stack);
    if (node.val <= target) {
      stack.push(node.val);
    } else {
      return;
    }
    inorder(node.right, target, stack);
  }
  private void reverseinorder(TreeNode node, double target, Stack<Integer> stack) {
    if (node == null) {
      return;
    }
    inorder(node.right, target, stack);
    if (node.val > target) {
      stack.push(node.val);
    } else {
      return;
    }
    inorder(node.left, target, stack);
  }
}