public class Solution {
    public int kthSmallest(int[][] matrix, int k) {
        int N = matrix.length;
        PriorityQueue<Tuple> pq = new PriorityQueue<>();
        for (int i = 0; i < N; i++) {
            pq.offer(new Tuple(0, i, matrix[0][i]));
        }
        for (int i = 1; i <= k - 1; i++) {
            Tuple cur = pq.poll();
            if (cur.x == N - 1) continue;
            pq.offer(new Tuple(cur.x + 1, cur.y, matrix[cur.x + 1][cur.y]));
        }
        return pq.poll().val;
    }

    class Tuple implements Comparable<Tuple> {
        int x;
        int y;
        int val;
        Tuple(int x, int y, int val) {
            this.x = x;
            this.y = y;
            this.val = val;
        }

        @Override
        public int compareTo(Tuple that) {
            return this.val - that.val;
        }
    }
}