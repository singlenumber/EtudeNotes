public class Solution {
    public String largestNumber(int[] nums) {
        String[] arr = new String[nums.length];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = String.valueOf(nums[i]);
        }
        Arrays.sort(arr, (a, b) -> {
            String str1 = a + b;
            String str2 = b + a;
            for (int i = 0; i < str1.length(); i++) {
                if (str1.charAt(i) > str2.charAt(i)) {
                    return 1;
                } else if (str1.charAt(i) < str2.charAt(i)) {
                    return -1;
                }
            }
            return 0;
        });
        StringBuilder res = new StringBuilder();
        for (int i = arr.length - 1; i >= 0; i--) {
            res.append(arr[i]);
        }
        int i = 0;
        while (i < res.length() - 1 && res.charAt(i) == '0') {
            res.deleteCharAt(i);
        }
        return res.toString();
    }
}