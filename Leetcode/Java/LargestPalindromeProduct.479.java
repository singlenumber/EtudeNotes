public class Solution {
    public int largestPalindrome(int n) {
        if (n == 1) {
            return 9;
        }
        int high = (int)(Math.pow(10, n) - 1);
        int low = (int)(Math.pow(10, n - 1));

        for (int i = high; i >= low; i--) {
            long candidate = createPalindrome(i);
            for (int j = high; j >= low; j--) {
                if (candidate / j > high) {
                    break;
                }
                if (candidate % j == 0) {
                    return (int)(candidate % 1337);
                }
            }
        }
        return -1;
    }

    public long createPalindrome(long num) {
        String str = num + new StringBuilder(Long.toString(num)).reverse().toString();
        return Long.parseLong(str);
    }
}