public class Solution {
    public boolean isReflected(int[][] points) {
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        HashSet<String> set = new HashSet<>();
        for (int[] e: points) {
            max = Math.max(max, e[0]);
            min = Math.min(min, e[0]);
            set.add(e[0] + "" + e[1]);
        }
        int sum = max + min;
        for (int[] e: points) {
            int a = e[0];
            int b = sum - e[0];
            if (!set.contains(b + "" + e[1])) {
                return false;
            }
        }
        return true;
    }
}