public class Solution {
    public int longestIncreasingPath(int[][] matrix) {
        if (matrix.length == 0) {
            return 0;
        }
        int M = matrix.length;
        int N = matrix[0].length;
        int[][] cache = new int[M][N];
        int max = 1;
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                int cur = dfs(matrix, i, j, cache);
                max = Math.max(max, cur);
            }
        }
        return max;
    }

    private int dfs(int[][] matrix, int X, int Y, int[][] cache) {
        if (cache[X][Y] > 0) {
            return cache[X][Y];
        }
        int max = 1;
        int[][] diff = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
        for (int i = 0; i < diff.length; i++) {
            int pos_x = X + diff[i][0];
            int pos_y = Y + diff[i][1];
            if (pos_x < 0 || pos_y < 0 || pos_x >= matrix.length || pos_y >= matrix[0].length || 
                        matrix[X][Y] >= matrix[pos_x][pos_y]) {
                continue;
            }
            int cur = 1 + dfs(matrix, pos_x, pos_y, cache);
            max = Math.max(max, cur);
        }
        cache[X][Y] = max;
        return max;
    }
}