public class Solution {
    public int longestPalindrome(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        int cnt = 0;
        HashSet<Character> set = new HashSet<>();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (set.contains(c)) {
                set.remove(c);
                cnt++;
            } else {
                set.add(c);
            }
        }
        return set.isEmpty()? cnt * 2: cnt * 2 + 1;
    }
}