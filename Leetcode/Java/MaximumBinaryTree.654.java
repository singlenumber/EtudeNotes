/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public TreeNode constructMaximumBinaryTree(int[] nums) {
        return helper(nums, 0, nums.length);
    }
    private TreeNode helper(int[] nums, int beg, int end) {
        if (beg == end) {
            return null;
        }
        int maxIndex = maxHelper(nums, beg, end);
        TreeNode root = new TreeNode(nums[maxIndex]);
        root.left = helper(nums, beg, maxIndex);
        root.right = helper(nums, maxIndex + 1, end);
        return root;
    }
    
    private int maxHelper(int[] nums, int beg, int end) {
        int res = beg;
        for (int i = beg; i < end; i++) {
            if (nums[i] > nums[res]) {
                res = i;
            }
        }
        return res;
    }
}