/*Find the contiguous subarray within an array (containing at least one number) which has the largest product.

For example, given the array [2,3,-2,4],
the contiguous subarray [2,3] has the largest product = 6.*/
//keep update max,min by max * A[i],min*A[i],A[i]
public class Solution {
    public int maxProduct(int[] nums) {
        int min = nums[0];
        int max = nums[0];
        int res = nums[0];
        for (int i = 1; i < nums.length; i++) {
            int oldMax = max;
            max = Math.max(nums[i] * max, Math.max(nums[i], nums[i] * min));
            min = Math.min(nums[i] * oldMax, Math.min(nums[i], nums[i] * min));
            res = Math.max(res, Math.max(max, min));
        }
        return res;
    }
}
