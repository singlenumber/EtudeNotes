public class Solution {
    public int maxProduct(String[] words) {
        if (words == null || words.length == 0) {
            return 0;
        }
        int N = words.length;
        int[] values = new int[N];
        for (int i = 0; i < N; i++) {
            values[i] = 0;
            for (int j = 0; j < words[i].length(); j++) {
                values[i] |= 1 << (words[i].charAt(j) - 'a');
            }
        }
        int res = 0;
        for (int i = 0; i < words.length; i++) {
            for (int j = i + 1; j < words.length; j++) {
                if ((values[i] & values[j]) == 0 && (words[i].length() * words[j].length()) > res) {
                    res = words[i].length() * words[j].length();
                }
            }
        }
        return res;
    }
}
