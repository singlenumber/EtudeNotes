/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
public class Solution {
    public ListNode mergeKLists(ListNode[] lists) {
        if (lists == null || lists.length == 0) {
            return null;
        }
        PriorityQueue<ListNode> pq = new PriorityQueue<>((a, b) -> (a.val - b.val));
        for (ListNode e: lists) {
            if (e != null) {
                pq.offer(e);
            }
        }
        ListNode dummy = new ListNode(0);
        ListNode cur = dummy;
        while (pq.size() != 0) {
            ListNode node = pq.poll();
            if (node.next != null) {
                pq.offer(node.next);
            }
            cur.next = node;
            cur = cur.next;
        }
        return dummy.next;
    }
}
// Merge K arrays
public class Solution {
    public List<Integer> mergeKArray(List<List<Integer>> lists) {
        List<Integer> res = new ArrayList<>();
        PriorityQueue<Tuple> minHeap = new PriorityQueue<>((a, b) -> a.val - b.val);
        for(List<Integer> list: lists) {
            if (list != null) {
                Iterator<Integer> iterator = list.iterator();
                if (iterator.hasNext()) {
                    int val = iterator.next();
                    minHeap.offer(new Tuple(val, iterator));
                }
            }
        }
        while (minHeap.size() != 0) {
            Tuple cur = minHeap.poll();
            res.add(cur.val);
            if (cur.getIterator().hasNext()) {
                int val = cur.getIterator().next();
                minHeap.offer(new Tuple(val, cur.iterator));
            }
        }
        return res;
    }


    public static void main(String[] args) {
        List<Integer> a = Arrays.asList(3, 5, 7, 9);
        List<Integer> b = Arrays.asList(2, 4, 8);
        List<Integer> c = Arrays.asList(1, 5, 6, 9);
        List<List<Integer>> list = Arrays.asList(a, b, c);
        List<Integer> res = new Solution().mergeKArray(list);
        System.out.println(res);
    }

    class Tuple {
        int val;
        Iterator<Integer> iterator;

        Tuple(int val, Iterator<Integer> iterator) {
            this.val = val;
            this.iterator = iterator;
        }

        public int getVal() {
            return val;
        }

        public Iterator<Integer> getIterator() {
            return iterator;
        }
    }
}

public class Solution {
    public ListNode mergeKLists(ListNode[] lists) {
        if (lists == null || lists.length == 0) {
            return null;
        }
        return helper(lists, 0, lists.length - 1);
    }
    public ListNode helper(ListNode[] lists, int L, int R) {
        if (L < R) {
            int M = L + (R - L) / 2;
            return merge(helper(lists, L, M), helper(lists, M + 1, R));
        }
        return lists[L];
    }
    public ListNode merge(ListNode a, ListNode b) {
        ListNode dummy = new ListNode(-1);
        ListNode cur = dummy;
        while (a != null && b != null) {
            if (a.val <= b.val) {
                cur.next = a;
                a = a.next;
            } else {
                cur.next = b;
                b = b.next;
            }
            cur = cur.next;
        }
        if (a != null) {
            cur.next = a;
        }
        if (b != null) {
            cur.next = b;   
        }
        return dummy.next;
    }
}

public class Solution {
    public ListNode mergeKLists(ListNode[] lists) {
        if (lists == null || lists.length == 0) {
            return null;
        }
        int end = lists.length - 1;
        while(end > 0) {
            int beg = 0;
            while(beg < end) {
                lists[beg] = merge(lists[beg++], lists[end--]);
            }
        }
        return lists[0];
    }

    public ListNode merge(ListNode a, ListNode b) {
        ListNode dummy = new ListNode(-1);
        ListNode cur = dummy;
        while (a != null && b != null) {
            if (a.val <= b.val) {
                cur.next = a;
                a = a.next;
            } else {
                cur.next = b;
                b = b.next;
            }
            cur = cur.next;
        }
        if (a != null) {
            cur.next = a;
        }
        if (b != null) {
            cur.next = b;   
        }
        return dummy.next;
    }
}
