class Solution {
    public NestedInteger deserialize(String s) {
        if (s == null || s.length() == 0) {
            return null;
        }
        if (s.charAt(0) != '[') {
            return new NestedInteger(Integer.valueOf(s));
        }
        int L = 0;
        int R = 0;
        NestedInteger cur = null;
        Stack<NestedInteger> stack = new Stack<>();
        while (R < s.length()) {
            if (s.charAt(R) == '[') {
                if (cur != null) {
                    stack.push(cur);
                }
                cur = new NestedInteger();
                L = R + 1;
            } else if (s.charAt(R) == ',') {
                if (s.charAt(R - 1) != ']') {
                    String num = s.substring(L, R);
                    cur.add(new NestedInteger(Integer.valueOf(num)));
                }
                L = R + 1;
            } else if (s.charAt(R) == ']') {
                String num = s.substring(L, R);
                if (num.length() != 0) {
                    cur.add(new NestedInteger(Integer.valueOf(num)));
                }
                if (!stack.isEmpty()) {
                    NestedInteger pop = stack.pop();
                    pop.add(cur);
                    cur = pop;
                }
                L = R + 1;
            }
            R++;
        }
        return cur;
    }
}