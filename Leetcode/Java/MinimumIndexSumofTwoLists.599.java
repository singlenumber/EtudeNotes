public class Solution {
    public String[] findRestaurant(String[] list1, String[] list2) {
        Map<String, Integer> map = new HashMap<>();
        List<String> result = new ArrayList<>();
        int minSum = Integer.MAX_VALUE;
        for (int i = 0; i < list1.length; i++) {
            map.put(list1[i], i);
        }
        for (int i = 0; i < list2.length; i++) {
            String str2 = list2[i];
            Integer j = map.get(str2);
            if (j != null && i + j <= minSum) {
                if (i + j < minSum) {
                    result = new ArrayList<>();
                    minSum = i + j;
                }
                result.add(str2);
            }
        }
        return result.toArray(new String[result.size()]);
    }
}