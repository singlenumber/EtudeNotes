public class MovingAverage {
    Queue<Integer> queue;
    int size;
    double sum;
    /** Initialize your data structure here. */
    public MovingAverage(int size) {
        queue = new LinkedList<>();
        this.sum = 0;
        this.size = size;
    }

    public double next(int val) {
        if (queue.size() < size) {
            sum += val;
            queue.offer(val);
            return sum / queue.size();
        } else {
            int valToRemove = queue.poll();
            sum -= valToRemove;
            queue.offer(val);
            sum += val;
            return sum / queue.size();
        }
    }
}

public class MovingAverage {
    Deque<Integer> deque;
    int size;
    double sum;
    /** Initialize your data structure here. */
    public MovingAverage(int size) {
        deque = new LinkedList<>();
        this.sum = 0;
        this.size = size;
    }

    public double next(int val) {
        if (deque.size() < size) {
            sum += val;
            deque.addLast(val);
            return sum / deque.size();
        } else {
            int valToRemove = deque.removeFirst();
            sum -= valToRemove;
            deque.addLast(val);
            sum += val;
            return sum / deque.size();
        }
    }
}

/**
 * Your MovingAverage object will be instantiated and called as such:
 * MovingAverage obj = new MovingAverage(size);
 * double param_1 = obj.next(val);
 */