public class Solution {
    public String multiply(String num1, String num2) {
        int M = num1.length();
        int N = num2.length();
        int[] res = new int[M + N];
        for (int i = M - 1; i >= 0; i--) {
            for (int j = N - 1; j >= 0; j--) {
                res[i + j + 1] += (num1.charAt(i) - '0') * (num2.charAt(j) - '0');
            }
        }
        int carry = 0;
        for (int i = res.length - 1; i >= 0; i--) {
            int tmp = res[i];
            res[i] = (tmp + carry) % 10;
            carry = (tmp + carry) / 10;
        }
        StringBuilder sb = new StringBuilder();
        for (Integer e: res) {
            sb.append(e);
        }
        int i = 0;
        while (sb.charAt(0) == '0' && sb.length() > 1) {
            sb.deleteCharAt(0);
        }
        return sb.toString();
    }
}