/**
 * // This is the interface that allows for creating nested lists.
 * // You should not implement it, or speculate about its implementation
 * public interface NestedInteger {
 *
 *     // @return true if this NestedInteger holds a single integer, rather than a nested list.
 *     public boolean isInteger();
 *
 *     // @return the single integer that this NestedInteger holds, if it holds a single integer
 *     // Return null if this NestedInteger holds a nested list
 *     public Integer getInteger();
 *
 *     // @return the nested list that this NestedInteger holds, if it holds a nested list
 *     // Return null if this NestedInteger holds a single integer
 *     public List<NestedInteger> getList();
 * }
 */
//DFS
public class Solution {
    public int depthSum(List<NestedInteger> nestedList) {
        if (nestedList == null || nestedList.size() == 0) {
            return 0;
        }
        return helper(1, nestedList);
    }
    public int helper(int depth, List<NestedInteger> nestedList) {
        int res = 0;
        for (NestedInteger e: nestedList) {
            if (e.isInteger()) {
                res += depth * e.getInteger();
            } else {
                res += helper(depth + 1, e.getList());
            }
        }
        return res;
    }
}
//BFS
public class Solution {
    public int depthSum(List<NestedInteger> nestedList) {
        if (nestedList == null || nestedList.size() == 0) {
            return 0;
        }
        Queue<NestedInteger> queue = new LinkedList<>();
        int res = 0;
        int depth = 1;
        queue.addAll(nestedList);
        while (!queue.isEmpty()) {
            int curSize = queue.size();
            for (int i = 0; i < curSize; i++) {
                NestedInteger cur = queue.poll();
                if (cur.isInteger()) {
                    res += depth * cur.getInteger();
                } else {
                    for (NestedInteger e: cur.getList()) {
                        queue.offer(e);
                    }
                }
            }
            depth++;
        }
        return res;
    }
}
//Stack
public class Solution {
    public int depthSum(List<NestedInteger> nestedList) {
        if (nestedList == null || nestedList.size() == 0) {
            return 0;
        }
        Stack<Pair> stack = new Stack<>();
        int res = 0;
        for (NestedInteger e: nestedList) {
            if (e.isInteger()) {
                res += e.getInteger();
            } else {
                stack.push(new Pair(e, 1));
            }
        }
        while (!stack.isEmpty()) {
            Pair cur = stack.pop();
            if (cur.nestedInteger.isInteger()) {
                res += cur.depth * cur.nestedInteger.getInteger();
            } else {
                for (NestedInteger e: cur.nestedInteger.getList()) {
                    stack.push(new Pair(e, cur.depth + 1));
                }
            }
        }
        return res;
    }
    class Pair {
        NestedInteger nestedInteger;
        int depth;
        Pair(NestedInteger nestedInteger, int depth) {
            this.nestedInteger = nestedInteger;
            this.depth = depth;
        }
    }
}