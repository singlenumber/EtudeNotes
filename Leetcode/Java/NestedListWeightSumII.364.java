/**
 * // This is the interface that allows for creating nested lists.
 * // You should not implement it, or speculate about its implementation
 * public interface NestedInteger {
 *     // Constructor initializes an empty nested list.
 *     public NestedInteger();
 *
 *     // Constructor initializes a single integer.
 *     public NestedInteger(int value);
 *
 *     // @return true if this NestedInteger holds a single integer, rather than a nested list.
 *     public boolean isInteger();
 *
 *     // @return the single integer that this NestedInteger holds, if it holds a single integer
 *     // Return null if this NestedInteger holds a nested list
 *     public Integer getInteger();
 *
 *     // Set this NestedInteger to hold a single integer.
 *     public void setInteger(int value);
 *
 *     // Set this NestedInteger to hold a nested list and adds a nested integer to it.
 *     public void add(NestedInteger ni);
 *
 *     // @return the nested list that this NestedInteger holds, if it holds a nested list
 *     // Return null if this NestedInteger holds a single integer
 *     public List<NestedInteger> getList();
 * }
 */
public class Solution {
    public int depthSumInverse(List<NestedInteger> nestedList) {
        int maxDepth = getDepth(nestedList, 1);
        return helper(maxDepth, nestedList);
    }
    
    public int helper(int depth, List<NestedInteger> nestedList) {
        int res = 0;
        for (NestedInteger e: nestedList) {
            if (e.isInteger()) {
                res += depth * e.getInteger();
            } else {
                res += helper(depth - 1, e.getList());
            }
        }
        return res;
    }
    
    public int getDepth(List<NestedInteger> nestedList, int cur) {
        int res = cur;
        for (NestedInteger e: nestedList) {
            if (!e.isInteger()) {
                res = Math.max(res, getDepth(e.getList(), cur + 1));
            }
        }
        return res;
    }
}

public class Solution {
    public int depthSumInverse(List<NestedInteger> nestedList) {
        int sum = 0;
        int curSum = 0;
        Queue<NestedInteger> queue = new LinkedList<>();
        queue.addAll(nestedList);
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                NestedInteger node = queue.poll();
                if (node.isInteger()) {
                    curSum += node.getInteger();
                } else {
                    for (NestedInteger e: node.getList()) {
                        queue.offer(e);
                    }
                }
            }
            sum += curSum;
        }
        return sum;
    }
}