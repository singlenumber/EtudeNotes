//DFS1
public class Solution {
    public int countComponents(int n, int[][] edges) {
        List<List<Integer>> g = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            g.add(new ArrayList<>());
        }
        for (int[] edge: edges) {
            g.get(edge[0]).add(edge[1]);
            g.get(edge[1]).add(edge[0]);
        }
        int count = 0;
        boolean[] isVisited = new boolean[n];
        for (int i = 0; i < n; i++) {
            if (!isVisited[i]) {
                count++;
                dfs(g, i, isVisited);
            }
        }
        return count;
    }

    public void dfs(List<List<Integer>> g, int cur, boolean[] isVisited) {
        isVisited[cur] = true;
        for (Integer adj: g.get(cur)) {
            if (!isVisited[adj]) {
                dfs(g, adj, isVisited);
            }
        }
    }
}
//DFS2
public class Solution {
    public int countComponents(int n, int[][] edges) {
        Map<Integer, HashSet<Integer>> g = new HashMap<>();
        Set<Integer> isVisited = new HashSet<>();
        for (int i = 0; i < n; i++) {
            g.put(i, new HashSet<>());
        }
        for (int[] e: edges) {
            int a = e[0];
            int b = e[1];
            g.get(a).add(b);
            g.get(b).add(a);
        }
        int count = 0;
        for (int i = 0; i < n; i++) {
            if (!isVisited.contains(i)) {
                count++;
                dfs(g, isVisited, i);
            }
        }
        return count;
    }
    public void dfs(Map<Integer, HashSet<Integer>> g, Set<Integer> isVisited, int cur) {
        isVisited.add(cur);
        for (int adj: g.get(cur)) {
            if (!isVisited.contains(adj)) {
                dfs(g, isVisited, adj);
            }
        }
    }
}
//BFS1
public class Solution {
    public int countComponents(int n, int[][] edges) {
        List<List<Integer>> g = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            g.add(new ArrayList<>());
        }
        for (int[] edge: edges) {
            g.get(edge[0]).add(edge[1]);
            g.get(edge[1]).add(edge[0]);
        }
        int count = 0;
        boolean[] isVisited = new boolean[n];
        for (int i = 0; i < n; i++) {
            if (!isVisited[i]) {
                count++;
                bfs(g, i, isVisited);
            }
        }
        return count;
    }

    public void bfs(List<List<Integer>> g, int cur, boolean[] isVisited) {
        Queue<Integer> q = new LinkedList<>();
        q.offer(cur);
        while (!q.isEmpty()) {
            int node = q.poll();
            isVisited[node] = true;
            for (int adj: g.get(node)) {
                if (!isVisited[adj]) {
                    q.offer(adj);
                }
            }
        }
    }
}
//BFS2
public class Solution {
    public int countComponents(int n, int[][] edges) {
        Map<Integer, HashSet<Integer>> g = new HashMap<>();
        Set<Integer> isVisited = new HashSet<>();
        for (int i = 0; i < n; i++) {
            g.put(i, new HashSet<>());
        }
        for (int[] e: edges) {
            int a = e[0];
            int b = e[1];
            g.get(a).add(b);
            g.get(b).add(a);
        }
        int count = 0;
        for (int i = 0; i < n; i++) {
            if (!isVisited.contains(i)) {
                count++;
                bfs(g, isVisited, i);
            }
        }
        return count;
    }
    public void bfs(Map<Integer, HashSet<Integer>> g, Set<Integer> isVisited, int cur) {
        Queue<Integer> queue = new LinkedList<>();
        queue.offer(cur);
        while (!queue.isEmpty()) {
            int curNode = queue.poll();
            isVisited.add(curNode);
            for (int adj: g.get(curNode)) {
                if (!isVisited.contains(adj)) {
                    queue.offer(adj);
                }
            }
        }
    }
}
//UF
public class Solution {
    public int countComponents(int n, int[][] edges) {
        int[] UF = new int[n];
        for (int i = 0; i < n; i++) {
            UF[i] = i;
        }
        for (int[] edge: edges) {
            int a = edge[0];
            int b = edge[1];
            while (UF[a] != a) {
                //compress
                UF[a] = UF[UF[a]];
                a = UF[a];
                
            };
            while (UF[b] != b) {
                //compress
                UF[b] = UF[UF[b]];
                b = UF[b];
            };
            UF[a] = b;
            if (a != b) {
                n--;
            }
        }
        return n;
    }
}
public class Solution {
    public int countComponents(int n, int[][] edges) {
        int[] UF = new int[n];
        for (int i = 0; i < n; i++) {
            UF[i] = i;
        }
        for (int[] edge: edges) {
            int p = find(UF, edge[0]);
            int q = find(UF, edge[1]);
            if (p != q) {
                UF[p] = q;
                n--;
            }
        }
        return n;
    }
    
    public int find(int[] UF, int p) {
        while (p != UF[p]) {
            UF[p] = UF[UF[p]];
            p = UF[p];
        }
        return p;
    }
}