/*
Given a 2d grid map of '1's (land) and '0's (water), count the number of islands. 
An island is surrounded by water and is formed by connecting adjacent lands horizontally or vertically. 
You may assume all four edges of the grid are all surrounded by water.
Example 1:
11110
11010
11000
00000
Answer: 1
Example 2:
11000
11000
00100
00011
Answer: 3
*/
public class Solution {
    public int numIslands(char[][] grid) {
        if (grid == null || grid.length == 0) return 0;
        int count = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (grid[i][j] == '1') {
                    count++;
                    grid[i][j] = '0';
                    dfs(grid, i, j);
                }
            }
        }
        return count;
    }
    public void dfs(char[][] grid, int i, int j){
        if (i - 1 >= 0 && grid[i - 1][j] == '1') {
            grid[i - 1][j] = '0';
            dfs(grid, i - 1, j);
        }
        if (i + 1 < grid.length && grid[i + 1][j] == '1') {
            grid[i + 1][j] = '0';
            dfs(grid, i + 1, j);
        }
        if (j - 1 >= 0 && grid[i][j - 1] == '1') {
            grid[i][j - 1] = '0';
            dfs(grid, i, j - 1);
        }
        if (j + 1 < grid[0].length && grid[i][j + 1] == '1') {
            grid[i][j + 1] = '0';
            dfs(grid, i, j + 1);
        }
    }
}

public class Solution {
    public int numIslands(char[][] grid) {
        if(grid == null || grid.length == 0){
            return 0;
        }
        int cnt = 0;
        for(int i = 0 ; i < grid.length; i++){
            for(int j = 0 ; j < grid[0].length; j++){
                if(grid[i][j] == '1'){
                    cnt++;
                    grid[i][j] = '0';
                    bfs(grid, i, j);
                }
            }
        }
        return cnt;
    }
    public void bfs(char[][] grid, int i, int j){
        Queue<Integer> row = new LinkedList<Integer>();
        Queue<Integer> col = new LinkedList<Integer>();
        row.offer(i);
        col.offer(j);
        grid[i][j] = '0';
        while(!row.isEmpty() && !col.isEmpty()){
            int m = row.poll();
            int n = col.poll();

            if(m - 1 >= 0 && grid[m - 1][n] == '1'){
                grid[m - 1][n] = '0';
                row.offer(m - 1); col.offer(n);
            }
            if(m + 1 < grid.length && grid[m + 1][n] == '1'){
                grid[m + 1][n] = '0';
                row.offer(m + 1); col.offer(n);
            }
            if(n - 1 >= 0 && grid[m][n - 1] == '1'){
                grid[m][n - 1] = '0';
                row.offer(m); col.offer(n - 1);
            }
            if(n + 1 < grid[0].length && grid[m][n + 1] == '1'){
                grid[m][n + 1] = '0';
                row.offer(m); col.offer(n + 1);
            }
        }
    }
}

//Union Find
public class Solution {
    public int numIslands(char[][] grid) {
        if(grid == null || grid.length == 0 || grid[0].length == 0){
            return 0;
        }
        UnionFind uf = new UnionFind(grid);
        int M = grid.length;
        int N = grid[0].length;
        int[][] diffs = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                if (grid[i][j] == '1') {
                    for (int[] diff :diffs) {
                        int X = i + diff[0];
                        int Y = j + diff[1];
                        if (X >= 0 && X < M && Y >= 0 && Y < N && grid[X][Y] == '1') {
                            int curId = i * N + j;
                            int anotherID = X * N + Y;
                            uf.union(curId, anotherID);
                        }
                    }
                }
            }
        }
        return uf.count;
    }
}

class UnionFind {
    int M;
    int N;
    int count;
    int[] root;
    UnionFind(char[][] grid){
        M = grid.length;
        N = grid[0].length;
        count = 0;
        root = new int[M * N];
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                if (grid[i][j] == '1') {
                    int curId = i * N + j;
                    root[curId] = curId;
                    count++;
                }
            }
        }
    }

    void union(int p, int q) {
        while (p != root[p]) {
            root[p] = root[root[p]];
            p = root[p];
        }
        while (q != root[q]) {
            root[q] = root[root[q]];
            q = root[q];
        }
        if (p != q) {
            count--;
        }
        root[p] = q;
    }
}
