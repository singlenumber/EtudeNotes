public class Solution {
    public boolean isOneEditDistance(String s, String t) {
        if(s == null || t == null)
            return false;
        if(Math.abs(s.length() - t.length()) > 1)
            return false;
        if(s.length() == t.length()){
            if(s.equals(t))
                return false;
            int cnt = 0;
            for(int i = 0; i < s.length(); i++){
                if(s.charAt(i) != t.charAt(i))
                    cnt++;
                if(cnt > 1)
                    return false;
            }
        }else{
            int cnt = 0;
            for(int i = 0, j = 0; i < s.length() && j < t.length(); i++, j++){
                if(s.charAt(i) != t.charAt(j)){
                    cnt++;
                    if(cnt > 1)
                        return false;
                    if(s.length() > t.length()){
                        j--;
                    }else{
                        i--;
                    }
                }
            }
        }
        return true;
    }
}

public class Solution {
    public boolean isOneEditDistance(String s, String t) {
        if (s == null || t == null || s.equals(t)) {
            return false;
        }
        int M = s.length();
        int N = t.length();
        if (M > N) {
            return isOneEditDistance(t, s);
        }
        int shift = N - M;
        if (shift > 1) {
            return false;
        }
        int i = 0;
        while (i < s.length() && s.charAt(i) == t.charAt(i)) {
            i++;
        }
        if (i == M) {
            return shift == 1;
        }
        if (shift == 0) {
            i++;
        }
        while (i < s.length() && s.charAt(i) == t.charAt(i + shift)) {
            i++;
        }
        return i == M;
    }
}


public class Solution {
    public boolean isOneEditDistance(String s, String t) {
        if (s == null || t == null || Math.abs(s.length() - t.length()) > 1 || s.equals(t)) {
            return false;
        }
        if (s.length() == t.length()) {
            return sameLength(s, t);
        } else {
            return notSameLength(s, t);
        }
    }
    
    public boolean sameLength(String s, String t) {
        int diff = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != t.charAt(i)) {
                diff++;
            }
            if (diff > 1) {
                return false;
            }
        }
        return true;
    }
    
    public boolean notSameLength(String s, String t) {
        if (s.length() > t.length()) {
            return notSameLength(t, s);
        }
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != t.charAt(i)) {
                return s.equals(t.substring(0, i) + t.substring(i + 1));
            }
        }
        return true;
    }
}