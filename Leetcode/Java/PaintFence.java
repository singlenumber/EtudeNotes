public class Solution {
    public int numWays(int n, int k) {
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return k;
        }
        int[] dp = new int[n];
        dp[0] = k;
        dp[1] = (k - 1) * k + k;
        for (int i = 2; i < n; i++) {
            dp[i] = dp[i - 1] * (k - 1) + dp[i - 2] * (k - 1);
        }
        return dp[n - 1];
    }
}

public class Solution {
    public int numWays(int n, int k) {
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return k;
        }
        int same = k;
        int diff = k * (k - 1);
        for (int i = 2; i < n; i++) {
            int oldSame = same;
            same = diff;
            diff = (oldSame + diff) * (k - 1);
        }
        return same + diff;
    }
}

public class Solution {
    public int numWays(int n, int k) {
        if(n == 0 || k == 0){
            return 0;
        }
        if(n == 1){
            return k;
        }
        int[] diff = new int[n + 1];
        int[] same = new int[n + 1];
        diff[2] = k * (k - 1);
        same[2] = k;
        for(int i = 3; i <= n; i++){
            same[i] = diff[i - 1];
            diff[i] = (same[i - 1] + diff[i- 1]) * (k - 1);
        }
        return same[n] + diff[n];
    }
}

