public class Solution {
    public int minCostII(int[][] costs) {
        if (costs == null || costs.length == 0) {
            return 0;
        }
        int N = costs.length;
        int M = costs[0].length;
        int prevMin = 0;
        int prevSec = 0;
        int preIdx = -1;
        for (int i = 0; i < N; i++) {
            int curMin = Integer.MAX_VALUE;
            int curSec = Integer.MAX_VALUE;
            int curIdx = -1;
            for (int j = 0; j < M; j++) {
                costs[i][j] += j == preIdx ? prevSec : prevMin;
                if (costs[i][j] < curMin) {
                    curSec = curMin;
                    curMin = costs[i][j];
                    curIdx = j;
                } else if (costs[i][j] < curSec) {
                    curSec = costs[i][j];
                }
            }
            prevMin = curMin;
            prevSec = curSec;
            preIdx = curIdx;
        }
        return prevMin;
    }
}

public class Solution {
    public int minCostII(int[][] costs) {
        if (costs == null || costs.length == 0 || costs[0].length == 0) {
            return 0;
        }
        int len = costs.length;
        int k = costs[0].length;
        int min1 = 0;
        int min2 = 0;
        int[] rec = new int[k];
        for (int i = 0 ; i < len; i++) {
            int oldmin1 = min1;
            int oldmin2 = min2;
            min1 = Integer.MAX_VALUE;
            min2 = Integer.MAX_VALUE;
            for (int j = 0; j < k; j++) {
                if (rec[j] != oldmin1 || oldmin1 == oldmin2) {
                    rec[j] = costs[i][j] + oldmin1;
                } else {
                    rec[j] = costs[i][j] + oldmin2;
                }
                if (min1 <= rec[j]) {
                    min2 = Math.min(min2, rec[j]);
                } else {
                    min2 = min1;
                    min1 = rec[j];
                }
            }
        }
        return min1;
    }
}
