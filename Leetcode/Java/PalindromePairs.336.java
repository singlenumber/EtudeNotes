public class Solution {
    public List<List<Integer>> palindromePairs(String[] words) {
        List<List<Integer>> res = new ArrayList<>();
        if (words == null || words.length < 2) {
            return res;
        }
        Map<String, Integer> map = new HashMap<>();
        for (int i = 0; i < words.length; i++) {
            map.put(words[i], i);
        }
        for (int i = 0; i < words.length; i++) {
            for (int j = 0; j <= words[i].length(); j++) {
                String str1 = words[i].substring(0, j);
                String str2 = words[i].substring(j);
                if (isP(str1)) {
                    String revStr2 = new StringBuilder(str2).reverse().toString();
                    if (map.containsKey(revStr2) && map.get(revStr2) != i) {
                        List<Integer> list = new ArrayList<>();
                        list.add(map.get(revStr2));
                        list.add(i);
                        res.add(list);
                    }
                }

                if (isP(str2)) {
                    String revStr1 = new StringBuilder(str1).reverse().toString();
                    if (map.containsKey(revStr1) && map.get(revStr1) != i && str2.length() != 0) {
                        List<Integer> list = new ArrayList<>();
                        list.add(i);
                        list.add(map.get(revStr1));
                        res.add(list);
                    }
                }
            }
        }
        return res;
    }

    public boolean isP(String str) {
        int beg = 0;
        int end = str.length() - 1;
        while (beg < end) {
            if (str.charAt(beg) != str.charAt(end)) {
                return false;
            }
            beg++;
            end--;
        }
        return true;
    }
}