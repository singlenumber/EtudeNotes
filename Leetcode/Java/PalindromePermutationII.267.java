/*
Given a string s, return all the palindromic permutations (without duplicates) of it. 
Return an empty list if no palindromic permutation could be form.
For example:
Given s = "aabb", return ["abba", "baab"].
Given s = "abc", return [].
*/
public class Solution {
    public List<String> generatePalindromes(String s) {
        List<String> res = new ArrayList<>();
        HashMap<Character, Integer> map = new HashMap<>();
        for (Character c: s.toCharArray()) {
            if (!map.containsKey(c)) {
                map.put(c, 1);
            } else {
                map.put(c, map.get(c) + 1);
            }
        }
        int cnt = 0;
        char odd = 0;
        for (Character c: map.keySet()) {
            if (map.get(c) % 2 == 1 ) {
                cnt++;
                odd = c;
            }
            if (cnt > 1) {
                return res;
            }
        }
        String str = "";
        if (cnt == 1) {
            map.put(odd, map.get(odd) - 1);
            str = Character.toString(odd);
        }
        helper(res, map, str, s.length());
        return res;
    }

    public void helper(List<String> res, HashMap<Character, Integer> map, String str, int len) {
        if (str.length() == len) {
            res.add(str);
            return;
        }
        for (Character c: map.keySet()) {
            if (map.get(c) > 0) {
                map.put(c, map.get(c) - 2);
                helper(res, map, c + str + c, len);
                map.put(c, map.get(c) + 2);
            }
        }
    }
}
