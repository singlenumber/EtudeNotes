/*
 * Given a collection of numbers, return all possible permutations.

 * For example,
 * [1,2,3] have the following permutations:
 * [1,2,3], [1,3,2], [2,1,3], [2,3,1], [3,1,2], and [3,2,1].
 */
public class Solution {
    public List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        List<Integer> list = new ArrayList<>();
        helper(res, list, nums);
        return res;
    }

    public void helper(List<List<Integer>> res, List<Integer> list, int[] nums){
        if (list.size() == nums.length) {
            res.add(new ArrayList<>(list));
            return;
        }
        for(int i = 0; i < nums.length; i++){
            if (!list.contains(nums[i])) {
                list.add(nums[i]);
                helper(res, list, nums);
                list.remove(list.size() - 1);
            }
        }
    }
}

public class Solution {
    public List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        List<Integer> list = new ArrayList<>();
        boolean[] rec = new boolean[nums.length];
        helper(res, list, rec, nums);
        return res;
    }

    public void helper(List<List<Integer>> res, List<Integer> list, boolean[] rec, int[] nums){
        if (list.size() == nums.length) {
            res.add(new ArrayList<>(list));
            return;
        }
        for(int i = 0; i < nums.length; i++){
            if (!rec[i]) {
                list.add(nums[i]);
                rec[i] = true;
                helper(res, list, rec, nums);
                rec[i] = false;
                list.remove(list.size() - 1);
            }
        }
    }
}

public class Solution {
    public List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        res.add(new ArrayList<>());
        for (int i = 0; i < nums.length; i++) {
            List<List<Integer>> newRes = new ArrayList<>();
            for (int j = 0; j < res.size(); j++) {
                for (int k = 0; k <= res.get(j).size(); k++) {
                    List<Integer> list = new ArrayList<>(res.get(j));
                    list.add(k, nums[i]);
                    newRes.add(list);
                }
            }
            res = newRes;
        }
        return res;
    }
}