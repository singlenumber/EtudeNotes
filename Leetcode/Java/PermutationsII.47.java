/*
 * Given a collection of numbers, return all possible permutations.

 * For example,
 * [1,2,3] have the following permutations:
 * [1,2,3], [1,3,2], [2,1,3], [2,3,1], [3,1,2], and [3,2,1].
 */
public class Solution {
    public List<List<Integer>> permuteUnique(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        List<Integer> list = new ArrayList<>();
        boolean[] rec = new boolean[nums.length];
        Arrays.sort(nums);
        helper(res, list, rec, nums);
        return res;
    }

    public void helper(List<List<Integer>> res, List<Integer> list, boolean[] rec, int[] nums){
        if (list.size() == nums.length) {
            res.add(new ArrayList<>(list));
            return;
        }
        for(int i = 0; i < nums.length; i++){
            if (!rec[i]) {
                list.add(nums[i]);
                rec[i] = true;
                helper(res, list, rec, nums);
                rec[i] = false;
                list.remove(list.size() - 1);
                while(i + 1 < nums.length && nums[i] == nums[i + 1]) {
                    i++;
                }
            }
        }
    }
}

public class Solution {
    public List<List<Integer>> permuteUnique(int[] num) {
        List<List<Integer>> res = new ArrayList<>();
        res.add(new ArrayList<>());
        for(int i = 0; i < num.length; i++) {
            HashSet<List<Integer>> set = new HashSet<>();
            for(int j = 0; j < res.size(); j++) {
                for(int k = 0; k <= res.get(j).size(); k++) {
                    List<Integer> list = new ArrayList<>(res.get(j));
                    list.add(k, num[i]);
                    set.add(list);
                }
            }
            res = new ArrayList<>(set);
        }
        return res;
    }
}

