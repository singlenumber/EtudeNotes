public class Solution {
    public double myPow(double x, int n) {
        if (n >= 0) {
            return helper(x, n);
        } else {
            return 1 / helper(x, -n);
        }
    }
    
    public double helper(double x, int n){
        if (n == 0) {
            return 1;
        }
        double half = helper(x, n / 2);
        if (n % 2 == 0) {
            return half * half;
        } else {
            return half * half * x;
        }
    }
}

public class Solution {
    public double myPow(double x, int n) {
        double res = 1;
        for (int m = n; m != 0; m /= 2) {
            if (m % 2 != 0) {
                res *= x;
            }
            x *= x;
        }
        return n >= 0 ? res : 1 / res;
    }
}