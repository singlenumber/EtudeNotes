/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public List<List<String>> printTree(TreeNode root) {
        int height = getHeight(root);
        String[][] res = new String[height][(1 << height) - 1];
        for (String[] arr: res) {
            Arrays.fill(arr, "");
        }
        List<List<String>> ans = new ArrayList<>();
        helper(res, root, 0, 0, res[0].length);
        for (String[] arr: res) {
            ans.add(Arrays.asList(arr));
        }
        return ans;
    }

    private void helper(String[][] res, TreeNode root, int level, int L, int R) {
        if (root == null) {
            return;
        }
        res[level][(L + R) / 2] = String.valueOf(root.val);
        helper(res, root.left, level + 1, L, (L + R) / 2);
        helper(res, root.right, level + 1, (L + R) / 2 + 1, R);
    }

    private int getHeight(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return 1 + Math.max(getHeight(root.left), getHeight(root.right));
    }
}