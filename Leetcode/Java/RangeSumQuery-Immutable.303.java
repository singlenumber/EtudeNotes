class NumArray {
    private final int[] rangeSum;
    public NumArray(int[] nums) {
        rangeSum = new int[nums.length];
        int sum = 0;
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
            rangeSum[i] = sum;
        }
    }
    
    public int sumRange(int i, int j) {
        return rangeSum[j] - (i == 0 ? 0 : rangeSum[i - 1]);
    }
}

public class NumArray {
    int[] rangeSum;
    public NumArray(int[] nums) {
        rangeSum = new int[nums.length + 1];
        rangeSum[0] = 0;
        for(int i = 1; i <= nums.length; i++){
            rangeSum[i] = rangeSum[i - 1] + nums[i - 1];
        }
    }

    public int sumRange(int i, int j) {
        return rangeSum[j + 1] - rangeSum[i];
    }
}

// Your NumArray object will be instantiated and called as such:
// NumArray numArray = new NumArray(nums);
// numArray.sumRange(0, 1);
// numArray.sumRange(1, 2);