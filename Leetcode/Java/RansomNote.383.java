public class Solution {
    public boolean canConstruct(String ransomNote, String magazine) {
        int[] hash = new int[26];
        for (Character c: magazine.toCharArray()) {
            hash[c - 'a']++;
        }
        for (Character c: ransomNote.toCharArray()) {
            hash[c - 'a']--;
            if (hash[c - 'a'] < 0) {
                return false;
            }
        }
        return true;
    }
}