/* The read4 API is defined in the parent class Reader4.
      int read4(char[] buf); */

public class Solution extends Reader4 {
    /**
     * @param buf Destination buffer
     * @param n   Maximum number of characters to read
     * @return    The number of characters read
     */
    public int read(char[] buf, int n) {
        int i = 0;
        while (i < n && (i4 < n4 || (i4 = 0) < (n4 = read4(buf4))))
            buf[i++] = buf4[i4++];
        return i;
    }
    char[] buf4 = new char[4];
    int i4 = 0, n4 = 0;
}

public class Solution extends Reader4 {
    /**
     * @param buf Destination buffer
     * @param n   Maximum number of characters to read
     * @return    The number of characters read
     */
    private char[] buffer = new char[4];
    private int offSet = 0;
    private int readSize = 0;
    public int read(char[] buf, int n) {
        int readByteCount = 0;
        boolean eof = false;
        while (readByteCount < n && !eof) {
            if (readSize == 0) {
                readSize = read4(buffer);
                if (readSize < 4) {
                    eof = true;
                }
            }
            int bytes = Math.min(n - readByteCount, readSize);
            System.arraycopy(buffer, offSet, buf, readByteCount, bytes);
            offSet = (offSet + bytes) % 4;
            readSize -= bytes;
            readByteCount += bytes;
        }
        return readByteCount;
    }
}