public class Solution {
    public List<String> findRepeatedDnaSequences(String s) {
        List<String> res = new ArrayList<>();
        if (s == null || s.length() == 0) return res;
        HashMap<String, Integer> map = new HashMap<>();
        for (int i = 0; i + 10 <= s.length(); i++) {
            String substr = s.substring(i, i + 10);
            if (map.containsKey(substr)) {
                map.put(substr, map.get(substr) + 1);
            } else {
                map.put(substr, 1);
            }
        }
        for (Map.Entry<String, Integer> entry: map.entrySet()) {
            if (entry.getValue() >= 2) {
                res.add(entry.getKey());
            }
        }
        return res;
    }
}

public class Solution {
    public List<String> findRepeatedDnaSequences(String s) {
        Set<String> seen = new HashSet<>();
        Set<String> res = new HashSet<>();
        for (int i = 0; i + 10 <= s.length(); i++) {
            String str = s.substring(i, i + 10);
            if (!seen.add(str)) {
                res.add(str);
            }
        }
        return new ArrayList<>(res);
    }
}

public class Solution {
    public List<String> findRepeatedDnaSequences(String s) {
        List<String> res = new ArrayList<>();
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i + 10 <= s.length(); i++) {
            String subStr = s.substring(i, i + 10);
            int hash = encode(subStr);
            if (!map.containsKey(hash)) {
                map.put(hash, 1);
            } else if (map.get(hash) == 1) {
                res.add(subStr);
                map.put(hash, 2);
            }
        }
        return res;
    }
    
    public int encode(String str) {
        StringBuilder sb = new StringBuilder();
        for (Character e: str.toCharArray()) {
            if (e == 'A') sb.append("0");
            else if (e == 'C') sb.append("1");
            else if (e == 'G') sb.append("2");
            else if (e == 'T') sb.append("3");
        }
        return Integer.parseInt(sb.toString(), 4);
    }
}

public class Solution {
    public List<String> findRepeatedDnaSequences(String s) {
        List<String> res = new ArrayList<>();
        if (s == null || s.length() == 0) return res;
        HashMap<Integer, Boolean> map = new HashMap<>();
        for (int i = 0; i + 10 <= s.length(); i++) {
            String substr = s.substring(i, i + 10);
            int num = toHash(substr);
            if (map.containsKey(num)) {
                if (!map.get(num)) {
                    res.add(substr);
                    map.put(num, true);
                }
            } else {
                map.put(num, false);
            }
        }
        return res;
    }
    
    public int toHash(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (c == 'A') sb.append('0');
            if (c == 'C') sb.append('1');
            if (c == 'G') sb.append('2');
            if (c == 'T') sb.append('3');
        }
        return Integer.parseInt(sb.toString(), 4);
    }
}

public class Solution {
    public List<String> findRepeatedDnaSequences(String s) {
        List<String> res = new ArrayList<>();
        HashMap<Long, Integer> map = new HashMap<>();
        for (int i = 0; i + 10 <= s.length(); i++) {
            String str = s.substring(i, i + 10);
            Long code = encode(str);
            if (!map.containsKey(code)) {
                map.put(code, 1);
            } else if (map.get(code) == 1) {
                res.add(str);
                map.put(code, 2);
            }
        }
        return res;
    }
    public Long encode(String str) {
        StringBuilder sb = new StringBuilder();
        for (Character c : str.toCharArray()) {
            switch (c) {
                case 'A':
                    sb.append("0");
                    break;
                case 'C':
                    sb.append("1");
                    break;
                case 'G':
                    sb.append("2");
                    break;
                case 'T':
                    sb.append("3");
                    break;
            }
        }
        return Long.parseLong(sb.toString());
    }
}
