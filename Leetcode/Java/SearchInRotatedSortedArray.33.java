/*
 * Suppose a sorted array is rotated at some pivot unknown to you beforehand.

 * (i.e., 0 1 2 4 5 6 7 might become 4 5 6 7 0 1 2).

 * You are given a target value to search. If found in the array return its 
 * index, otherwise return -1.

 * You may assume no duplicate exists in the array.
 */
 /*1. Search 2 in 5 6 7 8 9 1 2 3 
      A[start]>A[end], target<=A[end], A[mid]>A[end]
   2. Search 8 in 7 8 9 1 2 3 5 6
      A[start]>A[end], target>A[end],A[mid]<A[end]
 */
public class Solution {
    public int search(int[] nums, int target) {
        int beg = 0;
        int end = nums.length - 1;
        while (beg <= end) {
            int mid = beg + (end - beg) / 2;
            if (nums[mid] == target) {
                return mid;
            }
            if (nums[beg] < nums[mid]) {
                if (nums[beg] <= target && target <= nums[mid]) {
                    end = mid - 1;
                } else {
                    beg = mid + 1;
                }
            } else if (nums[beg] > nums[mid]) {
                if (nums[mid] <= target && target <= nums[end]) {
                    beg = mid + 1;
                } else {
                    end = mid - 1;
                } 
            } else {
                beg++; //beg++,--,end++,-- is all ok, deal with only one elements
            }
        }
        return -1;
    }
}

public class Solution {
    public int search(int[] nums, int target) {
        int beg = 0;
        int end = nums.length - 1;
        while (beg <= end) {
            int mid = beg + (end - beg) / 2;
            if (nums[mid] == target) {
                return mid;
            }
            if (nums[beg] <= nums[mid]) {
                if (nums[beg] <= target && target <= nums[mid]) {
                    end = mid - 1;
                } else {
                    beg = mid + 1;
                }
            } else if (nums[mid] <= nums[end]) {
                if (nums[mid] <= target && target <= nums[end]) {
                    beg = mid + 1;
                } else {
                    end = mid - 1;
                } 
            } else {
                beg++;
            }
        }
        return -1;
    }
}
