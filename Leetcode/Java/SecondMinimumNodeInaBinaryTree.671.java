class Solution {
    public int findSecondMinimumValue(TreeNode root) {
        Set<Integer> set = new HashSet<>();
        dfs(root, set);
        int min = root.val;
        int secMin = Integer.MAX_VALUE;
        for (Integer elem: set) {
            if (elem > min && elem < secMin) {
                secMin = elem;
            }
        }
        return secMin == Integer.MAX_VALUE ? -1 : secMin;
    }

    public void dfs(TreeNode root, Set<Integer> set) {
        if (root != null) {
            set.add(root.val);
            dfs(root.left, set);
            dfs(root.right, set);
        }
    }
}

class Solution {
    int min;
    int secMin = Integer.MAX_VALUE;
    public int findSecondMinimumValue(TreeNode root) {
        min = root.val;
        dfs(root);
        return secMin == Integer.MAX_VALUE ? -1 : secMin;
    }

    public void dfs(TreeNode root) {
        if (root != null) {
            if (root.val > min && root.val < secMin) {
                secMin = root.val;
            }
            dfs(root.left);
            dfs(root.right);
        }
    }
}