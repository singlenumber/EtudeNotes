/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */

public class Codec {

    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        if (root == null) return "#";
        StringBuilder sb = new StringBuilder();
        Queue<TreeNode> q = new LinkedList<>();
        sb.append(root.val);
        q.offer(root);
        while (!q.isEmpty()) {
            int size = q.size();
            for (int i = 0; i < size; i++) {
                TreeNode cur = q.poll();
                sb.append(" ");
                if (cur.left != null) {
                    sb.append(cur.left.val);
                    q.offer(cur.left);
                } else {
                    sb.append("#");
                }
                sb.append(" ");
                if (cur.right != null) {
                    sb.append(cur.right.val);
                    q.offer(cur.right);
                } else {
                    sb.append("#");
                }
            }
        }
        return sb.toString();
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        if (data == null || data.length() == 0 || data.equals("#")) {
            return null;
        }
        String[] arr = data.split(" ");
        int idx = 0;
        TreeNode root = new TreeNode(Integer.parseInt(arr[idx++]));
        Queue<TreeNode> q = new LinkedList<>();
        q.offer(root);
        while (!q.isEmpty() && idx < arr.length) {
            int size = q.size();
            for (int i = 0; i < size; i++) {
                TreeNode cur = q.poll();
                if (!arr[idx].equals("#")) {
                    cur.left = new TreeNode(Integer.parseInt(arr[idx]));
                    q.offer(cur.left);
                } else {
                    cur.left = null;
                }
                idx++;
                if (!arr[idx].equals("#")) {
                    cur.right = new TreeNode(Integer.parseInt(arr[idx]));
                    q.offer(cur.right);
                } else {
                    cur.right = null;
                }
                idx++;
            }
        }
        return root;
    }
}

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Codec {
    private static final String delimiter = ",";
    private static final String nullNode = "#";
    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        StringBuilder sb = new StringBuilder();
        buildString(root, sb);
        return sb.toString();
    }
    public void buildString(TreeNode root, StringBuilder sb) {
        if (root == null) {
            sb.append(nullNode).append(delimiter);
        } else {
            sb.append(root.val).append(delimiter);
            buildString(root.left, sb);
            buildString(root.right, sb);
        }
    }
    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        Deque<String> dq = new LinkedList<>();
        dq.addAll(Arrays.asList(data.split(delimiter)));
        return buildTree(dq);
    }
    
    public TreeNode buildTree(Deque<String> dq) {
        String cur = dq.remove();
        if (cur.equals(nullNode)) {
            return null;
        }
        TreeNode root = new TreeNode(Integer.parseInt(cur));
        root.left = buildTree(dq);
        root.right = buildTree(dq);
        return root;
    }
}

// Your Codec object will be instantiated and called as such:
// Codec codec = new Codec();
// codec.deserialize(codec.serialize(root));

public class Codec {

    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        if (root == null) {
            return "#";
        }
        StringBuilder sb = new StringBuilder();
        Queue<TreeNode> q = new LinkedList<>();
        q.offer(root);
        sb.append(root.val);
        int curLevel = 1;
        int nextLevel = 0;
        while (!q.isEmpty()) {
            for(int i = 0; i < curLevel; i++){
                TreeNode cur = q.poll();
                sb.append(" ");
                if (cur.left != null) {
                    sb.append(cur.left.val);
                    q.offer(cur.left);
                    nextLevel++;
                } else {
                    sb.append("#");
                }
                sb.append(" ");
                if (cur.right != null) {
                    sb.append(cur.right.val);
                    q.offer(cur.right);
                    nextLevel++;
                } else {
                    sb.append("#");
                }
            }
            curLevel = nextLevel;
            nextLevel = 0;
        }
        return sb.toString();
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        if (data == null || data.length() == 0 || data.equals("#")) {
            return null;
        }
        int idx = 0;
        String[] strs = data.split(" ");
        TreeNode root = new TreeNode(Integer.parseInt(strs[idx++]));
        Queue<TreeNode> q = new LinkedList<>();
        q.offer(root);
        int curLevel = 1;
        int nextLevel = 0;
        while (!q.isEmpty() && idx < data.length()) {
            for (int i = 0 ; i < curLevel; i++) {
                TreeNode cur = q.poll();
                if (strs[idx].equals("#")) {
                    cur.left = null;
                } else {
                    cur.left = new TreeNode(Integer.parseInt(strs[idx]));
                    q.offer(cur.left);
                    nextLevel++;
                }
                idx++;
                if (strs[idx].equals("#")) {
                    cur.right = null;
                } else {
                    cur.right = new TreeNode(Integer.parseInt(strs[idx]));
                    q.offer(cur.right);
                    nextLevel++;
                }
                idx++;
            }
            curLevel = nextLevel;
            nextLevel = 0;
        }
        return root;
    }
}

// Your Codec object will be instantiated and called as such:
// Codec codec = new Codec();
// codec.deserialize(codec.serialize(root));
