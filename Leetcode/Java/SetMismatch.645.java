class Solution {
    public int[] findErrorNums(int[] nums) {
        Arrays.sort(nums);
        int duplicated = -1;
        int missed = 1;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] == nums[i - 1]) {
                duplicated = nums[i];
            } else if (nums[i] != nums[i - 1] + 1) {
                missed = nums[i - 1] + 1;
            }
        }
        return new int[] {duplicated, nums[nums.length - 1] != nums.length ? nums.length : missed};
    }
}