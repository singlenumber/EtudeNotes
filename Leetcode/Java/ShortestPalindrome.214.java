public class Solution {
    public String shortestPalindrome(String s) {
        if (s == null || s.length() <= 1) {
            return s;
        }
        int start = s.length();

        StringBuilder sb = new StringBuilder();
        while (start > 1) {
            String firstSub = s.substring(0, start);
            if (isP(firstSub)) {
                sb.append(new StringBuilder(s.substring(start)).reverse());
                break;
            }
            start--;
        }
        if (sb.length() == 0) {
            sb.append(s.substring(start)).reverse();
        }
        return sb.append(s).toString();
    }

    public boolean isP(String str) {
        int len = str.length();
        for (int i = 0; i < len / 2; i++) {
            if (str.charAt(i) != str.charAt(len - i - 1))
                return false;
        }
        return true;
    }
}