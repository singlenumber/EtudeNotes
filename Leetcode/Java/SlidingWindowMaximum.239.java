public class Solution {
    public int[] maxSlidingWindow(int[] nums, int k) {
        if (nums == null || nums.length == 0 || k <= 0) {
            return new int[0];
        }
        int N = nums.length;
        int[] res = new int[N - k + 1];
        Deque<Integer> dq = new LinkedList<>();
        for (int i = 0; i < N; i++) {
            if (i - k >= 0 && nums[i - k] == dq.peekLast()) {
                dq.removeLast();
            }
            while (!dq.isEmpty() && nums[i] > dq.peekFirst()) {
                dq.removeFirst();
            }
            dq.addFirst(nums[i]);
            if (i + 1 - k >= 0) {
                res[i + 1 - k] = dq.peekLast();
            }
        }
        return res;
    }
}