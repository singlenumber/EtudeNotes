//Brutal
public class Solution {
    public int minArea(char[][] image, int x, int y) {
        int left = y;
        int right = y;
        int top = x;
        int down = x;
        for (int i = 0; i < image.length; i++) {
            for (int j = 0; j < image[0].length; j++) {
                if (image[i][j] == '1') {
                    left = Math.min(left, j);
                    right = Math.max(right, j);
                    top = Math.min(top, i);
                    down = Math.max(down, i);
                }
            }
        }
        return (down - top + 1) * (right - left + 1);
    }
}

//DFS
public class Solution {
    private int left = Integer.MAX_VALUE;
    private int top = Integer.MAX_VALUE;
    private int right = Integer.MIN_VALUE;
    private int down = Integer.MIN_VALUE;
    public int minArea(char[][] image, int x, int y) {
        if (image == null || image.length == 0 || image[0].length == 0) {
            return 0;
        }
        dfs(image, x, y);
        return (down - top + 1) * (right - left + 1);
    }
    private void dfs(char[][] image, int x, int y) {
        int M = image.length;
        int N = image[0].length;
        if (x < 0 || x >= M || y < 0 || y >= N || image[x][y] == '0') {
            return;
        }
        image[x][y] = '0';
        left = Math.min(left, y);
        right = Math.max(right, y);
        top = Math.min(top, x);
        down = Math.max(down, x);
        dfs(image, x + 1, y);
        dfs(image, x - 1, y);
        dfs(image, x, y + 1);
        dfs(image, x, y - 1);
    }
}

//Binary
public class Solution {
    private char[][] data;
    public int minArea(char[][] image, int x, int y) {
        data = image;
        int M = image.length; int N = image[0].length;
        int left = searchCol(0, y, 0, M, true);
        int right = searchCol(y + 1, N, 0, M, false);
        int top = searchRow(0, x, left, right, true);
        int down = searchRow(x + 1, M, left, right, false);
        return (right - left) * (down - top);
    }
    private int searchCol(int i, int j, int top, int bottom, boolean opt) {
        while (i != j) {
            int k = top, mid = (i + j) / 2;
            while (k < bottom && data[k][mid] == '0') ++k;
            if (k < bottom == opt)
                j = mid;
            else
                i = mid + 1;
        }
        return i;
    }
    private int searchRow(int i, int j, int left, int right, boolean opt) {
        while (i != j) {
            int k = left, mid = (i + j) / 2;
            while (k < right && data[mid][k] == '0') ++k;
            if (k < right == opt)
                j = mid;
            else
                i = mid + 1;
        }
        return i;
    }
}