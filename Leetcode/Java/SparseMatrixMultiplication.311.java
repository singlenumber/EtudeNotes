public class Solution {
    public int[][] multiply(int[][] A, int[][] B) {
        if (A == null || B == null || A.length == 0 || B.length == 0) return null;
        int[][] res = new int[A.length][B[0].length];
        for (int i = 0; i < A.length; i++) {
            for(int j = 0; j < A[0].length; j++) {
                if (A[i][j] == 0) {
                    continue;
                }
                for (int k = 0; k < B[0].length; k++) {
                    res[i][k] += A[i][j] * B[j][k];
                }
            }    
        }
        return res;
    }
}

public class Solution {
    public int[][] multiply(int[][] A, int[][] B) {
        if (A == null || B == null || A.length == 0 || B.length == 0) return null;
        //A[0].length == B.length
        int[][] res = new int[A.length][B[0].length];
        Map<Integer, Map<Integer, Integer>> mapA = new HashMap<>();
        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < A[0].length; j++) {
                if (A[i][j] != 0) {
                    if (!mapA.containsKey(i)) {
                        mapA.put(i, new HashMap<>());
                    }
                    mapA.get(i).put(j, A[i][j]);
                }
            }
        }
        
        Map<Integer, Map<Integer, Integer>> mapB = new HashMap<>();
        for (int i = 0; i < B.length; i++) {
            for (int j = 0; j < B[0].length; j++) {
                if (B[i][j] != 0) {
                    if (!mapB.containsKey(i)) {
                        mapB.put(i, new HashMap<>());
                    }
                    mapB.get(i).put(j, B[i][j]);
                }
            }
        }
        for (int i : mapA.keySet()) {
            for (int j : mapA.get(i).keySet()) {
                if (mapB.containsKey(j)) {
                    for (int k : mapB.get(j).keySet()) {
                        res[i][k] += mapA.get(i).get(j) * mapB.get(j).get(k);
                    }
                }
            }
        }
        return res;
    }
}