public class Solution {
    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        if(nums == null || nums.length == 0) return res;
        res.add(new ArrayList<>());
        for (int num: nums) {
            List<List<Integer>> resDup = new ArrayList<>(res);
            for (List<Integer> e: resDup) {
                List<Integer> newList = new ArrayList<>(e);
                newList.add(num);
                res.add(newList);
            }
        }
        return res;
    }
}

public class Solution {
    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        if(nums == null || nums.length == 0) return res;
        Arrays.sort(nums);
        for (int i = 0; i < nums.length; i++) {
            List<List<Integer>> tmp = new ArrayList<>();
            for (List<Integer> e: res) {
                tmp.add(new ArrayList<>(e));
            }
            for (List<Integer> e: tmp) {
                e.add(nums[i]);
            }
            List<Integer> single = new ArrayList<>();
            single.add(nums[i]);
            tmp.add(single);
            res.addAll(tmp);
        }
        res.add(new ArrayList<>());
        return res;
    }
}

public class Solution {
    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        if (nums == null || nums.length == 0) {
            return res;
        }
        Arrays.sort(nums);
        helper(res, new ArrayList<>(), nums, 0);
        return res;
    }
    public void helper(List<List<Integer>> res, List<Integer> list, int[] nums, int pos) {
        res.add(new ArrayList<>(list));
        for (int i = pos; i < nums.length; i++) {
            list.add(nums[i]);
            helper(res, list, nums, i + 1);
            list.remove(list.size() - 1);
        }
    }
}