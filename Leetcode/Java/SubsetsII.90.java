public class Solution {
    public List<List<Integer>> subsetsWithDup(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        if(nums == null || nums.length == 0) return res;
        Arrays.sort(nums);
        res.add(new ArrayList<>());
        for (int num: nums) {
            List<List<Integer>> resDup = new ArrayList<>(res);
            for (List<Integer> e: resDup) {
                List<Integer> newList = new ArrayList<>(e);
                newList.add(num);
                if (!res.contains(newList)) {
                    res.add(newList);
                }
            }
        }
        return res;
    }
}


public class Solution {
    public List<List<Integer>> subsetsWithDup(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        if(nums == null || nums.length == 0) return res;
        Arrays.sort(nums);
        helper(res, new ArrayList<>(), nums, 0);
        return res;
    }
    public void helper(List<List<Integer>> res, List<Integer> list, int[] nums, int pos){
        res.add(new ArrayList<>(list));
        for(int i = pos; i < nums.length; i++){
            list.add(nums[i]);
            helper(res, list, nums, i + 1);
            list.remove(list.size() - 1);
            while(i < nums.length - 1 && nums[i] == nums[i + 1]){
                i++;
            }
        }
    }
}

// can't pass, order wrong
public class Solution {
    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        if(nums == null || nums.length == 0) return res;
        Arrays.sort(nums);
        for (int i = 0; i < nums.length; i++) {
            List<List<Integer>> tmp = new ArrayList<>();
            for (List<Integer> e: res) {
                tmp.add(new ArrayList<>(e));
            }
            for (List<Integer> e: tmp) {
                List<Integer> list = new ArrayList<>(e);
                list.add(nums[i]);
                if (!tmp.contains(list)) {
                    e.add(nums[i]);
                }
            }
            List<Integer> single = new ArrayList<>();
            single.add(nums[i]);
            if (!res.contains(single)){
                tmp.add(single);
            }
            res.addAll(tmp);
        }
        res.add(new ArrayList<>());
        return res;
    }
    public static void main(String[] args) {
        System.out.println(new Solution().subsets(new int[]{1,2,2}));
    }
}