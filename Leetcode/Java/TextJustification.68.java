public class Solution {
    public List<String> fullJustify(String[] words, int maxWidth) {
        List<String> res = new ArrayList<>();
        if (words == null || words.length == 0) {
            return res;
        }
        int count = 0;
        int last = 0;
        for (int i = 0; i < words.length; i++) {
            //count是上一次计算的单词的长度，words[i].length()是当前尝试放的一个单词的长度，
            //假设当前放上了这个单词，那么这一行单词跟单词间的间隔数就是i-last
            //判断这些总的长度加起来是不是大于L（超行数了）
            if (count + words[i].length() + (i - last) > maxWidth) {
                int spaceNum = 0;
                int extraNum = 0;
                //因为尝试的words[i]失败了，所以间隔数减1.此时判断剩余的间隔数是否大于0
                if (i - last - 1 > 0) {
                    //是间隔的倍数（为啥要减1，因为尝试当前words[i]后发现比L长了，
                    //所以当前这个单词不能算作这行，所以间隔就减少一个
                    spaceNum = (maxWidth - count) / (i - last - 1);
                    extraNum = (maxWidth - count) % (i - last - 1);
                }
                StringBuilder sb = new StringBuilder();
                for (int j = last; j < i; j++) {
                    sb.append(words[j]);
                    //words[i-1]的话后面就不用填空格了，所以这里j<i-1
                    if (j < i - 1) {
                        for (int k = 0; k < spaceNum; k++) {
                            sb.append(" ");
                        }
                        if (extraNum > 0) {
                            sb.append(" ");
                        }
                        extraNum--;
                    }
                }
                //下面这个for循环作用于一行只有一个单词还没填满一行的情况
                for (int j = sb.length(); j < maxWidth; j++) {
                    sb.append(" ");
                }
                res.add(sb.toString());
                count = 0;
                last = i;
            }
            count += words[i].length();
        }
        //处理最后一行
        StringBuilder sb = new StringBuilder();
        for (int i = last; i < words.length; i++) {
            sb.append(words[i]);
            if (sb.length() < maxWidth) {
                sb.append(" ");
            }
        }
        for (int j = sb.length(); j < maxWidth; j++) {
            sb.append(" ");
        }
        res.add(sb.toString());
        return res;
    }
}