//O(N * logK)
public class Solution {
    public List<Integer> topKFrequent(int[] nums, int k) {
        Map<Integer, Integer> map = new HashMap<>();
        for (Integer num: nums) {
            if (!map.containsKey(num)) {
                map.put(num, 1);
            } else {
                map.put(num, map.get(num) + 1);
            }
        }
        PriorityQueue<Record> pq = new PriorityQueue<>((a, b) -> a.count - b.count);
        for (Map.Entry<Integer, Integer> entry: map.entrySet()) {
            int num = entry.getKey();
            int count = entry.getValue();
            pq.add(new Record(num, count));
            if (pq.size() > k) {
                pq.poll();
            }
        }
        List<Integer> result = new ArrayList<>();
        for (Record record: pq) {
            result.add(record.num);
            if (result.size() == k) {
                break;
            }
        }
        Collections.reverse(result);
        return result;
    }
}

class Record {
    int num;
    int count;
    Record(int num, int count) {
        this.num = num;
        this.count = count;
    }
}
//O(N)
public class Solution {
    public List<Integer> topKFrequent(int[] nums, int k) {
        Map<Integer, Integer> map = new HashMap<>();
        int max = 1;
        for (Integer num: nums) {
            if (!map.containsKey(num)) {
                map.put(num, 1);
            } else {
                map.put(num, map.get(num) + 1);
            }
            max = Math.max(max, map.get(num));
        }
        List<Integer>[] list = new ArrayList[max + 1];
        for (int i = 1; i <= max; i++) {
            list[i] = new ArrayList<>();
        }
        for (Map.Entry<Integer, Integer> entry: map.entrySet()) {
            int num = entry.getKey();
            int count = entry.getValue();
            list[count].add(num);
        }
        List<Integer> result = new ArrayList<>();
        for (int i = max; i >= 1; i--) {
            if (list[i].size() > 0) {
                result.addAll(list[i]);
            }
            if (result.size() == k) {
                break;
            }
        }
        return result;
    }
}

public class Solution {
    public List<Integer> topKFrequent(int[] nums, int k) {
        HashMap<Integer,Integer> map = new HashMap<>();
        for (Integer e: nums) {
            if (!map.containsKey(e)) {
                map.put(e, 1);
            } else {
                map.put(e, map.get(e) + 1);
            }
        }
        List<Integer> res = new ArrayList<>();
        for (int i = 1; i <= k; i++) {
            int max = 0;
            int key = 0;
            for (Map.Entry<Integer, Integer> e: map.entrySet()) {
                if (e.getValue() > max) {
                    max = e.getValue();
                    key = e.getKey();
                }
            }
            res.add(key);
            map.remove(key);
        }
        return res;
    }
}