public class Solution {

    class Cell {
        int row;
        int col;
        int height;

        Cell(int row, int col, int height) {
            this.row = row;
            this.col = col;
            this.height = height;
        }
    }

    public int trapRainWater(int[][] heightMap) {
        if (heightMap == null || heightMap.length == 0 || heightMap[0].length == 0) {
            return 0;
        }
        PriorityQueue<Cell> minHeap = new PriorityQueue<>(1, new Comparator<Cell>() {
            @Override
            public int compare(Cell a, Cell b) {
                return a.height - b.height;
            }
        });
        int M = heightMap.length;
        int N = heightMap[0].length;
        boolean[][] isVisited = new boolean[M][N];
        for (int i = 0 ; i < M; i++) {
            isVisited[i][0] = true;
            isVisited[i][N - 1] = true;
            minHeap.offer(new Cell(i, 0, heightMap[i][0]));
            minHeap.offer(new Cell(i, N - 1, heightMap[i][N - 1]));
        }

        for (int i = 0 ; i < N; i++) {
            isVisited[0][i] = true;
            isVisited[M - 1][i] = true;
            minHeap.offer(new Cell(0, i, heightMap[0][i]));
            minHeap.offer(new Cell(M - 1, i, heightMap[M - 1][i]));
        }
        int[] x_diff = {0, 0, 1, -1};
        int[] y_diff = {1, -1, 0, 0};

        int sum = 0;
        while (minHeap.size() != 0) {
            Cell cur = minHeap.poll();
            for (int i = 0; i < 4; i++) {
                int X = cur.row + x_diff[i];
                int Y = cur.col + y_diff[i];
                if (X >= 0 && Y >= 0 && X < M && Y < N && !isVisited[X][Y]) {
                    isVisited[X][Y] = true;
                    sum += Math.max(0, cur.height - heightMap[X][Y]);
                    minHeap.offer(new Cell(X, Y, Math.max(cur.height, heightMap[X][Y])));
                }
            }
        }
        return sum;
    }
}