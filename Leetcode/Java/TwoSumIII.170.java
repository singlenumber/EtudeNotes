public class TwoSum {
    HashMap<Integer, Integer> map;
    public TwoSum(){
        map = new HashMap<>();
    }
    // Add the number to an internal data structure.
  public void add(int number) {
      Integer val = map.get(number);
      if (val == null) {
          map.put(number , 1);
      } else {
          map.put(number, val + 1);
      }
  }

    // Find if there exists any pair of numbers which sum is equal to the value.
  public boolean find(int value) {
      for (Map.Entry<Integer, Integer> e: map.entrySet()) {
          int a = e.getKey();
          int b = value - a;
          if (a == b) {
              if (map.get(a) >= 2) return true;
          } else {
              if (map.containsKey(b)) return true;
          }
      }
      return false;
  }
}


// Your TwoSum object will be instantiated and called as such:
// TwoSum twoSum = new TwoSum();
// twoSum.add(number);
// twoSum.find(value);

public class FasterSearchSum implements TwoSum {
    Set<Integer> set = new HashSet<>();
    List<Integer> nums = new ArrayList<>();
     
    public void store(int input) {
        if (!nums.isEmpty()) {
            for (int num : nums) {
                set.add(input + num);
            }
        }
         
        nums.add(input);
    }
     
    public boolean test(int val) {
        return set.contains(val);
    }
}