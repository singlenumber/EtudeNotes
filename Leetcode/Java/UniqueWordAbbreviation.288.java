class ValidWordAbbr {
    HashMap<String, String> map;
    public ValidWordAbbr(String[] dictionary) {
        map = new HashMap<>();
        for (String str: dictionary) {
            String key = helper(str);
            if (map.containsKey(key)) {
                if (!map.get(key).equals(str)) {
                    map.put(key, "");
                } 
            } else {
                map.put(key, str);
            }
        }
    }

    public boolean isUnique(String word) {
        String key = helper(word);
        return !map.containsKey(key) || map.get(key).equals(word);
    }
    
    public String helper(String str) {
        if (str.length() >= 3) {
            return str.charAt(0) + String.valueOf(str.length() - 2) + str.charAt(str.length() - 1);  
        } else {
            return str;
        }
    }
}

// Your ValidWordAbbr object will be instantiated and called as such:
// ValidWordAbbr vwa = new ValidWordAbbr(dictionary);
// vwa.isUnique("Word");
// vwa.isUnique("anotherWord");