public class Solution {
    public boolean isPerfectSquare(int num) {
        long beg = 1;
        long end = num;
        while (beg <= end) {
            long mid = beg + (end - beg) / 2;
            if (mid * mid == num) return true;
            if (mid * mid < num) beg = mid + 1;
            else if (mid * mid > num) end = mid - 1;
        }
        return false;
    }
}

//iteration
public class Solution {
    public boolean isPerfectSquare(int num) {
        long x = num;
        while (x * x > num) {
            x = (x + num / x) / 2;
        }
        return x * x == num;
    }
}

public class Solution {
    public boolean isPerfectSquare(int num) {
        int low = 1;
        int high = num;
        while (low <= high) {
            long mid = low + (high - low) / 2;
            if (mid * mid == num) {
                return true;
            } else if (mid * mid > num) {
                high = (int)mid - 1;
            } else if (mid * mid < num) {
                low = (int)mid + 1;
            }
        }
        return false;
    }
}

public class Solution {
    public boolean isPerfectSquare(int num) {
        if (num == 1) {
            return true;
        }
        for (int i = 1; i <= num / 2; i++) {
            if (i * i == num) {
                return true;
            }
        }
        return false;
    }
}