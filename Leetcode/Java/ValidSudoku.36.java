/*
 * Determine if a Sudoku is valid, according to: Sudoku Puzzles - The Rules.
 * The Sudoku board could be partially filled, where empty cells are filled with the character '.'.
 */

public class Solution {
    public boolean isValidSudoku(char[][] board) {
        int N = board.length;
        boolean[][] row = new boolean[N][N];
        boolean[][] col = new boolean[N][N];
        boolean[][] block = new boolean[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (board[i][j] == '.') {
                    continue;
                }
                int c = board[i][j] - '1';
                if (row[i][c] || col[j][c] || block[3 * (i / 3) + j / 3][c]) {
                    return false;
                }
                row[i][c] = col[j][c] = block[3 * (i / 3) + j / 3][c] = true;
            }
        }
        return true;
    }
}


public class Solution {
    public boolean isValidSudoku(char[][] board) {
        int row;
        int[] col = new int[9];
        int[] block = new int[9];
        for(int i = 0; i < 9; i++) {
            row = 0;
            for(int j = 0; j < 9; j++) {
                if(board[i][j] == '.')
                    continue;
                int bit = 1 << (board[i][j] - '1');
                if((row & bit) != 0 || (col[j] & bit) != 0 || (block[i / 3 * 3 + j / 3] & bit) != 0)
                    return false;
                row |= bit;
                col[j] |= bit;
                block[i / 3 * 3 + j / 3] |= bit;
            }
        }
        return true;
    }
}
