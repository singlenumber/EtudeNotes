public class Solution {
    public List<String> wordBreak(String s, Set<String> wordDict) {
        List<String> res = new ArrayList<>();
        int N = s.length();
        boolean[] dp = new boolean[N + 1];
        dp[N] = true;
        for (int i = N; i >= 0; i--) {
            for (int j = N; j > i; j--) {
                if (dp[j] && wordDict.contains(s.substring(i, j))) {
                    dp[i] = true;
                    break;
                }
            }
        }
        DFS(res, wordDict, "", 0, s, dp);
        return res;
    }

    public void DFS(List<String> res, Set<String> wordDict, String buffer, int pos, String str, boolean[] dp) {
        if (pos == str.length()) {
            res.add(buffer.trim());
            return;
        }
        for (int i = pos; i <= str.length(); i++) {
            if (wordDict.contains(str.substring(pos, i)) && dp[i]) {
                DFS(res, wordDict, buffer + str.substring(pos, i) + " ", i, str, dp);
            }
        }
    }
}