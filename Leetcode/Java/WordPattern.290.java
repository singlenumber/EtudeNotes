public class Solution {
    public boolean wordPattern(String pattern, String str) {
        String[] arrs = str.split(" ");
        if(pattern.length() != arrs.length){
            return false;
        }
        HashMap<Character,String> map = new HashMap<>();
        for(int i = 0; i < pattern.length(); i++){
            char c = pattern.charAt(i);
            if(!map.containsKey(c)){
                if(map.containsValue(arrs[i])){
                    return false;
                }
                map.put(c, arrs[i]);
            } else {
                if(!map.get(c).equals(arrs[i])){
                    return false;
                }
            }
        }
        return true;
    }
}