public class Solution {

    public List<String> findWords(char[][] board, String[] words) {
        HashSet<String> res = new HashSet<>();
        Trie trie = new Trie();
        for (String s: words){
            trie.insert(s);
        }
        int M = board.length;
        int N = board[0].length;
        boolean[][] isVisited = new boolean[M][N];
        for (int i = 0 ; i < M; i++) {
            for (int j = 0 ; j < N; j++) {
                dfs(board, isVisited, i, j, "", trie, res);
            }
        }
        return new ArrayList<>(res);
    }
    
    public void dfs(char[][] board, boolean[][] isVisited, int i, int j, String str, Trie trie, HashSet<String> res){
        if (i < 0 || j < 0 || i >= board.length || j >= board[0].length){
            return;
        }
        if (isVisited[i][j]) return;
        str = str + board[i][j];
        if (!trie.startWith(str)) return;
        if (trie.search(str)) {
            res.add(str);
        }
        isVisited[i][j] = true;
        dfs(board, isVisited, i + 1, j, str, trie, res);
        dfs(board, isVisited, i - 1, j, str, trie, res);
        dfs(board, isVisited, i, j + 1, str, trie, res);
        dfs(board, isVisited, i, j - 1, str, trie, res);
        isVisited[i][j] = false;
    }
}

public class Solution {
    public List<String> findWords(char[][] board, String[] words) {
        HashSet<String> res = new HashSet<>();
        Trie trie = new Trie();
        for (String str: words) {
            trie.insert(str);
        }
        int M = board.length;
        int N = board[0].length;
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                dfs(board, i, j, "", res, trie);
            }
        }
        return new ArrayList<>(res);
    }
    
    public void dfs(char[][] board, int X, int Y, String str, HashSet<String> res, Trie trie) {
        if (X < 0 || Y < 0 || X >= board.length || Y >= board[0].length) {
            return;
        }
        if (board[X][Y] == '.') {
            return;
        }
        str += board[X][Y];
        if (!trie.startWith(str)) {
            return;
        }
        if (trie.search(str)) {
            res.add(str);
        }
        char tmp = board[X][Y];
        board[X][Y] = '.';
        dfs(board, X + 1, Y, str, res, trie);
        dfs(board, X - 1, Y, str, res, trie);
        dfs(board, X, Y + 1, str, res, trie);
        dfs(board, X, Y - 1, str, res, trie);
        board[X][Y] = tmp;
    }
}


class TrieNode{
    TrieNode[] next;
    boolean isEnd;
    TrieNode(){
        next = new TrieNode[26];
        isEnd = false;
    }
}

class Trie{
    TrieNode root;
    Trie(){
        root = new TrieNode();
    }
    public void insert(String str){
        TrieNode cur = root;
        for (int i = 0 ; i < str.length(); i++){
            int idx = str.charAt(i) - 'a';
            if (cur.next[idx] == null) {
                cur.next[idx] = new TrieNode();
            }
            cur = cur.next[idx];
        }
        cur.isEnd = true;
    }
    
    public boolean search(String str){
        TrieNode cur = root;
        for (int i = 0 ; i < str.length(); i++){
            int idx = str.charAt(i) - 'a';
            if (cur.next[idx] == null) {
                return false;
            }
            cur = cur.next[idx];
        }
        return cur.isEnd == true;
    }
    
    public boolean startWith(String str){
        TrieNode cur = root;
        for (int i = 0 ; i < str.length(); i++){
            int idx = str.charAt(i) - 'a';
            if (cur.next[idx] == null) {
                return false;
            }
            cur = cur.next[idx];
        }
        return true;
    }
}
