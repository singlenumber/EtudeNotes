public class Solution {
    /**
     * @param nums: a rotated sorted array
     * @return: the minimum number in the array
     */
    public int findMin(int[] nums) {
        // write your code here
        int beg = 0;
        int end = nums.length - 1;
        while (beg <= end) {
            if (nums[beg] < nums[end]) {
                return nums[beg];
            }
            if (beg == end) {
                return nums[beg];
            }
            int mid = beg + (end - beg) / 2;
            if (nums[beg] < nums[mid]) {
                beg = mid + 1;
            } else if (nums[beg] > nums[mid]) {
                end = mid;
            } else {
                beg++;
            }
        }
        return nums[beg];
    }
}