public class Solution {
    /**
     * @param n: An integer.
     * return : An array storing 1 to the largest number with n digits.
     */
    public List<Integer> numbersByRecursion(int n) {
        List<Integer> res = new ArrayList<Integer>();
        if (n >= 0) {
            helper(n, res);
        }
        return res;
    }
    public int helper(int n, List<Integer> res) {
        if (n == 0) return 1;
        int base = helper(n-1, res);
        int size = res.size();
        for (int i = 1; i <= 9; i++) {
            int curbase = i * base;
            res.add(curbase);
            for (int j = 0; j < size; j++) {
                res.add(curbase + res.get(j));
            }
        }
        return base * 10;
    }
}