public class Solution {
    /**
     * @param string: An array of Char
     * @param length: The true length of the string
     * @return: The true length of new string
     */
    public int replaceBlank(char[] string, int length) {
        // Write your code here
        if (length == 0) {
            return 0;
        }
        int len = length;
        for (int i = 0; i < length; i++) {
            if (string[i] == ' ') {
                len += 2;
            }
        }
        int j = 1;
        string[len] = 0;
        for (int i = length - 1; i >= 0; i--) {
            if (string[i] == ' ') {
                string[len - j] = '0';
                j++;
                string[len - j] = '2';
                j++;
                string[len - j] = '%';
                j++;
            } else {
                string[len - j] = string[i];
                j++;
            }
        }
        return len;
    }
}