/**
 * Definition of TreeNode:
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left, right;
 *     public TreeNode(int val) {
 *         this.val = val;
 *         this.left = this.right = null;
 *     }
 * }
 */
 
 
public class Solution {
    /**
     *@param preorder : A list of integers that preorder traversal of a tree
     *@param inorder : A list of integers that inorder traversal of a tree
     *@return : Root of a tree
     */
    public TreeNode buildTree(int[] preorder, int[] inorder) {
        if(preorder.length == 0)
            return null;
        return buildTreeHelper(preorder, 0, preorder.length - 1, inorder, 0, inorder.length - 1);
    }

    public TreeNode buildTreeHelper(int[] preorder, int pStart, int pEnd, int[] inorder, int iStart, int iEnd){ 
        int i = 0;
        TreeNode root = new TreeNode(preorder[pStart]);
        while(inorder[iStart + i] != preorder[pStart]) {
            i++;
        }
        if(i != 0) 
            root.left = buildTreeHelper(preorder, pStart + 1, pStart + i, inorder, iStart, iStart + i - 1);
        if(pStart + i + 1 <= pEnd) 
            root.right = buildTreeHelper(preorder, pStart + i + 1, pEnd, inorder, iStart + i + 1, iEnd);
        return root;
    }
}