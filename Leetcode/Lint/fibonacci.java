class Solution {
    /**
     * @param n: an integer
     * @return an integer f(n)
     */
    public int fibonacci(int n) {
        // write your code here
        int a = 0;
        int b = 1;
        for (int i = 1; i < n; i++) {
            int c = b;
            b = a + b;
            a = c;
        }
        return a;
    }
}