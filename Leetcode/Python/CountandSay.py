class Solution(object):
    def countAndSay(self, n):
        """
        :type n: int
        :rtype: str
        """
        result = "1"
        if not n or n == 1:
          return result 
        for i in range(n - 1):
          result = self.getNext(result)
        return result
    def getNext(self, result):
      count = 1
      nextStr = ""
      length = len(result)
      for i in range(1, length):
        if result[i] == result[i - 1]:
          count += 1
        else:
          nextStr += str(count) + result[i - 1]
          count = 1
      nextStr += str(count) + result[-1]
      return nextStr