class Solution(object):
    def calculateMinimumHP(self, dungeon):
        """
        :type dungeon: List[List[int]]
        :rtype: int
        """
        M = len(dungeon)
        N = len(dungeon[0])
        
        dp = [[0] * N for i in range(M)]
        dp[M - 1][N - 1] = max(1, 1 - dungeon[M - 1][N - 1])
        
        for i in range(M - 1, -1, -1):
            for j in range(N - 1, -1, -1):
                down = None
                right = None
                if i + 1 < M:
                    down = max(1, dp[i + 1][j] - dungeon[i][j])
                if j + 1 < N:
                    right = max(1, dp[i][j + 1] - dungeon[i][j])
                if down and right:
                    dp[i][j] = min(down, right)
                elif down:
                    dp[i][j] = down
                elif right:
                    dp[i][j] = right
                    
        return dp[0][0]
        