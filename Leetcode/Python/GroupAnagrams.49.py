class Solution(object):
    def groupAnagrams(self, strs):
        """
        :type strs: List[str]
        :rtype: List[List[str]]
        """
        map = collections.defaultdict(list)
        for s in strs:
            map[''.join(sorted(s))] += s,
        return filter(lambda x : len(x) >= 1, map.values())