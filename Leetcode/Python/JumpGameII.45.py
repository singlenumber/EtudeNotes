class Solution(object):
    def jump(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if nums is None or len(nums) <= 1:
            return 0
        reach, cnt = nums[0], 0
        next = reach
        i = 0
        while i <= reach:
            next = max(next, i + nums[i])
            if i == reach:
                reach = next
                cnt += 1
            if reach >= len(nums) - 1:
                return cnt + 1
            i += 1
        return -1
        