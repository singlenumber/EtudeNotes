class Solution(object):
    def lengthOfLastWord(self, s):
        """
        :type s: str
        :rtype: int
        """
        s = s.strip()
        length = len(s)
        if length == 0:
          return 0
        result = 0
        end = length - 1
        while end >= 0:
          if s[end] != ' ':
            end -= 1
            result += 1
          else:
            break
        return result