class Solution(object):
    def longestIncreasingPath(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: int
        """
        m = len(matrix)
        if m == 0:
            return 0
        n = len(matrix[0])
        
        def dfs(x, y):
          for dx, dy in zip([1, 0, -1, 0],[0, 1, 0, -1]):
            nx, ny = x + dx, y + dy
            if 0 <= nx < m and 0 <= ny < n and matrix[nx][ny] > matrix[x][y]:
              if not dp[nx][ny]:
                dp[nx][ny] = dfs(nx, ny)
              dp[x][y] = max(dp[x][y], 1 + dp[nx][ny])
          dp[x][y] = max(dp[x][y], 1)
          return dp[x][y]

        dp = [[0] * n for x in range(m)]
        for x in range(m):
          for y in range(n):
            if not dp[x][y]:
              dp[x][y] = dfs(x, y)
        return max([max(x) for x in dp])
