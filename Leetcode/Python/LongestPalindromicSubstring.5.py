class Solution:
    def longestPalindrome(self, s):
        """
        :type s: str
        :rtype: str
        """
        res = ""
        for idx in range(0, len(s)):
            str1 = self.expand_from_center(s, idx, idx)
            str2 = self.expand_from_center(s, idx, idx + 1)
            if len(str1) > len(res):
                res = str1
            if len(str2) > len(res):
                res = str2
        return res
    
    def expand_from_center(self, s, l, r):
        while l >= 0 and r < len(s) and s[l] == s[r]:
            l -= 1
            r += 1
        return s[l + 1:r]

# Manacher
def longestPalindrome(self, s):
        """
        :type s: str
        :rtype: str
        """
        l = len(s)
        if s == None or l == 0:
            return ""
        nl = 2 * l + 1
        p = [0] * nl
        mx, pos = 0, 0
        for i in range(nl):
            if i > mx:
                p[i] = 1
            else:
                p[i] = min(p[2 * pos - i], mx - i)
            
            while i - p[i] >= 0 and i + p[i] < nl and self.get(s, i + p[i]) == self.get(s, i - p[i]):
                p[i] += 1
            
            if p[i] + i - 1 > mx:
                mx = p[i] + i - 1
                pos = i
        ml = ct = 0
        for i in range(nl):
            if p[i] > ml:
                ct = i
                ml = p[i]
        return s[((ct - ml + 1) // 2):((ct + ml) // 2)]

    def get(self, s, i):
        if i % 2 == 0:
            return "#"
        else:
            return s[i // 2]