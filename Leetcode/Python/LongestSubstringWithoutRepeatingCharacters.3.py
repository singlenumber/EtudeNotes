class Solution(object):
    def lengthOfLongestSubstring(self, s):
        """
        :type s: str
        :rtype: int
        """
        map = {}
        res = beg = end = 0
        while end < len(s):
            if s[end] in map:
                beg = max(beg, map[s[end]] + 1)
            res = max(res, end - beg + 1)
            map[s[end]] = end
            end = end + 1
        return res