class Solution(object):
    def maxProduct(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        big = small = res = nums[0]
        for num in nums[1:]:
            big, small = max(num, num * small, num * big), min(num, num * small, num * big)
            res = max(res, big)
        return res