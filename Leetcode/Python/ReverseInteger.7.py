class Solution:
    def reverse(self, x):
        """
        :type x: int
        :rtype: int
        """
        res = 0
        sign = 1
        if x < 0:
            sign = -1
            x = -x
        while x > 0:
            res = res * 10 + x % 10
            x //= 10
        return 0 if res > 2 ** 31 else res * sign