/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
	public ArrayList&lt;ArrayList&lt;Integer&gt;&gt; fourSum(int[] num, int target) {
		ArrayList&lt;ArrayList&lt;Integer&gt;&gt; rst = new ArrayList&lt;ArrayList&lt;Integer&gt;&gt;();
		Arrays.sort(num);

		for (int i = 0; i &lt; num.length - 3; i++) {
			if (i != 0 &amp;&amp; num[i] == num[i - 1]) {
				continue;
			}

			for (int j = i + 1; j &lt; num.length - 2; j++) {
				if (j != i + 1 &amp;&amp; num[j] == num[j - 1])
					continue;

				int left = j + 1;
				int right = num.length - 1;
				while (left &lt; right) {
					int sum = num[i] + num[j] + num[left] + num[right];
					if (sum &lt; target) {
						left++;
					} else if (sum &gt; target) {
						right--;
					} else {
						ArrayList&lt;Integer&gt; tmp = new ArrayList&lt;Integer&gt;();
						tmp.add(num[i]);
						tmp.add(num[j]);
						tmp.add(num[left]);
						tmp.add(num[right]);
						rst.add(tmp);
						left++;
						right--;
						while (left &lt; right &amp;&amp; num[left] == num[left - 1]) {
							left++;
						}
						while (left &lt; right &amp;&amp; num[right] == num[right + 1]) {
							right--;
						}
					}
				}
			}
		}

		return rst;
	}
}
