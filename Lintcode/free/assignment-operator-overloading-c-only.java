/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

class Solution {
public:
    char *m_pData;
    Solution() {
        this-&gt;m_pData = NULL;
    }
    Solution(char *pData) {
        if (pData) {
            m_pData = new char[strlen(pData) + 1];
            strcpy(m_pData, pData);
        } else {
            m_pData = NULL;
        }
    }

    // Implement an assignment operator
    Solution operator=(const Solution &amp;object) {
        if (this == &amp;object) {
            return *this;
        }
        
        if (object.m_pData) {
            char *temp = m_pData;
            try {
                m_pData = new char[strlen(object.m_pData)+1];
                strcpy(m_pData, object.m_pData);
                if (temp)
                    delete[] temp;
            } catch (bad_alloc&amp; e) {
                m_pData = temp;
            }
        } else {
            m_pData = NULL;
        }
        return *this;
    }
};
