/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    public int shortestDistance(int[][] grid) {
        if (grid==null || grid.length==0 || grid[0].length==0) return -1;
        int m = grid.length;
        int n = grid[0].length;
        int[][] dist = new int[m][n];
        int[][] reach = new int[m][n];
        int houseNum = 0;
        int[][] directions = {{-1,0},{1,0},{0,-1},{0,1}};
        
        for (int i=0; i&lt;m; i++) {
            for (int j=0; j&lt;n; j++) {
                if (grid[i][j] == 1) {
                    houseNum++;
                    int level = 0;
                    boolean[][] visited = new boolean[m][n];
                    LinkedList&lt;Integer&gt; queue = new LinkedList&lt;Integer&gt;();
                    queue.offer(i*n+j);
                    visited[i][j] = true;
                    while (!queue.isEmpty()) {
                        int size = queue.size();
                        for (int t=0; t&lt;size; t++) {
                            int cur = queue.poll();
                            int x = cur/n;
                            int y = cur%n;
                            for (int[] dir : directions) {
                                int xnew = x + dir[0];
                                int ynew = y + dir[1];
                                if (xnew&gt;=0 &amp;&amp; xnew&lt;m &amp;&amp; ynew&gt;=0 &amp;&amp; ynew&lt;n &amp;&amp; !visited[xnew][ynew] &amp;&amp; grid[xnew][ynew]==0) {
                                    queue.offer(xnew*n+ynew);
                                    visited[xnew][ynew] = true;
                                    dist[xnew][ynew] += level+1;
                                    reach[xnew][ynew]++;
                                }
                            }
                        }
                        level++;
                    }
                }
            }
        }
        
        int minDist = Integer.MAX_VALUE;
        for (int i=0; i&lt;m; i++) {
            for (int j=0; j&lt;n; j++) {
                if (grid[i][j]==0 &amp;&amp; reach[i][j] == houseNum) {
                    minDist = Math.min(minDist, dist[i][j]);
                }
            }
        }
        return minDist==Integer.MAX_VALUE? -1 : minDist;
    }
}