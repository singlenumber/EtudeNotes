/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

// 方法一 
public class Solution {
    /**
     * @param grid a 2D grid
     * @return an integer
     */
    public int shortestDistance(int[][] grid) {
        // Write your code here
        int n = grid.length;
        if (n == 0)
            return -1;

        int m = grid[0].length;
        if (m == 0)
            return -1;

        List&lt;Integer&gt; sumx = new ArrayList&lt;Integer&gt;();
        List&lt;Integer&gt; sumy = new ArrayList&lt;Integer&gt;();
        List&lt;Integer&gt; x = new ArrayList&lt;Integer&gt;();
        List&lt;Integer&gt; y = new ArrayList&lt;Integer&gt;();

        int result = Integer.MAX_VALUE;
        for (int i = 0; i &lt; n; ++i)
            for (int j = 0; j &lt; m; ++j)
                if (grid[i][j] == 1) {
                    x.add(i);
                    y.add(j);
                }
        
        Collections.sort(x);
        Collections.sort(y);

        int total = x.size();

        sumx.add(0);
        sumy.add(0);
        for (int i = 1; i &lt;= total; ++i) {
            sumx.add(sumx.get(i-1) + x.get(i-1));
            sumy.add(sumy.get(i-1) + y.get(i-1));
        }

        for (int i = 0; i &lt; n; ++i)
            for (int j = 0; j &lt; m; ++j)
                if (grid[i][j] == 0) {
                    int cost_x = get_cost(x, sumx, i, total);
                    int cost_y = get_cost(y, sumy, j, total);
                    if (cost_x + cost_y &lt; result)
                        result = cost_x + cost_y;
                }

        return result;
    }

    public int get_cost(List&lt;Integer&gt; x, List&lt;Integer&gt; sum, int pos, int n) {
        if (n == 0)
            return 0;
        if (x.get(0) &gt; pos)
            return sum.get(n) - pos * n;

        int l = 0, r = n - 1;
        while (l + 1 &lt; r) {
            int mid = l + (r - l) / 2;
            if (x.get(mid) &lt;= pos)
                l = mid;
            else
                r = mid - 1;
        }
        
        int index = 0;
        if (x.get(r) &lt;= pos)
            index = r;
        else
            index = l;
        return sum.get(n) - sum.get(index + 1) - pos * (n - index - 1) + 
               (index + 1) * pos - sum.get(index + 1);
    }
}

// 方法二见 c++