/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

//记忆化
public class Solution {
    public int maxCoins(int[] nums) {
        int n = nums.length;
        int [][]dp = new int [n+2][n+2];
        int [][]visit = new int[n+2][n+2]; 
        int [] arr = new int [n+2];
        for (int i = 1; i &lt;= n; i++){
        	arr[i] = nums[i-1];
        }
        arr[0] = 1;
        arr[n+1] = 1;
        
        return search(arr, dp, visit, 1 , n);
    }
    public int search(int []arr, int [][]dp, int [][]visit, int left, int right) {
        if(visit[left][right] == 1)
        	return dp[left][right];
    	
    	int res = 0;
        for (int k = left; k &lt;= right; ++k) {
        	int midValue =  arr[left - 1] * arr[k] * arr[right + 1];
        	int leftValue = search(arr, dp, visit, left, k - 1);
        	int rightValue = search(arr, dp, visit, k + 1, right);
            res = Math.max(res, leftValue + midValue + rightValue);
        }
        visit[left][right] = 1;
        dp[left][right] = res;
        return res;
    }
}


// 非递归 请见c++版本