/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    public  ArrayList&lt;ArrayList&lt;Integer&gt;&gt; combinationSum(int[] candidates, int target) {
        ArrayList&lt;ArrayList&lt;Integer&gt;&gt; result = new ArrayList&lt;ArrayList&lt;Integer&gt;&gt;();
        if (candidates == null) {
            return result;
        }

        ArrayList&lt;Integer&gt; path = new ArrayList&lt;Integer&gt;();
        Arrays.sort(candidates);
        helper(candidates, target, path, 0, result);

        return result;
    }

     void helper(int[] candidates, int target, ArrayList&lt;Integer&gt; path, int index,
        ArrayList&lt;ArrayList&lt;Integer&gt;&gt; result) {
        if (target == 0) {
            result.add(new ArrayList&lt;Integer&gt;(path));
            return;
        }

        int prev = -1;
        for (int i = index; i &lt; candidates.length; i++) {
            if (candidates[i] &gt; target) {
                break;
            }

            if (prev != -1 &amp;&amp; prev == candidates[i]) {
                continue;
            }

            path.add(candidates[i]);
            helper(candidates, target - candidates[i], path, i, result);
            path.remove(path.size() - 1);

            prev = candidates[i];
        }
    }
}
