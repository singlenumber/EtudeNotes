/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    public ArrayList&lt;ArrayList&lt;Integer&gt;&gt; combine(int n, int k) {
        ArrayList&lt;ArrayList&lt;Integer&gt;&gt; rst = new ArrayList&lt;ArrayList&lt;Integer&gt;&gt;();
        ArrayList&lt;Integer&gt; solution = new ArrayList&lt;Integer&gt;();
        
        helper(rst, solution, n, k, 1);
        return rst;
    }
    
    private void helper(
        ArrayList&lt;ArrayList&lt;Integer&gt;&gt; rst, 
        ArrayList&lt;Integer&gt; solution, 
        int n, 
        int k, 
        int start) {

        if (solution.size() == k){
            rst.add(new ArrayList(solution));
            return;
        }
        
        for(int i = start; i&lt;= n; i++){
            solution.add(i);
            
            // the new start should be after the next number after i
            helper(rst, solution, n, k, i+1); 
            solution.remove(solution.size() - 1);
        }
    }
}

