/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    /**
     * @param n an integer
     * @return a list of Map.Entry&lt;sum, probability&gt;
     */
    public List&lt;Map.Entry&lt;Integer, Double&gt;&gt; dicesSum(int n) {
        // Write your code here
        // Ps. new AbstractMap.SimpleEntry&lt;Integer, Double&gt;(sum, pro)
        // to create the pair.
        List&lt;Map.Entry&lt;Integer, Double&gt;&gt; results = 
                new ArrayList&lt;Map.Entry&lt;Integer, Double&gt;&gt;();
        
        double[][] f = new double[n + 1][6 * n + 1];
        for (int i = 1; i &lt;= 6; ++i)
            f[1][i] = 1.0 / 6;

        for (int i = 2; i &lt;= n; ++i)
            for (int j = i; j &lt;= 6 * n; ++j) {
                for (int k = 1; k &lt;= 6; ++k)
                    if (j &gt; k)
                        f[i][j] += f[i - 1][j - k];

                f[i][j] /= 6.0;
            }

        for (int i = n; i &lt;= 6 * n; ++i) 
            results.add(new AbstractMap.SimpleEntry&lt;Integer, Double&gt;(i, f[n][i]));

        return results;
    }
}
