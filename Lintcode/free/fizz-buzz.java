/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

class Solution {
    /**
     * param n: As description.
     * return: A list of strings.
     */
    public ArrayList&lt;String&gt; fizzBuzz(int n) {
        ArrayList&lt;String&gt; results = new ArrayList&lt;String&gt;();
        for (int i = 1; i &lt;= n; i++) {
            if (i % 15 == 0) {
                results.add(&quot;fizz buzz&quot;);
            } else if (i % 5 == 0) {
                results.add(&quot;buzz&quot;);
            } else if (i % 3 == 0) {
                results.add(&quot;fizz&quot;);
            } else {
                results.add(String.valueOf(i));
            }
        }
        return results;
    }
}