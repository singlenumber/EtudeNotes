/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    public ArrayList&lt;String&gt; generateParenthesis(int n) {
        ArrayList&lt;String&gt; result = new ArrayList&lt;String&gt;();
        if (n &lt;= 0) {
            return result;
        }
        helper(result, &quot;&quot;, n, n);
        return result;
    }
    
	public void helper(ArrayList&lt;String&gt; result,
	                   String paren, // current paren
	                   int left,     // how many left paren we need to add
	                   int right) {  // how many right paren we need to add
		if (left == 0 &amp;&amp; right == 0) {
			result.add(paren);
			return;
		}

        if (left &gt; 0) {
		    helper(result, paren + &quot;(&quot;, left - 1, right);
        }
        
        if (right &gt; 0 &amp;&amp; left &lt; right) {
		    helper(result, paren + &quot;)&quot;, left, right - 1);
        }
	}
}