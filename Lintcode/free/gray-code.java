/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    public ArrayList&lt;Integer&gt; grayCode(int n) {
        ArrayList&lt;Integer&gt; result = new ArrayList&lt;Integer&gt;();
        if (n &lt;= 1) {
            for (int i = 0; i &lt;= n; i++){
                result.add(i);
            }
            return result;
        }
        result = grayCode(n - 1);
        ArrayList&lt;Integer&gt; r1 = reverse(result);
        int x = 1 &lt;&lt; (n-1);
        for (int i = 0; i &lt; r1.size(); i++) {
            r1.set(i, r1.get(i) + x);
        }
        result.addAll(r1);
        return result;
    }
    
    public ArrayList&lt;Integer&gt; reverse (ArrayList&lt;Integer&gt; r) {
        ArrayList&lt;Integer&gt; rev = new ArrayList&lt;Integer&gt;();
        for (int i = r.size() - 1; i &gt;= 0; i--) {
            rev.add(r.get(i));
        }
        return rev;
    }
}

