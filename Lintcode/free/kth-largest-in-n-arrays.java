/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

import java.util.Comparator;  
import java.util.PriorityQueue;  
import java.util.Queue; 

class Node {
    public int value, from_id, index;
    public Node(int _v, int _id, int _i) {
        this.value = _v;
        this.from_id = _id;
        this.index = _i;
    }
}

public class Solution {
    /**
     * @param arrays a list of array
     * @param k an integer
     * @return an integer, K-th largest element in N arrays
     */
    public int KthInArrays(int[][] arrays, int k) {
        // Write your code here
        Queue&lt;Node&gt; queue =  new PriorityQueue&lt;Node&gt;(k, new Comparator&lt;Node&gt;() {  
                public int compare(Node o1, Node o2) {  
                    if (o1.value &gt; o2.value)
                        return -1;
                    else if (o1.value &lt; o2.value)
                        return 1;
                    else
                        return 0;
                }
            }); 

        int n = arrays.length;
        int i;
        for (i = 0; i &lt; n; ++i) {
            Arrays.sort(arrays[i]);
            
            if (arrays[i].length &gt; 0) {
                int from_id = i;
                int index = arrays[i].length - 1;
                int value = arrays[i][index];
                queue.add(new Node(value, from_id, index));
            }
        }

        for (i  = 0; i &lt; k; ++i) {
            Node temp = queue.poll();
            int from_id = temp.from_id;
            int index = temp.index;
            int value = temp.value;
            
            if (i == k - 1)
                return value;
            
            if (index &gt; 0) {
                index --;
                value = arrays[from_id][index];
                queue.add(new Node(value, from_id, index));
            }
        }

        return -1;
    }
}
