/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

class Solution {

public:

    /*

     * @param k: The number k.

     * @return: The kth prime number as description.

     */

    long long kthPrimeNumber(int k) {

        priority_queue&lt;long long&gt; heap;

        unordered_map&lt;long long, bool&gt; hash;

        

        long long primes[3];

        primes[0] = 3; primes[1] = 5; primes[2] = 7;

        heap.push(-3); heap.push(-5); heap.push(-7);

        

        for (int i = 0; i &lt; k - 1; i++) {

            long long top = -heap.top();

            heap.pop();

            

            for (int j = 0; j &lt; 3; j++) {

                if (hash.find(top * primes[j]) == hash.end()) {

                    hash[top * primes[j]] = true;

                    heap.push(-top * primes[j]);

                }

            }

        }

        return -heap.top();

    }

};
