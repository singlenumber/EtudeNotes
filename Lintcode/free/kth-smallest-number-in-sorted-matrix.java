/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

class Number {
    public int x, y, val;
    public Number(int x, int y, int val) {
        this.x = x;
        this.y = y;
        this.val = val;
    }
}
class NumComparator implements Comparator&lt;Number&gt; {
    public int compare(Number a, Number b) {
        return a.val - b.val;
    }
}

public class Solution {
    /**
     * @param matrix: a matrix of integers
     * @param k: an integer
     * @return: the kth smallest number in the matrix
     */
    private boolean valid(int x, int y, int[][] matrix, boolean[][] hash) {
        if(x &lt; matrix.length &amp;&amp; y &lt; matrix[x].length &amp;&amp; !hash[x][y]) {
            return true;
        }
        return false;
    }
    
    int dx[] = {0,1};
    int dy[] = {1,0};
    public int kthSmallest(int[][] matrix, int k) {
        // write your code here
        if (matrix == null || matrix.length == 0) {
            return -1;
        }
        if (matrix.length * matrix[0].length &lt; k) {
            return -1;
        }
        PriorityQueue&lt;Number&gt; heap = new PriorityQueue&lt;Number&gt;(k, new NumComparator());
        boolean[][] hash = new boolean[matrix.length][matrix[0].length];
        heap.add(new Number(0, 0, matrix[0][0]));
        hash[0][0] = true;
        
        for (int i = 0; i &lt; k - 1; i ++) {
            Number smallest = heap.poll();
            for (int j = 0; j &lt; 2; j++) {
                int nx = smallest.x + dx[j];
                int ny = smallest.y + dy[j];
                if (valid(nx, ny, matrix, hash)) {
                    hash[nx][ny] = true;
                    heap.add(new Number(nx, ny, matrix[nx][ny]));
                }
            }
        }
        return heap.peek().val;
    }
}