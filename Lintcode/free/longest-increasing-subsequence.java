/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    /**
     * @param nums: The integer array
     * @return: The length of LIS (longest increasing subsequence)
     */
   
    
    public int longestIncreasingSubsequence(int[] nums) {
        int []f = new int[nums.length];
        int max = 0;
        for (int i = 0; i &lt; nums.length; i++) {
            f[i] = 1;
            for (int j = 0; j &lt; i; j++) {
                if (nums[j] &lt; nums[i]) {
                    f[i] = f[i] &gt; f[j] + 1 ? f[i] : f[j] + 1;
                }
            }
            if (f[i] &gt; max) {
                max = f[i];
            }
        }
        return max;
    }
}


// O(nlogn) Binary Search
public class Solution {
    /**
     * @param nums: The integer array
     * @return: The length of LIS (longest increasing subsequence)
     */
    public int longestIncreasingSubsequence(int[] nums) {
        int[] minLast = new int[nums.length + 1];
        minLast[0] = -1;
        for (int i = 1; i &lt;= nums.length; i++) {
            minLast[i] = Integer.MAX_VALUE;
        }
        
        for (int i = 0; i &lt; nums.length; i++) {
            // find the first number in minLast &gt; nums[i]
            int index = binarySearch(minLast, nums[i]);
            minLast[index] = nums[i];
        }
        
        for (int i = nums.length; i &gt;= 1; i--) {
            if (minLast[i] != Integer.MAX_VALUE) {
                return i;
            }
        }
        
        return 0;
    }
    
    // find the first number &gt; num
    private int binarySearch(int[] minLast, int num) {
        int start = 0, end = minLast.length - 1;
        while (start + 1 &lt; end) {
            int mid = (end - start) / 2 + start;
            if (minLast[mid] &lt; num) {
                start = mid;
            } else {
                end = mid;
            }
        }
        
        if (minLast[start] &gt; num) {
            return start;
        }
        return end;
    }
}
