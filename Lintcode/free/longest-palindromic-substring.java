/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    public String longestPalindrome(String s) {
        if (s == null || s.length() == 0) {
            return &quot;&quot;;
        }
        
        int length = s.length();    
        int max = 0;
        String result = &quot;&quot;;
        for(int i = 1; i &lt;= 2 * length - 1; i++){
            int count = 1;
            while(i - count &gt;= 0 &amp;&amp; i + count &lt;= 2 * length  &amp;&amp; get(s, i - count) == get(s, i + count)){
                count++;
            }
            count--; // there will be one extra count for the outbound #
            if(count &gt; max) {
                result = s.substring((i - count) / 2, (i + count) / 2);
                max = count;
            }
        }
        
        return result;
    }
    
    private char get(String s, int i) {
        if(i % 2 == 0)
            return &#39;#&#39;;
        else 
            return s.charAt(i / 2);
    }
}
