/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

// 写法一 leetcode解
class MedianFinder {
    public PriorityQueue&lt;Integer&gt; minheap, maxheap;
    public MedianFinder() {
        maxheap = new PriorityQueue&lt;Integer&gt;(Collections.reverseOrder());
        minheap = new PriorityQueue&lt;Integer&gt;();
    }
    
    // Adds a number into the data structure.
    public void addNum(int num) {
        maxheap.add(num);
        minheap.add(maxheap.poll());
        if (maxheap.size() &lt; minheap.size()) {
            maxheap.add(minheap.poll());
        }
    }

    // Returns the median of current data stream
    public double findMedian() {
        if (maxheap.size() == minheap.size()) {
            return (maxheap.peek() + minheap.peek()) * 0.5;
        } else {
            return maxheap.peek();
        }
    }
};

// 写法二 lintcode 解
class Solution {
public:
    /**
     * @param nums: A list of integers.
     * @return: The median of numbers
     */
    vector&lt;int&gt; medianII(vector&lt;int&gt; &amp;nums) {
        vector&lt;int&gt; result;
        if (nums.size() == 0) {
            return result;
        }
        
        int median = nums[0];
        priority_queue&lt;int&gt; maxHeap, minHeap;
        
        result.push_back(median);
        for (int i = 1; i &lt; nums.size(); i++) {
            if (nums[i] &lt; median) {
                maxHeap.push(nums[i]);
            } else {
                minHeap.push(-nums[i]);
            }
            
            if (maxHeap.size() &gt; minHeap.size()) {
                minHeap.push(-median);
                median = maxHeap.top();
                maxHeap.pop();
            } else if (maxHeap.size() + 1 &lt; minHeap.size()) {
                maxHeap.push(median);
                median = -minHeap.top();
                minHeap.pop();
            }
            
            result.push_back(median);
        }
        
        return result;
    }
};
