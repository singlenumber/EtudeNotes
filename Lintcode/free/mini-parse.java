/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

/**
 * // This is the interface that allows for creating nested lists.
 * // You should not implement it, or speculate about its implementation
 * public interface NestedInteger {
 *     // Constructor initializes an empty nested list.
 *     public NestedInteger();
 *
 *     // Constructor initializes a single integer.
 *     public NestedInteger(int value);
 *
 *     // @return true if this NestedInteger holds a single integer, rather than a nested list.
 *     public boolean isInteger();
 *
 *     // @return the single integer that this NestedInteger holds, if it holds a single integer
 *     // Return null if this NestedInteger holds a nested list
 *     public Integer getInteger();
 *
 *     // Set this NestedInteger to hold a single integer.
 *     public void setInteger(int value);
 *
 *     // Set this NestedInteger to hold a nested list and adds a nested integer to it.
 *     public void add(NestedInteger ni);
 *
 *     // @return the nested list that this NestedInteger holds, if it holds a nested list
 *     // Return null if this NestedInteger holds a single integer
 *     public List&lt;NestedInteger&gt; getList();
 * }
 */
public class Solution {
    public NestedInteger deserialize(String s) {
        
        // return a NestedInteger with a single integer
        if (s.charAt(0) != &#39;[&#39;) {
            return new NestedInteger(Integer.parseInt(s));
        }
        
        int len = s.length();
        Stack&lt;NestedInteger&gt; stack = new Stack&lt;&gt;();
        StringBuilder num = new StringBuilder();
        
        for (int i = 0; i &lt; len; i++) {
            switch (s.charAt(i)) {
                case &#39;[&#39;:
                    stack.push(new NestedInteger());
                    break;
                    
                case &#39;,&#39;:
                    // judge if it&#39;s a num before &#39;,&#39;
                    if (num.length() != 0) {
                        stack.peek().add(new NestedInteger(Integer.parseInt(num.toString())));
                        clearNum(num);
                    }
                    break;
                    
                case &#39;]&#39;:
                    // judge if it&#39;s a num before &#39;]&#39;
                    if (num.length() != 0) {
                        stack.peek().add(new NestedInteger(Integer.parseInt(num.toString())));
                        clearNum(num);
                    }
                    NestedInteger ni = stack.pop();
                    
                    if (!stack.isEmpty()) {
                        stack.peek().add(ni);
                    } else {
                        return ni;
                    }
                    break;
                    
                default:
                    num.append(s.charAt(i));
                    break;
            }
        }
        
        return null;
    }
    
    public void clearNum(StringBuilder sb) {
        sb.delete(0, sb.length());
    }
}