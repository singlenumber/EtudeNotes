/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

class Solution {
    /**
     * Get all distinct N-Queen solutions
     * @param n: The number of queens
     * @return: All distinct solutions
     * For example, A string &#39;...Q&#39; shows a queen on forth position
     */
    ArrayList&lt;ArrayList&lt;String&gt;&gt; solveNQueens(int n) {
        // write your code here
        ArrayList&lt;ArrayList&lt;String&gt;&gt; resultList = new ArrayList&lt;&gt;();
        if (n &lt;= 0) {
            return resultList;
        }
        int[] row = new int[n];
        solveNQueensCore(resultList, row, n, 0);
        return resultList;
    }
    
    private void solveNQueensCore(ArrayList&lt;ArrayList&lt;String&gt;&gt; resultList,
                              int[] row,
                              int n,
                              int index) {
        if (index == n) {
            ArrayList&lt;String&gt; singleResult = translateString(row);
            resultList.add(singleResult);
            return;
        }
        
        for (int i = 0; i &lt; n; i++) {
            if (isValid(row, index, i)) {
                row[index] = i;
                solveNQueensCore(resultList, row, n, index + 1);
                row[index] = 0;
            }
        }
    }
    
    private ArrayList&lt;String&gt; translateString(int[] row) {
        ArrayList&lt;String&gt; resultList = new ArrayList&lt;&gt;();
        for (int i = 0; i &lt; row.length; i++) {
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j &lt; row.length; j++) {
                if (j == row[i]) {
                    sb.append(&#39;Q&#39;);
                }
                else {
                    sb.append(&#39;.&#39;);
                }
            }
            resultList.add(sb.toString());
        }
        return resultList;
    }
    
    private boolean isValid(int[] row, int rowNum, int columnNum) {
        for (int i = 0; i &lt; rowNum; i++) {
            if (row[i] == columnNum) {
                return false;
            }
            if (Math.abs(row[i] - columnNum) == Math.abs(i - rowNum)) {
                return false;
            }
        }
        return true;
    }
    
};
