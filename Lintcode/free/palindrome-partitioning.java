/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    public ArrayList&lt;ArrayList&lt;String&gt;&gt; partition(String s) {
        ArrayList&lt;ArrayList&lt;String&gt;&gt; result = new ArrayList&lt;ArrayList&lt;String&gt;&gt;();
        if (s == null) {
            return result;
        }

        ArrayList&lt;String&gt; path = new ArrayList&lt;String&gt;();
        helper(s, path, 0, result);

        return result;
    }

    private boolean isPalindrome(String s) {
        int beg = 0;
        int end = s.length() - 1;
        while (beg &lt; end) {
            if (s.charAt(beg) != s.charAt(end)) {
                return false;
            }

            beg++;
            end--;
        }

        return true;
    }

    private void helper(String s, ArrayList&lt;String&gt; path, int pos,
            ArrayList&lt;ArrayList&lt;String&gt;&gt; result) {
        if (pos == s.length()) {
            result.add(new ArrayList&lt;String&gt;(path));
            return;
        }

        for (int i = pos + 1; i &lt;= s.length(); i++) {
            String prefix = s.substring(pos, i);
            if (!isPalindrome(prefix)) {
                continue;
            }

            path.add(prefix);
            helper(s, path, i, result);
            path.remove(path.size() - 1);
        }
    }
}





------------------------------------------------
 
public class Solution {
    public ArrayList&lt;ArrayList&lt;String&gt;&gt; partition(String s) {
        ArrayList&lt;ArrayList&lt;String&gt;&gt; result = new ArrayList&lt;ArrayList&lt;String&gt;&gt;();
        if (s == null) {
            return result;
        }

        ArrayList&lt;String&gt; path = new ArrayList&lt;String&gt;();
        helper(s, path, 0, result);

        return result;
    }

    private boolean isPalindrome(String s) {
        int beg = 0;
        int end = s.length() - 1;
        while (beg &lt; end) {
            if (s.charAt(beg) != s.charAt(end)) {
                return false;
            }

            beg++;
            end--;
        }

        return true;
    }

    private void helper(String s, ArrayList&lt;String&gt; path, int pos,
            ArrayList&lt;ArrayList&lt;String&gt;&gt; result) {
        if (pos == s.length()) {
            result.add(new ArrayList&lt;String&gt;(path));
            return;
        }

        for (int i = pos; i &lt; s.length(); i++) {
            String prefix = s.substring(pos, i + 1);
            if (!isPalindrome(prefix)) {
                continue;
            }

            path.add(prefix);
            helper(s, path, i + 1, result);
            path.remove(path.size() - 1);
        }
    }
}