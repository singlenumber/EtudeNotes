/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    public List&lt;List&lt;Integer&gt;&gt; permute(int[] nums) {
         ArrayList&lt;List&lt;Integer&gt;&gt; rst = new ArrayList&lt;List&lt;Integer&gt;&gt;();
         if (nums == null) {
             return rst; 
         }
         
         if (nums.length == 0) {
            rst.add(new ArrayList&lt;Integer&gt;());
            return rst;
         }

         ArrayList&lt;Integer&gt; list = new ArrayList&lt;Integer&gt;();
         helper(rst, list, nums);
         return rst;
    }
    
    public void helper(ArrayList&lt;List&lt;Integer&gt;&gt; rst, ArrayList&lt;Integer&gt; list, int[] nums){
        if(list.size() == nums.length) {
            rst.add(new ArrayList&lt;Integer&gt;(list));
            return;
        }
        
        for(int i = 0; i &lt; nums.length; i++){
            if(list.contains(nums[i])){
                continue;
            }
            list.add(nums[i]);
            helper(rst, list, nums);
            list.remove(list.size() - 1);
        }
        
    }
}

// Non-Recursion
class Solution {
    /**
     * @param nums: A list of integers.
     * @return: A list of permutations.
     */
    public List&lt;List&lt;Integer&gt;&gt; permute(int[] nums) {
        ArrayList&lt;List&lt;Integer&gt;&gt; permutations
             = new ArrayList&lt;List&lt;Integer&gt;&gt;();
        if (nums == null) {
            
            return permutations;
        }

        if (nums.length == 0) {
            permutations.add(new ArrayList&lt;Integer&gt;());
            return permutations;
        }
        
        int n = nums.length;
        ArrayList&lt;Integer&gt; stack = new ArrayList&lt;Integer&gt;();
        
        stack.add(-1);
        while (stack.size() != 0) {
            Integer last = stack.get(stack.size() - 1);
            stack.remove(stack.size() - 1);
            
            // increase the last number
            int next = -1;
            for (int i = last + 1; i &lt; n; i++) {
                if (!stack.contains(i)) {
                    next = i;
                    break;
                }
            }
            if (next == -1) {
                continue;
            }
            
            // generate the next permutation
            stack.add(next);
            for (int i = 0; i &lt; n; i++) {
                if (!stack.contains(i)) {
                    stack.add(i);
                }
            }
            
            // copy to permutations set
            ArrayList&lt;Integer&gt; permutation = new ArrayList&lt;Integer&gt;();
            for (int i = 0; i &lt; n; i++) {
                permutation.add(nums[stack.get(i)]);
            }
            permutations.add(permutation);
        }
        
        return permutations;
    }
}