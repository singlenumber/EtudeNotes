/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    /**
     * @param A an integer array
     * @return void
     */
    public void sortIntegers2(int[] A) {
        quickSort(A, 0, A.length - 1);
    }
    
    private void quickSort(int[] A, int start, int end) {
        if (start &gt;= end) {
            return;
        }
        
        int left = start, right = end;
        // key point 1: pivot is the value, not the index
        int pivot = A[(start + end) / 2];

        // key point 2: every time you compare left &amp; right, it should be 
        // left &lt;= right not left &lt; right
        while (left &lt;= right) {
            // key point 3: A[left] &lt; pivot not A[left] &lt;= pivot
            while (left &lt;= right &amp;&amp; A[left] &lt; pivot) {
                left++;
            }
            // key point 3: A[right] &gt; pivot not A[right] &gt;= pivot
            while (left &lt;= right &amp;&amp; A[right] &gt; pivot) {
                right--;
            }
            if (left &lt;= right) {
                int temp = A[left];
                A[left] = A[right];
                A[right] = temp;
                
                left++;
                right--;
            }
        }
        
        quickSort(A, start, right);
        quickSort(A, left, end);
    }
}
