/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

/**
 * Definition of TreeNode:
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left, right;
 *     public TreeNode(int val) {
 *         this.val = val;
 *         this.left = this.right = null;
 *     }
 * }
 */
public class Solution {
    /**
     * @param root: The root of the binary search tree.
     * @param value: Remove the node with given value.
     * @return: The root of the binary search tree after removal.
     */
    public TreeNode removeNode(TreeNode root, int value) {
        TreeNode dummy = new TreeNode(0);
        dummy.left = root;
        
        TreeNode parent = findNode(dummy, root, value);
        TreeNode node;
        if (parent.left != null &amp;&amp; parent.left.val == value) {
            node = parent.left;
        } else if (parent.right != null &amp;&amp; parent.right.val == value) {
            node = parent.right;
        } else {
            return dummy.left;
        }
        
        deleteNode(parent, node);
        
        return dummy.left;
    }
    
    private TreeNode findNode(TreeNode parent, TreeNode node, int value) {
        if (node == null) {
            return parent;
        }
        
        if (node.val == value) {
            return parent;
        }
        if (value &lt; node.val) {
            return findNode(node, node.left, value);
        } else {
            return findNode(node, node.right, value);
        }
    }
    
    private void deleteNode(TreeNode parent, TreeNode node) {
        if (node.right == null) {
            if (parent.left == node) {
                parent.left = node.left;
            } else {
                parent.right = node.left;
            }
        } else {
            TreeNode temp = node.right;
            TreeNode father = node;
            
            while (temp.left != null) {
                father = temp;
                temp = temp.left;
            }
            
            if (father.left == temp) {
                father.left = temp.right;
            } else {
                father.right = temp.right;
            }
            
            if (parent.left == node) {
                parent.left = temp;
            } else {
                parent.right = temp;
            }
            
            temp.left = node.left;
            temp.right = node.right;
        }
    }
}

/*
Given a root of Binary Search Tree with unique value for each node.  Remove the node with given value. 
If there is no such a node with given value in the binary search tree, do nothing. You should keep the tree still
a binary search tree after removal.
*/

/**
 * Definition of TreeNode:
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left, right;
 *     public TreeNode(int val) {
 *         this.val = val;
 *         this.left = this.right = null;
 *     }
 * }
 */
public class Solution {
    /**
     * @param root: The root of the binary search tree.
     * @param value: Remove the node with given value.
     * @return: The root of the binary search tree after removal.
     */
    public TreeNode removeNode(TreeNode root, int value) {
        if(root == null)
            return root;
        TreeNode cur = root;
        TreeNode pre = null;
        boolean found = false;
        while(cur != null && !found){
            if(cur.val == value){
                found = true;
            }else if(cur.val > value){
                pre = cur;
                cur = cur.left;
            }else{
                pre = cur;
                cur = cur.right;
            }
        }
        if(found){
            if(cur.left == null && cur.right == null){
                if(pre == null){
                    root = null;
                }else{
                    if(pre.left == cur){
                        pre.left = null;
                    }else if(pre.right == cur){
                        pre.right = null;
                    }
                }
            }else if(cur.left != null && cur.right == null){
                if(pre == null){
                    root = cur.left;
                }else{
                    if(pre.left == cur){
                        pre.left = cur.left;
                    }else if(pre.right == cur){
                        pre.right = cur.right;
                    }
                }
            }else if(cur.left == null && cur.right != null){
                if(pre == null){
                    root = cur.right;
                }else{
                    if(pre.left == cur){
                        pre.left = cur.right;
                    }else if(pre.right == cur){
                        pre.right = cur.right;
                    }
                }
            }else{
                if(pre == null){
                    TreeNode l = cur.left;
                    TreeNode tmp = null;
                    cur = cur.right;
                    while(cur.left != null){
                        tmp = cur;
                        cur = cur.left;
                    }
                    root.val = cur.val;
                    if(tmp == null){
                        root.right = null;
                    }else{
                        tmp.left = null;
                    }
                }else{
                    TreeNode t1 = cur;
                    TreeNode t2 = null;
                    cur = cur.right;
                    while(cur.left != null){
                        t2 = cur;
                        cur = cur.left;
                    }
                    t1.val = cur.val;
                    if(t2 != null){
                        t2.left = null;
                    }else{
                        t1.right = null;
                    }
                }
            }
            return root;
        }else
            return root;
    }
}
