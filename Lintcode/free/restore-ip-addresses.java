/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    public ArrayList&lt;String&gt; restoreIpAddresses(String s) {
        ArrayList&lt;String&gt; result = new ArrayList&lt;String&gt;();
        ArrayList&lt;String&gt; list = new ArrayList&lt;String&gt;();
        
        if(s.length() &lt;4 || s.length() &gt; 12)
            return result;
        
        helper(result, list, s , 0);
        return result;
    }
    
    public void helper(ArrayList&lt;String&gt; result, ArrayList&lt;String&gt; list, String s, int start){
        if(list.size() == 4){
            if(start != s.length())
                return;
            
            StringBuffer sb = new StringBuffer();
            for(String tmp: list){
                sb.append(tmp);
                sb.append(&quot;.&quot;);
            }
            sb.deleteCharAt(sb.length()-1);
            result.add(sb.toString());
            return;
        }
        
        for(int i=start; i&lt;s.length() &amp;&amp; i &lt; start+3; i++){
            String tmp = s.substring(start, i+1);
            if(isvalid(tmp)){
                list.add(tmp);
                helper(result, list, s, i+1);
                list.remove(list.size()-1);
            }
        }
    }
    
    private boolean isvalid(String s){
        if(s.charAt(0) == &#39;0&#39;)
            return s.equals(&quot;0&quot;); // to eliminate cases like &quot;00&quot;, &quot;10&quot;
        int digit = Integer.valueOf(s);
        return digit &gt;= 0 &amp;&amp; digit &lt;= 255;
    }
}

