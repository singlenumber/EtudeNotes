/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    public String reverseVowels(String s) {
        int[] pos = new int[s.length()];
        int cnt = 0;
        HashSet&lt;Character&gt; vowel = new HashSet&lt;Character&gt;();
        vowel.add(&#39;a&#39;);
        vowel.add(&#39;e&#39;);
        vowel.add(&#39;i&#39;);
        vowel.add(&#39;o&#39;);
        vowel.add(&#39;u&#39;);
        vowel.add(&#39;A&#39;);
        vowel.add(&#39;E&#39;);
        vowel.add(&#39;I&#39;);
        vowel.add(&#39;O&#39;);
        vowel.add(&#39;U&#39;);
        
        for (int i = 0; i &lt; s.length(); i++) {
            if (vowel.contains(s.charAt(i))) {
                pos[cnt] = i;
                cnt++;
            }
        }
        
        char[] ans = new char[s.length()];
        ans = s.toCharArray();
        for (int i = 0; i &lt; cnt; i++) {
            ans[pos[i]] = s.charAt(pos[cnt - i - 1]);
        }
        return String.valueOf(ans);
    }
}