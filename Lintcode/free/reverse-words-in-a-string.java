/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    public String reverseWords(String s) {
        if (s == null || s.length() == 0) {
            return &quot;&quot;;
        }

        String[] array = s.split(&quot; &quot;);
        StringBuilder sb = new StringBuilder();

        for (int i = array.length - 1; i &gt;= 0; --i) {
            if (!array[i].equals(&quot;&quot;)) {
                sb.append(array[i]).append(&quot; &quot;);
            }
        }

        //remove the last &quot; &quot;
        return sb.length() == 0 ? &quot;&quot; : sb.substring(0, sb.length() - 1);
    }
}