/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

/**
 * Definition for Directed graph.
 * class DirectedGraphNode {
 *     int label;
 *     ArrayList&lt;DirectedGraphNode&gt; neighbors;
 *     DirectedGraphNode(int x) { label = x; neighbors = new ArrayList&lt;DirectedGraphNode&gt;(); }
 * };
 */
public class Solution {
   /**
     * @param graph: A list of Directed graph node
     * @param s: the starting Directed graph node
     * @param t: the terminal Directed graph node
     * @return: a boolean value
     */
    public boolean hasRoute(ArrayList&lt;DirectedGraphNode&gt; graph,
                                 DirectedGraphNode s, DirectedGraphNode t) {
        HashSet&lt;DirectedGraphNode&gt; visited = new HashSet&lt;DirectedGraphNode&gt;();
        Queue&lt;DirectedGraphNode&gt; queue = new LinkedList&lt;DirectedGraphNode&gt;();
        
        queue.offer(s);
        visited.add(s);
        while (!queue.isEmpty()) {
            DirectedGraphNode node = queue.poll();
            for (int i = 0; i &lt; node.neighbors.size(); i++) {
                if (visited.contains(node.neighbors.get(i))) {
                    continue;
                }
                visited.add(node.neighbors.get(i));
                queue.offer(node.neighbors.get(i));
                if (node.neighbors.get(i) == t) {
                    return true;
                }
            }
        }
        
        return visited.contains(t);
    }
}
