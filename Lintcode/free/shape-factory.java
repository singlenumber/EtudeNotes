/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

/**
 * Your object will be instantiated and called as such:
 * ShapeFactory sf = new ShapeFactory();
 * Shape shape = sf.getShape(shapeType);
 * shape.draw();
 */
interface Shape {
    void draw();
}

class Rectangle implements Shape {
    // Write your code here
    @Override
    public void draw() {
        System.out.println(&quot; ---- &quot;);
        System.out.println(&quot;|    |&quot;);
        System.out.println(&quot; ---- &quot;);
   }
}

class Square implements Shape {
    // Write your code here
   @Override
   public void draw() {
        System.out.println(&quot; ---- &quot;);
        System.out.println(&quot;|    |&quot;);
        System.out.println(&quot;|    |&quot;);
        System.out.println(&quot; ---- &quot;);
   }
}

class Triangle implements Shape {
    // Write your code here
   @Override
   public void draw() {
        System.out.println(&quot;  /\\&quot;);
        System.out.println(&quot; /  \\&quot;);
        System.out.println(&quot;/____\\&quot;);
   }
}

public class ShapeFactory {
    /**
     * @param shapeType a string
     * @return Get object of type Shape
     */
    public Shape getShape(String shapeType) {
        // Write your code here
        if (shapeType == null) {
            return null;
        }		
        if (shapeType.equalsIgnoreCase(&quot;Triangle&quot;)) {
            return new Triangle();
        } else if(shapeType.equalsIgnoreCase(&quot;Rectangle&quot;)) {
            return new Rectangle();         
        } else if(shapeType.equalsIgnoreCase(&quot;Square&quot;)) {
            return new Square();
        }
        return null;
    }
}