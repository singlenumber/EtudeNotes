/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    /**
     * @param A : An integer array
     * @return : Two integers
     */
    public List&lt;Integer&gt; singleNumberIII(int[] A) {
        int xor = 0;
        for (int i = 0; i &lt; A.length; i++) {
            xor ^= A[i];
        }
        
        int lastBit = xor - (xor &amp; (xor - 1));
        int group0 = 0, group1 = 0;
        for (int i = 0; i &lt; A.length; i++) {
            if ((lastBit &amp; A[i]) == 0) {
                group0 ^= A[i];
            } else {
                group1 ^= A[i];
            }
        }
        
        ArrayList&lt;Integer&gt; result = new ArrayList&lt;Integer&gt;();
        result.add(group0);
        result.add(group1);
        return result;
    }
}
