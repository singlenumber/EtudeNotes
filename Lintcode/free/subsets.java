/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    public ArrayList&lt;ArrayList&lt;Integer&gt;&gt; subsets(int[] num) {
        ArrayList&lt;ArrayList&lt;Integer&gt;&gt; result = new ArrayList&lt;ArrayList&lt;Integer&gt;&gt;();
        if(num == null || num.length == 0) {
            return result;
        }
        ArrayList&lt;Integer&gt; list = new ArrayList&lt;Integer&gt;();
        Arrays.sort(num);  
        subsetsHelper(result, list, num, 0);

        return result;
    }


    private void subsetsHelper(ArrayList&lt;ArrayList&lt;Integer&gt;&gt; result,
        ArrayList&lt;Integer&gt; list, int[] num, int pos) {

        result.add(new ArrayList&lt;Integer&gt;(list));

        for (int i = pos; i &lt; num.length; i++) {

            list.add(num[i]);
            subsetsHelper(result, list, num, i + 1);
            list.remove(list.size() - 1);
        }
    }
}


// Non Recursion
class Solution {
    /**
     * @param S: A set of numbers.
     * @return: A list of lists. All valid subsets.
     */
    public ArrayList&lt;ArrayList&lt;Integer&gt;&gt; subsets(int[] nums) {
        ArrayList&lt;ArrayList&lt;Integer&gt;&gt; result = new ArrayList&lt;ArrayList&lt;Integer&gt;&gt;();
        int n = nums.length;
        Arrays.sort(nums);
        
        // 1 &lt;&lt; n is 2^n
        // each subset equals to an binary integer between 0 .. 2^n - 1
        // 0 -&gt; 000 -&gt; []
        // 1 -&gt; 001 -&gt; [1]
        // 2 -&gt; 010 -&gt; [2]
        // ..
        // 7 -&gt; 111 -&gt; [1,2,3]
        for (int i = 0; i &lt; (1 &lt;&lt; n); i++) {
            ArrayList&lt;Integer&gt; subset = new ArrayList&lt;Integer&gt;();
            for (int j = 0; j &lt; n; j++) {
                // check whether the jth digit in i&#39;s binary representation is 1
                if ((i &amp; (1 &lt;&lt; j)) != 0) {
                    subset.add(nums[j]);
                }
            }
            result.add(subset);
        }
        
        return result;
    }
}
