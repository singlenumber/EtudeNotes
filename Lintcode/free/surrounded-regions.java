/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

version 1:
public class Solution {
    static final int[] directionX = {+1, -1, 0, 0};
    static final int[] directionY = {0, 0, +1, -1};
    
    static final char FREE = &#39;F&#39;;
    static final char TRAVELED = &#39;T&#39;;
    
    public void solve(char[][] board) {
        if (board.length == 0) {
            return;
        }
        
        int row = board.length;
        int col = board[0].length;
        
        for (int i = 0; i &lt; row; i++) {
            bfs(board, i, 0);
            bfs(board, i, col - 1);
        }
        
        for (int j = 1; j &lt; col - 1; j++) {
            bfs(board, 0, j);
            bfs(board, row - 1, j);
        }
        
        for (int i = 0; i &lt; row; i++) {
            for (int j = 0; j &lt; col; j++) {
                switch(board[i][j]) {
                    case &#39;O&#39;: 
                        board[i][j] = &#39;X&#39;;
                        break;
                    case &#39;F&#39;:
                        board[i][j] = &#39;O&#39;;
                }
            }
        }
    }
    
    public void bfs(char[][] board, int i, int j) {
        if (board[i][j] != &#39;O&#39;) {
            return;
        }
        
        Queue&lt;Node&gt; queue = new LinkedList&lt;Node&gt;();
        queue.offer(new Node(i, j));
        
        while (!queue.isEmpty()) {
            Node crt = queue.poll();
            board[crt.x][crt.y] = FREE;
            
            for (Node node : expand(board, crt)) {
                queue.offer(node);
            }
        }
    }
    
    private List&lt;Node&gt; expand(char[][] board, Node node) {
        List&lt;Node&gt; expansion = new ArrayList&lt;Node&gt;();
        
        for (int i = 0; i &lt; directionX.length; i++) {
            int x = node.x + directionX[i];
            int y = node.y + directionY[i];
            
            // check validity
            if (x &gt;= 0 &amp;&amp; x &lt; board.length &amp;&amp; y &gt;= 0 &amp;&amp; y &lt; board[0].length &amp;&amp; board[x][y] == &#39;O&#39;) {
                board[x][y] = TRAVELED;
                expansion.add(new Node(x, y));
            }
        }
        
        return expansion;
    }
    
    static class Node {
        int x;
        int y;
        
        Node(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
}


version 2:

public class Solution {
    private static Queue&lt;Integer&gt; queue = null;
    private static char[][] board;
    private static int rows = 0;
    private static int cols = 0;

    public void solve(char[][] board) {
        // Note: The Solution object is instantiated only once and is reused by each test case.
        if (board.length == 0 || board[0].length == 0) return;
        queue = new LinkedList&lt;Integer&gt;();
        board = board;
        rows = board.length;
        cols = board[0].length;

        for (int i = 0; i &lt; rows; i++) { // **important**
            enqueue(i, 0);
            enqueue(i, cols - 1);
        }

        for (int j = 1; j &lt; cols - 1; j++) { // **important**
            enqueue(0, j);
            enqueue(rows - 1, j);
        }

        while (!queue.isEmpty()) {
            int cur = queue.poll();
            int x = cur / cols,
                y = cur % cols;

            if (board[x][y] == &#39;O&#39;) {
                board[x][y] = &#39;D&#39;;
            }

            enqueue(x - 1, y);
            enqueue(x + 1, y);
            enqueue(x, y - 1);
            enqueue(x, y + 1);
        }

        for (int i = 0; i &lt; rows; i++) {
            for (int j = 0; j &lt; cols; j++) {
                if (board[i][j] == &#39;D&#39;) board[i][j] = &#39;O&#39;;
                else if (board[i][j] == &#39;O&#39;) board[i][j] = &#39;X&#39;;
            }
        }

        queue = null;
        board = null;
        rows = 0;
        cols = 0;
    }

    public static void enqueue(int x, int y) {
        if (x &gt;= 0 &amp;&amp; x &lt; rows &amp;&amp; y &gt;= 0 &amp;&amp; y &lt; cols &amp;&amp; board[x][y] == &#39;O&#39;){  
            queue.offer(x * cols + y);
        }
    }
}