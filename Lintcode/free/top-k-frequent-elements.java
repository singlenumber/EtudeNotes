/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    public List&lt;Integer&gt; topKFrequent(int[] nums, int k) {
        Map&lt;Integer, Integer&gt; hashmap = new HashMap&lt;Integer, Integer&gt;();
        PriorityQueue&lt;Map.Entry&lt;Integer, Integer&gt;&gt; queue = new PriorityQueue&lt;Map.Entry&lt;Integer, Integer&gt;&gt;(
            new Comparator&lt;Map.Entry&lt;Integer, Integer&gt;&gt;() {
                public int compare(Map.Entry&lt;Integer, Integer&gt; e1, Map.Entry&lt;Integer, Integer&gt; e2) {
                    return e1.getValue() - e2.getValue();
                }
            });
        for (int i = 0; i &lt; nums.length; i++) {
            if (!hashmap.containsKey(nums[i])) {
                hashmap.put(nums[i], 1);
            } else {
                hashmap.put(nums[i], hashmap.get(nums[i]) + 1);
            }
        }
        
        for (Map.Entry&lt;Integer, Integer&gt; entry : hashmap.entrySet()) {
            if (queue.size() &lt; k) {
                queue.offer(entry);
            } else if (queue.peek().getValue() &lt; entry.getValue()) {
                queue.poll();
                queue.offer(entry);
            }
        }
        
        List&lt;Integer&gt; ans = new ArrayList&lt;Integer&gt;();
        for (Map.Entry&lt;Integer, Integer&gt; entry : queue)
            ans.add(entry.getKey());
        return ans;
    }
}