/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

import java.util.NavigableSet;

public class TopK {
    private Map&lt;String, Integer&gt; words = null;
    private NavigableSet&lt;String&gt; topk = null;
    private int k;

    private Comparator&lt;String&gt; myComparator = new Comparator&lt;String&gt;() {
        public int compare(String left, String right) {
            if (left.equals(right))
                return 0;

            int left_count = words.get(left);
            int right_count = words.get(right);
            if (left_count != right_count) {
                return right_count - left_count;
            }
            return left.compareTo(right);
        }
    };

    public TopK(int k) {
        // initialize your data structure here
        this.k = k;
        words = new HashMap&lt;String, Integer&gt;();
        topk = new TreeSet&lt;String&gt;(myComparator);
    }

    public void add(String word) {
        // Write your code here
        if (words.containsKey(word)) {
            if (topk.contains(word))
                topk.remove(word);
            words.put(word, words.get(word) + 1);
        } else {
            words.put(word, 1);
        }

        topk.add(word);
        if (topk.size() &gt; k) {
            topk.pollLast();
        }
    }

    public List&lt;String&gt; topk() {
        // Write your code here
        List&lt;String&gt; results = new ArrayList&lt;String&gt;();
        Iterator it = topk.iterator();
        while(it.hasNext()) {
             String str = (String)it.next();
             results.add(str);
        }
        Collections.sort(results, myComparator);
        return results;
    }
}
