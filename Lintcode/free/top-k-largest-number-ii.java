/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    private int maxSize;
    private Queue&lt;Integer&gt; minheap;
    public Solution(int k) {
        minheap = new PriorityQueue&lt;&gt;();
        maxSize = k;
    }

    public void add(int num) {
        if (minheap.size() &lt; maxSize) {
            minheap.offer(num);
            return;
        }
        
        if (num &gt; minheap.peek()) {
            minheap.poll();
            minheap.offer(num);
        }
    }

    public List&lt;Integer&gt; topk() {
        Iterator it = minheap.iterator();
        List&lt;Integer&gt; result = new ArrayList&lt;Integer&gt;();
        while (it.hasNext()) {
            result.add((Integer) it.next());
        }
        Collections.sort(result, Collections.reverseOrder());
        return result;
    }
};