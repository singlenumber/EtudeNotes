/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

// version 1:
public class Solution {
    private void search(int index, String s, List&lt;Integer&gt; path,
                   boolean[][] isWord, boolean[] possible,
                   List&lt;String&gt; result) {
        if (!possible[index]) {
            return;
        }
        
        if (index == s.length()) {
            StringBuilder sb = new StringBuilder();
            int lastIndex = 0;
            for (int i = 0; i &lt; path.size(); i++) {
                sb.append(s.substring(lastIndex, path.get(i)));
                if (i != path.size() - 1) sb.append(&quot; &quot;);
                lastIndex = path.get(i);
            }
            result.add(sb.toString());
            return;
        }
        
        for (int i = index; i &lt; s.length(); i++) {
            if (!isWord[index][i]) {
                continue;
            }
            path.add(i + 1);
            search(i + 1, s, path, isWord, possible, result);
            path.remove(path.size() - 1);
        }
    }
    
    public List&lt;String&gt; wordBreak(String s, Set&lt;String&gt; wordDict) {
        ArrayList&lt;String&gt; result = new ArrayList&lt;String&gt;();
        if (s.length() == 0) {
            return result;
        }
        
        boolean[][] isWord = new boolean[s.length()][s.length()];
        for (int i = 0; i &lt; s.length(); i++) {
            for (int j = i; j &lt; s.length(); j++) {
                String word = s.substring(i, j + 1);
                isWord[i][j] = wordDict.contains(word);
            }
        }
        
        boolean[] possible = new boolean[s.length() + 1];
        possible[s.length()] = true;
        for (int i = s.length() - 1; i &gt;= 0; i--) {
            for (int j = i; j &lt; s.length(); j++) {
                if (isWord[i][j] &amp;&amp; possible[j + 1]) {
                    possible[i] = true;
                    break;
                }
            }
        }
        
        List&lt;Integer&gt; path = new ArrayList&lt;Integer&gt;();
        search(0, s, path, isWord, possible, result);
        return result;
    }
}

// version 2:

public class Solution {
    public ArrayList&lt;String&gt; wordBreak(String s, Set&lt;String&gt; dict) {
        // Note: The Solution object is instantiated only once and is reused by each test case.
        Map&lt;String, ArrayList&lt;String&gt;&gt; map = new HashMap&lt;String, ArrayList&lt;String&gt;&gt;();
        return wordBreakHelper(s,dict,map);
    }

    public ArrayList&lt;String&gt; wordBreakHelper(String s, Set&lt;String&gt; dict, Map&lt;String, ArrayList&lt;String&gt;&gt; memo){
        if(memo.containsKey(s)) return memo.get(s);
        ArrayList&lt;String&gt; result = new ArrayList&lt;String&gt;();
        int n = s.length();
        if(n &lt;= 0) return result;
        for(int len = 1; len &lt;= n; ++len){
            String subfix = s.substring(0,len);
            if(dict.contains(subfix)){
                if(len == n){
                    result.add(subfix);
                }else{
                    String prefix = s.substring(len);
                    ArrayList&lt;String&gt; tmp = wordBreakHelper(prefix, dict, memo);
                    for(String item:tmp){
                        item = subfix + &quot; &quot; + item;
                        result.add(item);
                    }
                }
            }
        }
        memo.put(s, result);
        return result;
    }
}