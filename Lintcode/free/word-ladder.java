/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    public int ladderLength(String start, String end, Set&lt;String&gt; dict) {
        if (dict == null) {
            return 0;
        }

        if (start.equals(end)) {
            return 1;
        }
        
        dict.add(start);
        dict.add(end);

        HashSet&lt;String&gt; hash = new HashSet&lt;String&gt;();
        Queue&lt;String&gt; queue = new LinkedList&lt;String&gt;();
        queue.offer(start);
        hash.add(start);
        
        int length = 1;
        while(!queue.isEmpty()) {
            length++;
            int size = queue.size();
            for (int i = 0; i &lt; size; i++) {
                String word = queue.poll();
                for (String nextWord: getNextWords(word, dict)) {
                    if (hash.contains(nextWord)) {
                        continue;
                    }
                    if (nextWord.equals(end)) {
                        return length;
                    }
                    
                    hash.add(nextWord);
                    queue.offer(nextWord);
                }
            }
        }
        return 0;
    }

    // replace character of a string at given index to a given character
    // return a new string
    private String replace(String s, int index, char c) {
        char[] chars = s.toCharArray();
        chars[index] = c;
        return new String(chars);
    }
    
    // get connections with given word.
    // for example, given word = &#39;hot&#39;, dict = {&#39;hot&#39;, &#39;hit&#39;, &#39;hog&#39;}
    // it will return [&#39;hit&#39;, &#39;hog&#39;]
    private ArrayList&lt;String&gt; getNextWords(String word, Set&lt;String&gt; dict) {
        ArrayList&lt;String&gt; nextWords = new ArrayList&lt;String&gt;();
        for (char c = &#39;a&#39;; c &lt;= &#39;z&#39;; c++) {
            for (int i = 0; i &lt; word.length(); i++) {
                if (c == word.charAt(i)) {
                    continue;
                }
                String nextWord = replace(word, i, c);
                if (dict.contains(nextWord)) {
                    nextWords.add(nextWord);
                }
            }
        }
        return nextWords;
    }
}