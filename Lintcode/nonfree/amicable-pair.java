/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    /**
     * @param k an integer
     * @return all amicable pairs
     */
    public int factorSum(int n) {
        int sum = 1, i;
        for (i = 2; i * i &lt; n; ++i) {
            if (n % i == 0) {
                sum += i + n / i;
            }
        }
        if (i * i == n) {
            sum += i;
        }
        return sum;
    }
    
    public List&lt;List&lt;Integer&gt;&gt; amicablePair(int k) {
        List&lt;List&lt;Integer&gt;&gt; result = new ArrayList&lt;List&lt;Integer&gt;&gt;();
        for (int i = 2; i &lt;= k; ++i) {
            int amicable = factorSum(i);
            if (amicable &lt;= i || amicable &gt; k) {
                continue;
            }
            if (factorSum(amicable) == i) {
                ArrayList&lt;Integer&gt; pair = new ArrayList&lt;Integer&gt;();
                pair.add(i);
                pair.add(amicable);
                result.add(pair);
            }
        }
        return result;
    }
}
