/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class AnimalShelter {

    public AnimalShelter() {
        // do initialize if necessary
        tot = 0;
        dogs = new LinkedList&lt;String&gt;();
        cats = new LinkedList&lt;String&gt;();
    }

    /**
     * @param name a string
     * @param type an integer, 1 if Animal is dog or 0
     * @return void
     */
    void enqueue(String name, int type) {
        // write your code here
        tot += 1;
        if (type == 1)
            dogs.add(tot + &quot;#&quot; + name);
        else
            cats.add(tot + &quot;#&quot; + name);
    }

    public String dequeueAny() {
        // write your code here
        if (cats.isEmpty())
            return dequeueDog();
        else if (dogs.isEmpty())
            return dequeueCat();
        else {
            int d_time = getTime(dogs.getFirst());
            int c_time = getTime(cats.getFirst());
            if (c_time &lt; d_time)
                return dequeueCat();
            else
                return dequeueDog();
        }
    }

    public String dequeueDog() {
        // write your code here
        String name = getName(dogs.getFirst());
        dogs.removeFirst();
        return name;
    }

    public String dequeueCat() {
        // write your code here
        String name = getName(cats.getFirst());
        cats.removeFirst();
        return name;
    }

    private int tot;
    private LinkedList&lt;String&gt; cats, dogs;
    private String getName(String str) {
        return str.substring(str.indexOf(&quot;#&quot;) + 1, str.length());
    }
    private int getTime(String str) {
        return Integer.parseInt(str.substring(0, str.indexOf(&quot;#&quot;)));
    }
}
