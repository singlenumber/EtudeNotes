/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class ArrayListManager {
    /**
     * @param n: You should generate an array list of n elements.
     * @return: The array list your just created.
     */
    public static ArrayList&lt;Integer&gt; create(int n) {
        // Write your code here
        ArrayList&lt;Integer&gt; list = new ArrayList&lt;Integer&gt;();
        for (int i = 0; i &lt; n; i++) {
            list.add(i);
        }
        return list;
    }
    
    /**
     * @param list: The list you need to clone
     * @return: A deep copyed array list from the given list
     */
    public static ArrayList&lt;Integer&gt; clone(ArrayList&lt;Integer&gt; list) {
        // Write your code here
        ArrayList&lt;Integer&gt; dist = new ArrayList&lt;Integer&gt;();
        for (Integer a : list)
            dist.add(a);
        return dist;
        //return list;
    }
    
    /**
     * @param list: The array list to find the kth element
     * @param k: Find the kth element
     * @return: The kth element
     */
    public static int get(ArrayList&lt;Integer&gt; list, int k) {
        // Write your code here
        return list.get(k);
    }
    
    /**
     * @param list: The array list
     * @param k: Find the kth element, set it to val
     * @param val: Find the kth element, set it to val
     */
    public static void set(ArrayList&lt;Integer&gt; list, int k, int val) {
        // write your code here
        list.set(k, val);
    }
    
    /**
     * @param list: The array list to remove the kth element
     * @param k: Remove the kth element
     */
    public static void remove(ArrayList&lt;Integer&gt; list, int k) {
        // write tour code here
        list.remove(k);
    }
    
    /**
     * @param list: The array list.
     * @param val: Get the index of the first element that equals to val
     * @return: Return the index of that element
     */
    public static int indexOf(ArrayList&lt;Integer&gt; list, int val) {
        // Write your code here
        if (list == null)
            return -1;
        return list.indexOf(val);
    }
}
