/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

// 方法一
public class Solution {
    /**
     * @param nums an integer array and all positive numbers, no duplicates
     * @param target an integer
     * @return an integer
     */
    public int backPackIV(int[] nums, int target) {
        // Write your code here
        int m = target;
        int []A = nums;
        int f[][] = new int[A.length + 1][m + 1];
        
        f[0][0] = 1;
        for (int i = 0; i &lt; A.length; i++) {
            for (int j = 0; j &lt;= m; j++) {
                int k = 0; 
                while(k * A[i] &lt;= j) {
                    f[i + 1][j] += f[i][j-A[i]*k];
                    k+=1;
                }
            } // for j
        } // for i
        
        
        return f[A.length][target];
    }
}

// 方法二

public class Solution {
    /**
     * @param nums an integer array and all positive numbers, no duplicates
     * @param target an integer
     * @return an integer
     */
    public int backPackIV(int[] nums, int target) {
        // Write your code here
        int[] f = new int[target + 1];
        f[0] = 1;
        for (int i = 0; i &lt; nums.length; ++i)
            for (int  j = nums[i]; j &lt;= target; ++j)
                f[j] += f[j - nums[i]];

        return f[target];
    }
}
