/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

/**
 * Definition of TreeNode:
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left, right;
 *     public TreeNode(int val) {
 *         this.val = val;
 *         this.left = this.right = null;
 *     }
 * }
 */
public class Solution {
    /**
     * @param root the root of the binary tree
     * @param level the depth of the target level
     * @return an integer
     */
    private int sum = 0;
    public int levelSum(TreeNode root, int level) {
        helper(root, 1, level);
        return sum;
    }
    
    private void helper(TreeNode root, int depth, int level) {
        if (root == null) {
            return;
        }
        
        if (depth == level) {
            sum += root.val;
            return;
        }
        
        helper(root.left, depth + 1, level);
        helper(root.right, depth + 1, level);
    }
}
