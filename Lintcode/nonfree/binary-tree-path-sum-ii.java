/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

/**
 * Definition of TreeNode:
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left, right;
 *     public TreeNode(int val) {
 *         this.val = val;
 *         this.left = this.right = null;
 *     }
 * }
 */
public class Solution {
    /**
     * @param root the root of binary tree
     * @param target an integer
     * @return all valid paths
     */
    public List&lt;List&lt;Integer&gt;&gt; binaryTreePathSum2(TreeNode root, int target) {
        // Write your code here
        List&lt;List&lt;Integer&gt;&gt; results = new ArrayList&lt;List&lt;Integer&gt;&gt;();
        ArrayList&lt;Integer&gt; buffer = new ArrayList&lt;Integer&gt;();
        if (root == null)
            return results;
        findSum(root, target, buffer, 0, results);
        return results;
    }

    public void findSum(TreeNode head, int sum, ArrayList&lt;Integer&gt; buffer, int level, List&lt;List&lt;Integer&gt;&gt; results) {
        if (head == null) return;
        int tmp = sum;
        buffer.add(head.val);
        for (int i = level;i &gt;= 0; i--) {
            tmp -= buffer.get(i);
            if (tmp == 0) {
                List&lt;Integer&gt; temp = new ArrayList&lt;Integer&gt;();
                for (int j = i; j &lt;= level; ++j)
                    temp.add(buffer.get(j));
                results.add(temp);
            }
        }
        findSum(head.left, sum, buffer, level + 1, results);
        findSum(head.right, sum, buffer, level + 1, results);
        buffer.remove(buffer.size() - 1);
    }
}