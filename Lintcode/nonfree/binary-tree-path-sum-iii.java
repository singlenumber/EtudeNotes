/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

/**
 * Definition of ParentTreeNode:
 * 
 * class ParentTreeNode {
 *     public int val;
 *     public ParentTreeNode parent, left, right;
 * }
 */
public class Solution {
    /**
     * @param root the root of binary tree
     * @param target an integer
     * @return all valid paths
     */
    public List&lt;List&lt;Integer&gt;&gt; binaryTreePathSum3(ParentTreeNode root, int target) {
        // Write your code here
        List&lt;List&lt;Integer&gt;&gt; results = new ArrayList&lt;List&lt;Integer&gt;&gt;();
        dfs(root, target, results);
        return results;
    }
    
    public void dfs(ParentTreeNode root, int target, List&lt;List&lt;Integer&gt;&gt; results) {
        if (root == null)
            return;

        List&lt;Integer&gt; path = new ArrayList&lt;Integer&gt;();
        findSum(root, null, target, path, results);

        dfs(root.left, target, results);
        dfs(root.right, target, results);
    }

    public void findSum(ParentTreeNode root, ParentTreeNode father, int target,
                 List&lt;Integer&gt; path, List&lt;List&lt;Integer&gt;&gt; results) {
        path.add(root.val);
        target -= root.val;
        
        if (target == 0) {
            ArrayList&lt;Integer&gt; tmp = new ArrayList&lt;Integer&gt;();
            Collections.addAll(tmp,  new  Integer[path.size()]); 
            Collections.copy(tmp, path); 
            results.add(tmp);
        }

        if (root.parent != null &amp;&amp; root.parent != father)
            findSum(root.parent, root, target, path, results);

        if (root.left != null &amp;&amp; root.left  != father)
            findSum(root.left, root, target, path, results);

        if (root.right != null &amp;&amp; root.right != father)
            findSum(root.right, root, target, path, results);

        path.remove(path.size() - 1);
    }
}