/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    /**
     * @param A an integer array
     * @return  A list of integers includes the index of the first number and the index of the last number
     */
    public ArrayList&lt;Integer&gt; continuousSubarraySumII(int[] A) {
        // Write your code here
        ArrayList&lt;Integer&gt; result = new ArrayList&lt;Integer&gt;();
        result.add(0);
        result.add(0);
        int total = 0;
        int len = A.length;
        int start = 0, end = 0;
        int local = 0;
        int global = -0x7fffffff;
        for (int i = 0; i &lt; len; ++i) {
            total += A[i];
            if (local &lt; 0) {
                local = A[i];
                start = end = i;
            } else {
                local += A[i];
                end = i;
            }
            if (local &gt;= global) {
                global = local;
                result.set(0, start);
                result.set(1, end);
            }
        }
        local = 0;
        start = 0;
        end = -1;
        for (int i = 0; i &lt; len; ++i) {
            if (local &gt; 0) {
                local = A[i];
                start = end = i;
            } else {
                local += A[i];
                end = i;
            }
            if (start == 0 &amp;&amp; end == len-1) continue;
            if (total - local &gt;= global) {
                global = total - local;
                result.set(0, (end + 1) % len);
                result.set(1, (start - 1 + len) % len);
            }
        }
        return result;
    }
}