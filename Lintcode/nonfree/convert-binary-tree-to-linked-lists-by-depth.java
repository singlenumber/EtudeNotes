/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

/**
 * Definition of TreeNode:
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left, right;
 *     public TreeNode(int val) {
 *         this.val = val;
 *         this.left = this.right = null;
 *     }
 * }
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
public class Solution {
    /**
     * @param root the root of binary tree
     * @return a lists of linked list
     */
    public List&lt;ListNode&gt; binaryTreeToLists(TreeNode root) {
        // Write your code here
        List&lt;ListNode&gt; result = new ArrayList&lt;ListNode&gt;();
        dfs(root, 1, result);
        return result;
    }

    void dfs(TreeNode root, int depth, List&lt;ListNode&gt; result) {
        if (root == null)
            return;
        ListNode node = new ListNode(root.val);
        if (result.size() &lt; depth) {
            result.add(node);
        }
        else {
            node.next = result.get(depth-1);
            result.set(depth-1, node);
        }
        dfs(root.right, depth + 1, result);
        dfs(root.left, depth + 1, result);
    }
}