/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

 
class TreeNode {
	public int val;
	public String s;
	public TreeNode left, right;

	public TreeNode(int val, String ss) {
		this.val = val;
		this.s = ss;
		this.left = this.right = null;
	}

}


public class Solution {
    /**
     * @param expression: A string array
     * @return: The Reverse Polish notation of this expression
     */

    int get(String a, Integer base) {
		if (a.equals(&quot;+&quot;) || a.equals(&quot;-&quot;))
			return 1 + base;
		if (a.equals(&quot;*&quot;) || a.equals(&quot;/&quot;))
			return 2 + base;

		return Integer.MAX_VALUE;
	}

	void dfs(TreeNode root, ArrayList&lt;String&gt; as) {
		if(root==null)
			return;
		as.add(root.s);
		if (root.left != null)
			dfs(root.left, as);
		
		if (root.right != null)
			dfs(root.right, as);
		
	}

	public ArrayList&lt;String&gt; convertToPN(String[] expression) {
		// write your code here
		Stack&lt;TreeNode&gt; stack = new Stack&lt;TreeNode&gt;();
		TreeNode root = null;
		int val = 0;
		Integer base = 0;
		for (int i = 0; i &lt;= expression.length; i++) {
			if(i != expression.length)
			{
				
				if (expression[i].equals(&quot;(&quot;)) {
					base += 10;
					continue;
				}
				if (expression[i].equals(&quot;)&quot;)) {
					base -= 10;
					continue;
				}
				val = get(expression[i], base);

			}
			TreeNode right = i == expression.length ? new TreeNode(
					Integer.MIN_VALUE, &quot;&quot;) : new TreeNode(val,
					expression[i]);
			while (!stack.isEmpty()) {
				if (right.val &lt;= stack.peek().val) {
					TreeNode nodeNow = stack.pop();

					if (stack.isEmpty()) {
						right.left = nodeNow;

					} else {
						TreeNode left = stack.peek();
						if (left.val &lt; right.val) {
							right.left = nodeNow;
						} else {
							left.right = nodeNow;
						}
					}
				} else {
					break;
				}
			}
			stack.push(right);
		}

		ArrayList&lt;String&gt; reversepolish = new ArrayList&lt;String&gt;();
		dfs(stack.peek().left, reversepolish);
		
		
		return reversepolish;
	}

}