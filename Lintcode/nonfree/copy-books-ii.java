/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    /**
     * @param n: an integer
     * @param times: an array of integers
     * @return: an integer
     */
    public int copyBooksII(int n, int[] times) {
        int k = times.length;
        int[][] f = new int[2][n+1];
        for (int j = 0 ; j &lt;= n; ++j) {
            f[0][j] = j * times[0];
        }
        for (int i = 1; i &lt; k; ++i) {
            for (int j = 1; j &lt;= n; ++j) {
                int a = i%2;
                int b = 1-a;
                
                f[a][j] = Integer.MAX_VALUE;
                for (int l = 0; l &lt;= j; ++l) {
                    if (f[b][j-l] &gt; times[i] * l) {
                        f[a][j] = Math.min(f[a][j], f[b][j-l]);
                    } else {
                        f[a][j] = Math.min(f[a][j], times[i] * l);
                        break;
                    }
                }
                
            }
        }
        return f[(k-1)%2][n];
    }
    public int copyBooksII2D(int n, int[] times) {
        int k = times.length;
        int[][] f = new int[k][n+1];
        for (int j = 0 ; j &lt;= n; ++j) {
            f[0][j] = j * times[0];
        }
        for (int i = 1; i &lt; k; ++i) {
            for (int j = 1; j &lt;= n; ++j) {
                f[i][j] = Integer.MAX_VALUE;
                for (int l = 0; l &lt;= j; ++l) {
                    f[i][j] = Math.min(f[i][j], Math.max(f[i-1][j-l], times[i] * l));
                    if (f[i-1][j-l] &lt;= times[i] * l) {
                        break;
                    }
                }
                
            }
        }
        return f[k-1][n];
    }
}
