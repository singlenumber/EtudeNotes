/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

class Solution {
    /**
     * @param A: An integer matrix
     * @return: The index of the peak
     */
    public List&lt;Integer&gt; findPeakII(int[][] A) {
        // this is the nlog(n) method
        int low = 1, high = A.length-2;
        List&lt;Integer&gt; ans = new ArrayList&lt;Integer&gt;();
        while(low &lt;= high) {
            int mid = (low + high) / 2;
            int col = find(mid, A);
            if(A[mid][col] &lt; A[mid - 1][col]) {
                high = mid - 1;
            } else if(A[mid][col] &lt; A[mid + 1][col]) {
                low = mid + 1;
            } else {
                ans.add(mid);
                ans.add(col);
                break;
            }
        }
        return ans;
    }
    int find(int row, int [][]A) {
        int col = 0;
        for(int i = 0; i &lt; A[row].length; i++) {
            if(A[row][i] &gt; A[row][col]) {
                col = i;
            }
        }
        return col;
    } 
}
