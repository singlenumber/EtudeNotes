/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

// bfs 方法
/**
 * Definition for Undirected graph.
 * class UndirectedGraphNode {
 *     int label;
 *     ArrayList&lt;UndirectedGraphNode&gt; neighbors;
 *     UndirectedGraphNode(int x) { label = x; neighbors = new ArrayList&lt;UndirectedGraphNode&gt;(); }
 * };
 */
public class Solution {
    /**
     * @param nodes a array of Undirected graph node
     * @return a connected set of a Undirected graph
     */
    public List&lt;List&lt;Integer&gt;&gt; connectedSet(ArrayList&lt;UndirectedGraphNode&gt; nodes) {
        // Write your code here
        
        int m = nodes.size();
        Map&lt;UndirectedGraphNode, Boolean&gt; visited = new HashMap&lt;&gt;();
        
       for (UndirectedGraphNode node : nodes){
            visited.put(node, false);
       }
        
        List&lt;List&lt;Integer&gt;&gt; result = new ArrayList&lt;&gt;();
        
        for (UndirectedGraphNode node : nodes){
            if (visited.get(node) == false){
                bfs(node, visited, result);
            }
        }
        
        return result;
    }
    
    public void bfs(UndirectedGraphNode node, Map&lt;UndirectedGraphNode, Boolean&gt; visited, List&lt;List&lt;Integer&gt;&gt; result){
        List&lt;Integer&gt;row = new ArrayList&lt;&gt;();
        Queue&lt;UndirectedGraphNode&gt; queue = new LinkedList&lt;&gt;();
        visited.put(node, true);
        queue.offer(node);
        while (!queue.isEmpty()){
            UndirectedGraphNode u = queue.poll();
            row.add(u.label);    
            for (UndirectedGraphNode v : u.neighbors){
                if (visited.get(v) == false){
                    visited.put(v, true);
                    queue.offer(v);
                }
            }
        }
        Collections.sort(row);
        result.add(row);
        
    }
}

//------------------------我是分割线--------------------------------------
// 并查集方法
public
class Solution {
    class UnionFind {
        HashMap&lt;Integer, Integer&gt; father = new HashMap&lt;Integer, Integer&gt;();
        UnionFind(HashSet&lt;Integer&gt; hashSet)
        {
            for (Integer now : hashSet) {
                father.put(now, now);
            }
        }
        int find(int x)
        {
            int parent = father.get(x);
            while (parent != father.get(parent)) {
                parent = father.get(parent);
            }
            return parent;
        }
        int compressed_find(int x)
        {
            int parent = father.get(x);
            while (parent != father.get(parent)) {
                parent = father.get(parent);
            }
            int next;
            while (x != father.get(x)) {
                next = father.get(x);
                father.put(x, parent);
                x = next;
            }
            return parent;
        }

        void union(int x, int y)
        {
            int fa_x = find(x);
            int fa_y = find(y);
            if (fa_x != fa_y)
                father.put(fa_x, fa_y);
        }
    }
    
    List&lt;List&lt;Integer&gt; &gt; print(HashSet&lt;Integer&gt; hashSet, UnionFind uf, int n) {
        List&lt;List&lt;Integer&gt; &gt; ans = new ArrayList&lt;List&lt;Integer&gt; &gt;();
        HashMap&lt;Integer, List&lt;Integer&gt; &gt; hashMap = new HashMap&lt;Integer, List&lt;Integer&gt; &gt;();
        for (int i : hashSet) {
            int fa = uf.find(i);
            if (!hashMap.containsKey(fa)) {
                hashMap.put(fa, new ArrayList&lt;Integer&gt;());
            }
            List&lt;Integer&gt; now = hashMap.get(fa);
            now.add(i);
            hashMap.put(fa, now);
        }
        for (List&lt;Integer&gt; now : hashMap.values()) {
            Collections.sort(now);
            ans.add(now);
        }
        return ans;
    }

public
    List&lt;List&lt;Integer&gt; &gt; connectedSet(ArrayList&lt;UndirectedGraphNode&gt; nodes)
    {
        // Write your code here

        HashSet&lt;Integer&gt; hashSet = new HashSet&lt;Integer&gt;();
        for (UndirectedGraphNode now : nodes) {
            hashSet.add(now.label);
            for (UndirectedGraphNode neighbour : now.neighbors) {
                hashSet.add(neighbour.label);
            }
        }
        UnionFind uf = new UnionFind(hashSet);

        for (UndirectedGraphNode now : nodes) {

            for (UndirectedGraphNode neighbour : now.neighbors) {
                int fnow = uf.find(now.label);
                int fneighbour = uf.find(neighbour.label);
                if (fnow != fneighbour) {
                    uf.union(now.label, neighbour.label);
                }
            }
        }

        return print(hashSet, uf, nodes.size());
    }
}