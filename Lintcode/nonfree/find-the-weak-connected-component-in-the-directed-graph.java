/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */


public class Solution {
  class UnionFind{
    HashMap&lt;Integer, Integer&gt; father = new HashMap&lt;Integer, Integer&gt;();
    UnionFind(HashSet&lt;Integer&gt; hashSet){
      for(Integer now : hashSet) {
        father.put(now, now);
      }
    }
  int find(int x){
    int parent =  father.get(x);
    while(parent!=father.get(parent)) {
      parent = father.get(parent);
    }
    return parent;
  }
  int compressed_find(int x){
    int parent =  father.get(x);
    while(parent!=father.get(parent)) {
      parent = father.get(parent);
    }
    int temp = -1;
    int fa = father.get(x);
    while(fa!=father.get(fa)) {
      temp = father.get(fa);
      father.put(fa, parent) ;
      fa = temp;
    }
    return parent;
      
  }
  
  void union(int x, int y){
    int fa_x = find(x);
    int fa_y = find(y);
    if(fa_x != fa_y)
      father.put(fa_x, fa_y);
  }
  }
  List&lt;List&lt;Integer&gt; &gt;  print(HashSet&lt;Integer&gt; hashSet, UnionFind uf, int n) {
    List&lt;List &lt;Integer&gt; &gt; ans = new ArrayList&lt;List&lt;Integer&gt;&gt;();
  HashMap&lt;Integer, List &lt;Integer&gt;&gt; hashMap = new HashMap&lt;Integer, List &lt;Integer&gt;&gt;();
  for(int i : hashSet){
    int fa = uf.find(i);
    if(!hashMap.containsKey(fa)) {
      hashMap.put(fa,  new ArrayList&lt;Integer&gt;() );
    }
    List &lt;Integer&gt; now =  hashMap.get(fa);
    now.add(i);
    hashMap.put(fa, now);
  }
  for( List &lt;Integer&gt; now: hashMap.values()) {
  Collections.sort(now);
    ans.add(now);
  }
    return ans;
  }
  
  public List&lt;List&lt;Integer&gt;&gt; connectedSet2(ArrayList&lt;DirectedGraphNode&gt; nodes){
  // Write your code here
  
    HashSet&lt;Integer&gt; hashSet = new HashSet&lt;Integer&gt;(); 
    for(DirectedGraphNode now : nodes){
      hashSet.add(now.label);
      for(DirectedGraphNode neighbour : now.neighbors) {
        hashSet.add(neighbour.label);
      }
    }
    UnionFind uf = new UnionFind(hashSet);

  
    for(DirectedGraphNode now : nodes){
      
      for(DirectedGraphNode neighbour : now.neighbors) {
        int fnow = uf.find(now.label);
      int fneighbour = uf.find(neighbour.label);
      if(fnow!=fneighbour) {
        uf.union(now.label, neighbour.label);
      }
      }
    }
  
  
  return print(hashSet , uf, nodes.size());
  }

}
