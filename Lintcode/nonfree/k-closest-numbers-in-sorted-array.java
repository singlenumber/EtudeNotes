/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    /**
     * @param A an integer array
     * @param target an integer
     * @param k a non-negative integer
     * @return an integer array
     */
    public int[] kClosestNumbers(int[] A, int target, int k) {
        int[] result = new int[k];
        
        if (A == null || A.length == 0) {
            return A;
        }
        if (k &gt; A.length) {
            return A;
        }
        
        int index = firstIndex(A, target);
        
        int start = index - 1;
        int end = index;
        for (int i = 0; i &lt; k; i++) {
            if (start &lt; 0) {
                result[i] = A[end++];
            } else if (end &gt;= A.length) {
                result[i] = A[start--];
            } else {
                if (target - A[start] &lt;= A[end] - target) {
                    result[i] = A[start--];
                } else {
                    result[i] = A[end++];
                }
            }
        }
        return result;
    }
    
    private int firstIndex(int[] A, int target) {
        int start = 0, end = A.length - 1;
        while (start + 1 &lt; end) {
            int mid = start + (end - start) / 2;
            if (A[mid] &lt; target) {
                start = mid;
            } else if (A[mid] &gt; target) {
                end = mid;
            } else {
                end = mid;
            }
        }
        
        if (A[start] &gt;= target) {
            return start;
        }
        if (A[end] &gt;= target) {
            return end;
        }
        return A.length;
    }
}
