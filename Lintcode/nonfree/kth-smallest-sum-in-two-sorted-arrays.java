/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

class Pair implements Comparable&lt;Pair&gt; {
    int x;
    int y;
    int sum;
    Pair(int x, int y, int sum) {
        this.x = x;
        this.y = y;
        this.sum = sum;
    }
    @Override
    public int compareTo(Pair another) {
        if (this.sum == another.sum) {
            return 0;
        }
        return this.sum &lt; another.sum ? -1 : 1;
    }
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        } else if (!(obj instanceof Pair)) {
            return false;
        }
        Pair another = (Pair) obj;
        return this.x == another.x &amp;&amp; this.y == another.y;
    }
    @Override
    public int hashCode() {
        return x * 101 + y;
    }
}
public class Solution {
    /**
     * @param A an integer arrays sorted in ascending order
     * @param B an integer arrays sorted in ascending order
     * @param k an integer
     * @return an integer
     */
    int[] dx = {1, 0};
    int[] dy = {0, 1};
    public int kthSmallestSum(int[] A, int[] B, int k) {
        if (A.length == 0 &amp;&amp; B.length == 0) {
            return 0; 
        } else if (A.length == 0) {
            return B[k];
        } else if (B.length == 0) {
            return A[k];
        }
        HashSet&lt;Pair&gt; isVisited = new HashSet&lt;Pair&gt;();
        PriorityQueue&lt;Pair&gt; minHeap = new PriorityQueue&lt;Pair&gt;();
        Pair p;
        Pair nextP;
        p = new Pair(0, 0, A[0] + B[0]);
        minHeap.offer(p);
        isVisited.add(p);
        int nextX;
        int nextY;
        int nextSum;
        for (int count = 0; count &lt; k - 1; count++) {
            p = minHeap.poll();
            for (int i = 0; i &lt; 2; i++) {
                nextX = p.x + dx[i];
                nextY = p.y + dy[i];
                nextP = new Pair(nextX, nextY, 0);
                if (nextX &gt;= 0 &amp;&amp; nextX &lt; A.length &amp;&amp; nextY &gt;= 0 &amp;&amp; nextY &lt; B.length &amp;&amp; !isVisited.contains(nextP)) {
                    nextSum = A[nextX] + B[nextY];
                    nextP.sum = nextSum;
                    minHeap.offer(nextP);
                    isVisited.add(nextP);
                }
            }
        }
        return minHeap.peek().sum;
    }
}
