/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

/**
 * Definition of BitInteger:
 * public class BitInteger {
 *     public static int INTEGER_SIZE = 31;
 *     public int fetch(int j) {
 *         .... // return 0 or 1, fetch the jth bit of this number
 *     }
 * }
 */
public class Solution {
    /**
     * @param array a BitInteger list
     * @return an integer
     */
    public int findMissing(ArrayList&lt;BitInteger&gt; array) {
        // Write your code here
        /* Start from the least significant bit, and work our way up */
        return findMissing(array, 0);
    }

    public int findMissing(ArrayList&lt;BitInteger&gt; input, int column) {
        if (column &gt;= BitInteger.INTEGER_SIZE) { // We&#39;re done!
            return 0;
        }
        ArrayList&lt;BitInteger&gt; oneBits = new ArrayList&lt;BitInteger&gt;();
        ArrayList&lt;BitInteger&gt; zeroBits = new ArrayList&lt;BitInteger&gt;();
        for (BitInteger t : input) {
            if (t.fetch(column) == 0) {
                zeroBits.add(t);
            } else {
                oneBits.add(t);
            }
        }
        
        if (zeroBits.size() &lt;= oneBits.size()) {
            int v = findMissing(zeroBits, column + 1);
            return (v &lt;&lt; 1) | 0;
        } else {
            int v = findMissing(oneBits, column + 1);
            return (v &lt;&lt; 1) | 1;
        }
    }
}