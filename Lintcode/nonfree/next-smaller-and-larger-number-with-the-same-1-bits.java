/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    // @param n a positive integer
    // @return a positive integer or -1
    public int getPrev(int n) {
        // Write your code here
        int temp = n;
        int c0 = 0;
        int c1 = 0;
        while ((temp &amp; 1) == 1) {
            c1++;
            temp &gt;&gt;= 1;
        }
        if (temp == 0) return -1;

        while (((temp &amp; 1) == 0) &amp;&amp; (temp != 0)) {
            c0++;
            temp &gt;&gt;= 1;
        }
        return n - (1 &lt;&lt; c1) - (1 &lt;&lt; (c0 - 1)) + 1;
    }

    // @param n a positive integer
    // @return a positive integer or -1
    public int getNext(int n) {
        // Write your code here
        int temp = n;
        int c0 = 0;
        int c1 = 0;
        
        while (((temp &amp; 1) == 0) &amp;&amp; (temp != 0)) {
            c0++;
            temp &gt;&gt;= 1;
        }

        while ((temp &amp; 1) == 1) {
            c1++;
            temp &gt;&gt;= 1;
        }
        int result = n + (1 &lt;&lt; c0) + (1 &lt;&lt; (c1 - 1)) - 1;
        if (result &lt; 0)
            return -1;
        return n + (1 &lt;&lt; c0) + (1 &lt;&lt; (c1 - 1)) - 1;
    }
}