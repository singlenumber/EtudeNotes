/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    /**
     * @param n an integer
     * @param m an integer
     * @param operators an array of point
     * @return an integer array
     */
    int converttoId(int x, int y, int m){
        return x*m + y;
    }
    class UnionFind{
        HashMap&lt;Integer, Integer&gt; father = new HashMap&lt;Integer, Integer&gt;();
        UnionFind(int n, int m){
            for(int i = 0 ; i &lt; n; i++) {
                for(int j = 0 ; j &lt; m; j++) {
                    int id = converttoId(i,j,m);
                    father.put(id, id); 
                }
            }
        }
        int compressed_find(int x){
            int parent =  father.get(x);
            while(parent!=father.get(parent)) {
                parent = father.get(parent);
            }
            int temp = -1;
            int fa = x;
            while(fa!=father.get(fa)) {
                temp = father.get(fa);
                father.put(fa, parent) ;
                fa = temp;
            }
            return parent;
                
        }
        
        void union(int x, int y){
            int fa_x = compressed_find(x);
            int fa_y = compressed_find(y);
            if(fa_x != fa_y)
                father.put(fa_x, fa_y);
        }
    }
    
    public List&lt;Integer&gt; numIslands2(int n, int m, Point[] operators) {
        // Write your code here
        List&lt;Integer&gt; ans = new ArrayList&lt;Integer&gt;();
        if(operators == null) {
            return ans;
        }
        
        int []dx = {0,-1, 0, 1};
        int []dy = {1, 0, -1, 0};
        int [][]island = new int[n][m];
       
        
        UnionFind uf = new UnionFind(n, m);
        int count = 0;
        
        for(int i = 0; i &lt; operators.length; i++) {
            count ++;
            int x = operators[i].x;
            int y = operators[i].y;
            if(island[x][y] != 1){
                island[x][y]  = 1;
                int id = converttoId(x,y , m);
                for(int j = 0 ; j &lt; 4; j++) {
                    int nx = x + dx[j];
                    int ny = y + dy[j];
                    if(0 &lt;= nx &amp;&amp; nx &lt; n &amp;&amp; 0 &lt;= ny &amp;&amp; ny &lt; m &amp;&amp; island[nx][ny] == 1) 
                    {
                        int nid = converttoId(nx, ny, m);
                        
                        int fa = uf.compressed_find(id);
                        int nfa = uf.compressed_find(nid);
                        if(fa != nfa) {
                            count--;
                            uf.union(id, nid);
                        }
                    }
                }
            }
            ans.add(count);
        }
        return ans;
    }
}


///////////////////////
public class Solution {
    public List&lt;Integer&gt; numIslands2(int m, int n, int[][] positions) {
        boolean[][] map = new boolean[m][n];
        int[][] dir = {{0,1},{0,-1},{1,0},{-1,0}};
        List&lt;Integer&gt; list = new ArrayList&lt;Integer&gt;();
        int island = 0;
        int[] fa = new int[m * n];
        
        //initialization
        for (int i = 0; i &lt; m * n; i++) {
            fa[i] = i;
        }
        
        for (int i = 0; i &lt; positions.length; i++) {
            island++;
            map[positions[i][0]][positions[i][1]] = true;
            int x, y;
            int f = positions[i][0] * n + positions[i][1];
            for (int k = 0; k &lt; 4; k++) {
                x = positions[i][0] + dir[k][0];
                y = positions[i][1] + dir[k][1];
                if (x &gt;= 0 &amp;&amp; x &lt; m &amp;&amp; y &gt;=0 &amp;&amp; y &lt; n &amp;&amp; map[x][y] &amp;&amp; getfather(fa, x * n + y) != f) {
                    fa[getfather(fa, x * n + y)] = f;
                    island--;
                }
            }
            list.add(island);
        }
        return list;
    }
    
    //disjoint-set and path compression
    public int getfather(int[] fa, int i) {
        if (fa[i] == i) {
            return i;
        }
        fa[i] = getfather(fa, fa[i]);//path compression here
        return fa[i];
    }
}
