/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    /**
     * @param A an integer array
     * @param k an integer
     * @return an integer
     */
    int [][]init(int []A)  
    {  
        int n = A.length;
        int [][]dis = new int [n+1][n+1];
        for(int i = 1; i &lt;= n; i++) {  
            for(int j = i+1 ;j &lt;= n;++j)  
            {  
                int mid = (i+j) /2;  
                for(int k = i;k &lt;= j;++k)  
                    dis[i][j] += Math.abs(A[k - 1] - A[mid - 1]);  
            } 
        }
        return dis; 
    } 
    
    public int postOffice(int[] A, int k) {
        // Write your code here
        int n = A.length;
        Arrays.sort(A);

        int [][]dis = init(A);
        int [][]dp = new int[n + 1][k + 1];
        if(n == 0 || k &gt;= A.length)
            return 0;
        int ans = Integer.MAX_VALUE;
        for(int i = 0;i &lt;= n;++i)  {
            dp[i][1] = dis[1][i];

        }
        
        
        for(int nk = 2; nk &lt;= k; nk++) {
            
            for(int i = nk; i &lt;= n; i++) {
                dp[i][nk] = Integer.MAX_VALUE;
                for(int j = 0; j &lt; i; j++) {  
                    if(dp[i][nk] == Integer.MAX_VALUE || dp[i][nk] &gt; dp[j][nk-1] + dis[j+1][i])  
                        dp[i][nk] = dp[j][nk-1] + dis[j+1][i];   
                }  
            }
        }
        return dp[n][k];
    }
}