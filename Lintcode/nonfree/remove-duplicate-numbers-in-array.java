/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

// O(n) time, O(n) space
public class Solution {
    /**
     * @param nums an array of integers
     * @return the number of unique integers
     */
    public int deduplication(int[] nums) {
        // Write your code here
        HashMap&lt;Integer, Boolean&gt; mp = new HashMap&lt;Integer, Boolean&gt;();
        for (int i = 0; i &lt; nums.length; ++i)
            mp.put(nums[i], true);

        int result = 0;
        for (Map.Entry&lt;Integer, Boolean&gt; entry : mp.entrySet())
            nums[result++] = entry.getKey();
        return result;
    }
}

// O(nlogn) time, O(1) extra space
public class Solution {
    /**
     * @param nums an array of integers
     * @return the number of unique integers
     */
    public int deduplication(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }
        
        Arrays.sort(nums);
        int len = 0;
        for (int i = 0; i &lt; nums.length; i++) {
            if (nums[i] != nums[len]) {
                nums[++len] = nums[i];
            }
        }
        return len + 1;
    }
}