/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    /**
     * @param a an integer
     * @param b an integer
     * @param c an integer
     * @return a double array
     */
    public double[] rootOfEquation(double a, double b, double c) {
        if (b * b - 4 * a * c &lt; 0) {
            return new double[0];
        }
        
        if (b * b - 4 * a * c == 0) {
            double[] root = new double[1];
            root[0] = -b / 2.0 / a;
            return root;
        }
        
        double[] root = new double[2];
        double delta = Math.sqrt(b * b - 4 * a * c);
        root[0] = (-b - delta) / 2.0 / a;
        root[1] = (-b + delta) / 2.0 / a;
        
        if (root[0] &gt; root[1]) {
            double temp = root[0];
            root[0] = root[1];
            root[1] = temp;
        }
        return root;
    }
}
