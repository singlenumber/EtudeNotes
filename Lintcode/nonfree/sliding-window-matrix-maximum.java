/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    /**
     * @param matrix an integer array of n * m matrix
     * @param k an integer
     * @return the maximum number
     */
    public int maxSlidingWindow2(int[][] matrix, int k) {
        // Write your code here
        int n = matrix.length;
        if (n == 0 || n &lt; k)
            return 0;
        int m = matrix[0].length;
        if (m == 0 || m &lt; k)
            return 0;

        int[][] sum = new int[n + 1][m + 1];
        for (int i = 0; i &lt;= n; ++i) sum[i][0] = 0;
        for (int i = 0; i &lt;= m; ++i) sum[0][i] = 0;

        for (int i = 1; i &lt;= n; ++i)
            for (int j = 1; j &lt;= m; ++j)
                sum[i][j] = matrix[i - 1][j - 1] + 
                            sum[i - 1][j] + sum[i][j - 1] - sum[i - 1][j - 1];

        int max_value = Integer.MIN_VALUE;
        for (int i = k; i &lt;= n; ++i)
            for (int j = k; j &lt;= m; ++j) {
                int value = sum[i][j] - sum[i - k][j] -
                            sum[i][j - k] + sum[i - k][j - k];

                if (value &gt; max_value)
                    max_value = value;
            }
        return max_value;
    }
}
