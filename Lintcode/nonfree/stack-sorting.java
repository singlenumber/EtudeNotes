/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    /**
     * @param stack an integer stack
     * @return void
     */
    public void stackSorting(Stack&lt;Integer&gt; stack) {
        // Write your code here
        Stack&lt;Integer&gt; tmp = new Stack&lt;Integer&gt;();
        while (!stack.isEmpty()) {
            if (!stack.isEmpty() &amp;&amp; (tmp.isEmpty() || tmp.peek() &gt;= stack.peek())) {
                tmp.push(stack.peek());
                stack.pop();
            }
            else {
                int value = stack.peek(); stack.pop();
                while (!tmp.isEmpty() &amp;&amp; tmp.peek() &lt;= value)  {
                    stack.push(tmp.peek());
                    tmp.pop();
                }
                stack.push(value);
                while (!tmp.isEmpty()) {
                    stack.push(tmp.peek());
                    tmp.pop();
                }
            }
        }
        while (!tmp.isEmpty()) {
            stack.push(tmp.peek());
            tmp.pop();
        }
    }
}