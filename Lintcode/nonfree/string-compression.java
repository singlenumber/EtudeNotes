/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    /**
     * @param str a string
     * @return a compressed string
     */
    public String compress(String str) {
        // Write your code here
        /* Check if compression would create a longer string */
        int size = countCompression(str);
        if (size &gt;= str.length()) {
            return str;
        }

        char[] array = new char[size];
        int index = 0;
        char last = str.charAt(0);
        int count = 1;
        for (int i = 1; i &lt; str.length(); i++) {
            if (str.charAt(i) == last) { // Found repeated character
                count++;
            } else {
                /* Update the repeated character count */
                index = setChar(array, last, index, count);
                last = str.charAt(i);
                count = 1;
            }
        }
        /* Update string with the last set of repeated characters. */
        index = setChar(array, last, index, count);
        return String.valueOf(array);
    }

    public int setChar(char[] array, char c, int index, int count) {
        array[index] = c;
        index++;
        char[] cnt = String.valueOf(count).toCharArray();

        /* Copy characters from biggest digit to smallest */
        for (char x : cnt) {
            array[index] = x;
            index++;
        }
        return index;
    }

    int countCompression(String str) {
        if (str == null || str.isEmpty()) return 0;
        char last = str.charAt(0);
        int size = 0;
        int count = 1;
        for (int i = 1; i &lt; str.length(); i++) {
            if (str.charAt(i) == last) {
                count++;
            } else {
                last = str.charAt(i);
                size += 1 + String.valueOf(count).length();
                count = 1;
            }
        }
        size += 1 + String.valueOf(count).length();
        return size;
    }
}