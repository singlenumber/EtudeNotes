/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Student {
    /**
     * Declare two public attributes name(string) and score(int).
     */
    public String name;
    public int score;
    
    /**
     * Declare a constructor expect a name as a parameter.
     */
    public Student(String name) {
        this.name = name;
    }
    
    /**
     * Declare a public method `getLevel` to get the level(char) of this student.
     */
    public char getLevel() {
        if (score &gt;= 90) {
            return &#39;A&#39;;
        } else if (score &gt;= 80) {
            return &#39;B&#39;;
        } else if (score &gt;= 60) {
            return &#39;C&#39;;
        } else {
            return &#39;D&#39;;
        }
    }
}
