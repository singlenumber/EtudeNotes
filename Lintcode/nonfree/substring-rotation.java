/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    /**
     * @param s1 the first string
     * @param s2 the second string
     * @return true if s2 is a rotation of s1 or false
     */
    public boolean isRotation(String s1, String s2) {
        // Write your code here
        // Using one call to isSubstring
        int len = s1.length();
        /* check that s1 and s2 are equal length and not empty */
        if (len == s2.length() &amp;&amp; len &gt; 0) {
            /* concatenate s1 and s1 within new buffer */
            String s1s1 = s1 + s1;
            return isSubstring(s1s1, s2);
        }
        return false;
    }
    
    // @return true if t is a substring of s or false
    public boolean isSubstring(String s, String t) {
        return s.contains(t);
    }
}