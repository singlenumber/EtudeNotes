/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

import java.util.Random;

class Solution {
    /*
     * @param nums an integer array
     * @param k an integer
     * @return the top k largest numbers in array
     */
    public int[] topk(int[] nums, int k) {
        // Write your code here
        quickSort(nums, 0, nums.length - 1, k);

        List&lt;Integer&gt; result = new ArrayList&lt;Integer&gt;();
        for (int i = 0; i &lt; k &amp;&amp; i &lt; nums.length; ++i)
            result.add(nums[i]);

        Collections.sort(result, Collections.reverseOrder());
        int[] topk = new int[k];
        for (int i = 0; i &lt; k; ++i)
            topk[i] = result.get(i);

        return topk;
    }
    
    private void quickSort(int[] A, int start, int end, int k) {
        if (start &gt;= k || end &lt; k)
            return;

        if (start &gt;= end) {
            return;
        }
        
        int left = start, right = end;
        // key point 1: pivot is the value, not the index
        Random rand =new Random(end - start + 1);
        int index = rand.nextInt(end - start + 1) + start;
        int pivot = A[index];

        // key point 2: every time you compare left &amp; right, it should be 
        // left &lt;= right not left &lt; right
        while (left &lt;= right) {
            // key point 3: A[left] &lt; pivot not A[left] &lt;= pivot
            while (left &lt;= right &amp;&amp; A[left] &gt; pivot) {
                left++;
            }
            // key point 3: A[right] &gt; pivot not A[right] &gt;= pivot
            while (left &lt;= right &amp;&amp; A[right] &lt; pivot) {
                right--;
            }

            if (left &lt;= right) {
                int temp = A[left];
                A[left] = A[right];
                A[right] = temp;
                
                left++;
                right--;
            }
        }
        
        quickSort(A, start, right, k);
        quickSort(A, left, end, k);
    }
};
