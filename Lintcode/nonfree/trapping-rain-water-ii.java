/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

class Cell{
  public int x,y, h;
  Cell(){}
  Cell(int xx,int yy, int hh){
    x= xx;
    y= yy;
    h =hh;
  }
}
class CellComparator implements Comparator&lt;Cell&gt; {
  @Override
  public int compare(Cell x, Cell y)
  {
    if(x.h &gt; y.h)
      return 1;
    else if(x.h == y.h){
     return 0;
    }
    else {
      return -1;
    }
  }
} 


public class Solution {
  int []dx = {1,-1,0,0};
  int []dy = {0,0,1,-1};
  public  int trapRainWater(int[][] heights) {
       // write your code here
     if(heights.length == 0)  
        return 0;
    PriorityQueue&lt;Cell&gt; q =  new PriorityQueue&lt;Cell&gt;(1,new CellComparator());
      int n = heights.length;
      int m = heights[0].length;
      int [][]visit = new int[n][m];
      
      for(int i = 0; i &lt; n; i++) {
        q.offer(new Cell(i,0,heights[i][0]));

        q.offer(new Cell(i,m-1,heights[i][m-1]));
        visit[i][0] = 1;
        visit[i][m-1] = 1;
      }
      for(int i = 0; i &lt; m; i++) {
        q.offer(new Cell(0,i,heights[0][i]));

        q.offer(new Cell(n-1,i,heights[n-1][i]));
        visit[0][i] = 1;
        visit[n-1][i] = 1;

      }
      int ans = 0 ;
      while(!q.isEmpty()) {
        
        Cell now = q.poll();
        
        for(int i = 0; i &lt; 4; i++) {
          
          int nx = now.x + dx[i];
          int ny = now.y + dy[i];
          if(0&lt;=nx &amp;&amp; nx &lt; n &amp;&amp; 0 &lt;= ny &amp;&amp; ny &lt; m &amp;&amp; visit[nx][ny] == 0) {
            visit[nx][ny] = 1;
            q.offer(new Cell(nx,ny,Math.max(now.h,heights[nx][ny])));
            ans = ans + Math.max(0,now.h - heights[nx][ny]);
          }
          
        }
      }
      return ans;
    }
  
}