/**
 * 本代码由九章算法编辑提供。没有版权欢迎转发。
 * - 九章算法致力于帮助更多中国人找到好的工作，教师团队均来自硅谷和国内的一线大公司在职工程师。
 * - 现有的面试培训课程包括：九章算法班，系统设计班，九章强化班，Java入门与基础算法班，
 * - 更多详情请见官方网站：http://www.jiuzhang.com/
 */

public class Solution {
    /**
     * @param nums an integer array
     * @param target an integer
     * @return the difference between the sum and the target
     */
    public int twoSumCloset(int[] nums, int target) {
        // Write your code here
        Arrays.sort(nums);
        int i = 0, j = nums.length - 1;
        int diff = Integer.MAX_VALUE;
        while (i &lt; j) {
            if (nums[i] + nums[j] &lt; target) {
                if (target - nums[i] - nums[j] &lt; diff)
                    diff = target - nums[i] - nums[j];
                i ++;
            } else {
                if (nums[i] + nums[j] - target &lt; diff)
                    diff = nums[i] + nums[j] - target;
                j --;
            }
        }
        return diff;
    }
}
